package com.app.baakki.base;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import com.app.baakki.R;


public class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

    }

    public void setToolbar(ImageView ivBack, TextView tvTitle,View vwLine,String name) {
        ivBack.setImageResource(R.drawable.ic_back_black_arrow);
        tvTitle.setVisibility(View.VISIBLE);
        vwLine.setVisibility(View.VISIBLE);

        tvTitle.setText(name);
        ivBack.setOnClickListener(this);

    }

    public void setToolbar(ImageView ivBack, TextView tvTitle, View vwLine, TextView tvNeedSomeHelp, String name, View.OnClickListener onClickListener) {
        ivBack.setImageResource(R.drawable.ic_back_black_arrow);
        tvTitle.setVisibility(View.VISIBLE);
        vwLine.setVisibility(View.VISIBLE);
        tvNeedSomeHelp.setVisibility(View.VISIBLE);
        tvTitle.setText(name);
        ivBack.setOnClickListener(this);
        tvNeedSomeHelp.setOnClickListener(onClickListener);

    }


    public void setWhiteTitle(ImageView ivBack, TextView tvNeedSomeHelp) {
        tvNeedSomeHelp.setVisibility(View.GONE);
        ivBack.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_back) {
            finish();
        }
    }
}
