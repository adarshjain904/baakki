package com.app.baakki.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.databinding.ActivityThankYouBinding;
import com.app.baakki.util.Constant;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;

public class ThankYouActivity extends AppCompatActivity {
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityThankYouBinding activityBinding;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_thank_you);

        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    private void init() {
        context = ThankYouActivity.this;
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);
        sharedPreference.putBoolean(Constant.IS_NEW_USER,true);
    }

    public class MyClickHandlers {
        public void onLetStartClick(View view) {
            Intent intent = new Intent(context, SelectDistanceActivity.class);
           // intent.putExtra(Constant.IS_NEW_USER,true);

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }
}
