package com.app.baakki.home;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityChooseCountryBinding;

import com.app.baakki.listener.LocationHelper;
import com.app.baakki.login.SignUpActivity;
import com.app.baakki.model.RegisterRequest;
import com.app.baakki.model.login.LoginSignupData;
import com.app.baakki.model.login.LoginSignupRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.LocationBuilder;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.android.gms.location.LocationResult;
import com.google.gson.Gson;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ChooseCountryActivity extends BaseActivity implements LocationHelper {
    String lat, lon,address;
    String countryId = "1";
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityChooseCountryBinding activityBinding;
    CompositeDisposable compositeDisposable;
    LocationBuilder locationBuilder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_choose_country);
        setWhiteTitle(activityBinding.tool.ivBack, activityBinding.tool.tvNeedSomeHelp);
        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    private void init() {
        context = ChooseCountryActivity.this;
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        locationBuilder = new LocationBuilder(ChooseCountryActivity.this);
        locationBuilder.getLocation(ChooseCountryActivity.this);


    }

    public class MyClickHandlers {
        public void onContinueClick(View view) {

            if (!activityBinding.cbAllowStore.isChecked())
                Toast.makeText(context, "Please select privacy policy and terms & condition", Toast.LENGTH_SHORT).show();
            else if (!activityBinding.cbTermCondition.isChecked())
                Toast.makeText(context, "Please select terms & condition", Toast.LENGTH_SHORT).show();
            //lse
               // userRegisterService();
        }
    }

    @Override
    public void getLocation(LocationResult locationResult) {
        for (Location location : locationResult.getLocations()) {
            if (location.getLatitude() == 0 && location.getLongitude() == 0) {
                Log.e("null lat log", "=--=-=");
                return;
            }
            lat = String.valueOf(location.getLatitude());
            lon = String.valueOf(location.getLongitude());
            address = locationBuilder.getAddress(context, location.getLatitude(), location.getLongitude());
        }
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }
}
