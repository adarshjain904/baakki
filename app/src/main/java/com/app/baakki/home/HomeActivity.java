package com.app.baakki.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.app.baakki.R;
import com.app.baakki.category.CategoryFragment;
import com.app.baakki.databinding.ActivityHomeBinding;
import com.app.baakki.discover.DiscoverFragment;
import com.app.baakki.discover.ReserveOrderActivity;
import com.app.baakki.favorites.FavoritesFragment;
import com.app.baakki.listener.LocationHelper;
import com.app.baakki.model.ChooseLocation;
import com.app.baakki.model.reserveorder.ReserveOrderData;
import com.app.baakki.model.reserveorder.ReserveOrderRes;
import com.app.baakki.other.OtherFragment;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.splash.ReminderService;
import com.app.baakki.util.Constant;
import com.app.baakki.util.DialogUtility;
import com.app.baakki.util.LocationBuilder;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.android.gms.location.LocationResult;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.app.baakki.util.ManifestPermission.hasPermissions;

public class HomeActivity extends AppCompatActivity implements LocationHelper, BottomNavigationView.OnNavigationItemSelectedListener {
    Context context;
    ProgressLoader progressLoader;
    CompositeDisposable compositeDisposable;
    SharedPreference sharedPreference;
    ActivityHomeBinding activityBinding;
    ArrayList<ReserveOrderData> reserveOrderData;
    NavController navController;
    ChooseLocation chooseLocation;
    String currentLat, currentLong, address;
    boolean isLocation = false;
    LocationBuilder locationBuilder;
    String[] permissions = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    private void init() {
        context = HomeActivity.this;
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(activityBinding.bnvTab, navController);
        activityBinding.bnvTab.setOnNavigationItemSelectedListener(this);

        if (hasPermissions(this, permissions)) {
            if (!DialogUtility.isLocationEnable(context)) {
                progressLoader.show();

                locationBuilder = new LocationBuilder(this);
                locationBuilder.getLocation(this);
            } else {
                getLocalLatLog();
            }
        } else {
            getLocalLatLog();
        }


        Intent intent = new Intent(this, ReminderService.class);
        startService(intent);

        registerReceiver(broadcastReceiver, new IntentFilter("INTERNET_LOST"));
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.fragment_discover:
                navController.navigate(R.id.fragment_discover);
                isRreserveCampaignService();
                break;
            case R.id.fragment_category:
                navController.navigate(R.id.fragment_category);
                isRreserveCampaignService();
                break;
            case R.id.fragment_favorites:
                navController.navigate(R.id.fragment_favorites);
                isRreserveCampaignService();
                break;
            case R.id.fragment_other:
                navController.navigate(R.id.fragment_other);
                isRreserveCampaignService();
                break;

        }

        return true;
    }


    public class MyClickHandlers {
        public void onOrderClick(View view) {
            Intent intent = new Intent(context, ReserveOrderActivity.class);
            intent.putParcelableArrayListExtra(Constant.RESERVE_DATA_ARRAY, reserveOrderData);
            startActivityForResult(intent, Constant.RESERVE_CODE);
        }
    }

    public void getLocalLatLog() {
        chooseLocation = new Gson().fromJson(sharedPreference.getString(Constant.LOCATION_DATA), ChooseLocation.class);
        currentLat = String.valueOf(chooseLocation.getLatLng().latitude);
        currentLong = String.valueOf(chooseLocation.getLatLng().longitude);
        isRreserveCampaignService(); // add new
    }

    @Override
    public void getLocation(LocationResult locationResult) {
        for (Location location : locationResult.getLocations()) {
            if (location.getLatitude() == 0 && location.getLongitude() == 0) {
                Log.e("null lat log", "=--=-=");
                return;
            }
            currentLat = String.valueOf(location.getLatitude());
            currentLong = String.valueOf(location.getLongitude());
            address = locationBuilder.getAddress(context, location.getLatitude(), location.getLongitude());


            if (!isLocation) {
                isLocation = true;
                isRreserveCampaignService();
            }
        }
    }

    /*private void ShowLocationDialog() {
        DialogUtility.alertMessageNoGps(context, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, Constant.LOCATION_RESULT);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.LOCATION_RESULT) {
            if (DialogUtility.isLocationEnable(context)) {
                ShowLocationDialog();
            }
        } else if (resultCode == RESULT_OK && requestCode == Constant.RESERVE_CODE) {

        }
    }

    @Override
    public void getLocation(LocationResult locationResult) {
        for (Location location : locationResult.getLocations()) {
            if (location.getLatitude() == 0 && location.getLongitude() == 0) {
                Log.e("null lat log", "=--=-=");
                return;
            }
            currentLat = String.valueOf(location.getLatitude());
            currentLong = String.valueOf(location.getLongitude());
            address = locationBuilder.getAddress(context, location.getLatitude(), location.getLongitude());


            if (!isLocation) {
                progressLoader.dismiss();
                isLocation = true;
                isRreserveCampaignService();
            }
        }
    }*/

    private void isRreserveCampaignService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class)
                .myReserveCampaignApi(currentLat, currentLong)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ReserveOrderRes>() {
                    @Override
                    public void onSuccess(@NonNull ReserveOrderRes reserveOrderRes) {
                        progressLoader.dismiss();
                        handleCategoryResponse(reserveOrderRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        activityBinding.rlOrder.setVisibility(View.GONE);
                        ValidationUtils.errorCode(context, e.getMessage());
                        Log.e("onError: ", e.getMessage());
                    }
                }));
    }

    private void handleCategoryResponse(ReserveOrderRes reserveOrderRes) {
        Log.e("reserve status", reserveOrderRes.getMessage());
        reserveOrderData = reserveOrderRes.getData();
        if (reserveOrderRes.getStatus()) {
            activityBinding.rlOrder.setVisibility(View.VISIBLE);
            if (reserveOrderRes.getData().size() == 1)
                activityBinding.tvNoOrder.setText(/*"You Reserve " +*/  reserveOrderRes.getData().size() + " pending order");
            else
                activityBinding.tvNoOrder.setText(/*"You Reserve " + + */ reserveOrderRes.getData().size() + " pending orders");
        } else {
            activityBinding.rlOrder.setVisibility(View.GONE);
        }
    }


    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("onReceive", "call =-=-=");
            //   Toast.makeText(context, "call onReceive", Toast.LENGTH_SHORT).show();

            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
            String formattedDate = df.format(Calendar.getInstance().getTime());
            Log.e("formattedDate=--=", formattedDate);
            if (formattedDate.equals("23:59"))
                isRreserveCampaignService();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }
}
