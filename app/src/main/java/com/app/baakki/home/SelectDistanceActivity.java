package com.app.baakki.home;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.bottomsheet.DistanceCategoryBottomSheet;
import com.app.baakki.bottomsheet.DistanceBottomSheet;
import com.app.baakki.databinding.ActivitySelectDistanceBinding;
import com.app.baakki.listener.LocationHelper;
import com.app.baakki.listener.SelectCategoryClick;
import com.app.baakki.listener.SelectDistanceClick;
import com.app.baakki.model.ChooseLocation;
import com.app.baakki.model.category.CategoryData;
import com.app.baakki.model.category.CategoryRes;
import com.app.baakki.other.PrivacyPolicyActivity;
import com.app.baakki.other.TermsConditionActivity;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.DialogUtility;
import com.app.baakki.util.LocationBuilder;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.app.baakki.util.ManifestPermission.hasPermissions;


public class SelectDistanceActivity extends BaseActivity implements OnMapReadyCallback, LocationHelper {
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivitySelectDistanceBinding activityBinding;
    List<Place.Field> fields;
    LatLng latLng;
    CompositeDisposable compositeDisposable;
    List<CategoryData> categoryData;
    GoogleMap googleMap;
    LocationBuilder locationBuilder;
    String lat, lon, address, categoryId, selectDistance;
    Marker marker;
    boolean isLocation = false;
    String[] permissions = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_select_distance);
        setToolbar(activityBinding.tool.ivBack, activityBinding.tool.tvTitle, activityBinding.tool.vwLine, getString(R.string.choose_location));
        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    private void init() {
        context = SelectDistanceActivity.this;
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        Places.initialize(getApplicationContext(), getString(R.string.google_map_key));
        fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        setTextClick(activityBinding.cbTermCondition);

        if (sharedPreference.getBoolean(Constant.IS_NEW_USER)) {
            activityBinding.cbAllowStore.setVisibility(View.VISIBLE);
            activityBinding.cbTermCondition.setVisibility(View.VISIBLE);

        } else {
            activityBinding.cbAllowStore.setVisibility(View.GONE);
            activityBinding.cbTermCondition.setVisibility(View.GONE);
        }


        if (hasPermissions(this, permissions)) {
            if (!DialogUtility.isLocationEnable(context)) {
                progressLoader.show();

                locationBuilder = new LocationBuilder(this);
                locationBuilder.getLocation(this);
            }
        }
    }

    private void setTextClick(TextView textView) {
        SpannableString ss = new SpannableString(textView.getText().toString());

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                startActivity(new Intent(context, TermsConditionActivity.class));
            }

        };

        ClickableSpan clickableSpan2 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                startActivity(new Intent(context, PrivacyPolicyActivity.class));
            }
        };

        ss.setSpan(clickableSpan1, 17, 36, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan2, 44, 59, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

    }



    @Override
    public void getLocation(LocationResult locationResult) {
        progressLoader.dismiss();
        for (Location location : locationResult.getLocations()) {
            if (location.getLatitude() == 0 && location.getLongitude() == 0) {
                Log.e("null lat log", "=--=-=");
                return;
            }

            activityBinding.ivCurrentLocation.setImageResource(R.drawable.ic_location);
            lat = String.valueOf(location.getLatitude());
            lon = String.valueOf(location.getLongitude());
            latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
            address = locationBuilder.getAddress(context, location.getLatitude(), location.getLongitude());
            activityBinding.tvSearchCity.setText(address);
            addMarker(latLng, 12);
        }
    }
    public void addMarker(LatLng latLng, int i) {
        googleMap.clear();
        marker = googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(bitmapDescriptorFromVector(context, R.drawable.ic_baakki_marker)));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, i), 4000, null);
    }
    private boolean isValid() {
        boolean isValid = true;
        if (!activityBinding.cbAllowStore.isChecked()) {
            Toast.makeText(context, "Please select privacy policy and terms & condition", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (!activityBinding.cbTermCondition.isChecked()) {
            Toast.makeText(context, "Please select terms & condition", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        return isValid;
    }

    public class MyClickHandlers implements SelectDistanceClick, SelectCategoryClick {
        public void onContinueClick(View view) {
            if (TextUtils.isEmpty(activityBinding.tvSearchCity.getText().toString())) {
                Toast.makeText(context, "Enter a location of your choice", Toast.LENGTH_SHORT).show();
            } else if (sharedPreference.getBoolean(Constant.IS_NEW_USER)) {
                if (isValid())
                    moveActivity();
            } else {
                moveActivity();
            }

        }

        public void onSearchClick(View view) {
            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                    //  .setCountry(sharedPreference.getString(Constant.COUNTRY_CODE))
                    .setCountry("GB")
                    .build(SelectDistanceActivity.this);
            startActivityForResult(intent, Constant.AUTOCOMPLETE_REQUEST_CODE);
        }

        public void onDistanceClick(View view) {
            new DistanceBottomSheet(this).show(getSupportFragmentManager(), "DistanceBottomSheet");
        }

        public void onNavigationClick(View view) {
            if (!hasPermissions(SelectDistanceActivity.this, permissions)) {
                ActivityCompat.requestPermissions(SelectDistanceActivity.this, permissions, 1);
            } else if (DialogUtility.isLocationEnable(context)) {
                ShowLocationDialog();
            } else {
                progressLoader.show();
                locationBuilder = new LocationBuilder(SelectDistanceActivity.this);
                locationBuilder.getLocation(SelectDistanceActivity.this);
            }


        }

        public void onSelectCategoryClick(View view) {
            new DistanceCategoryBottomSheet(this, categoryData).show(getSupportFragmentManager(), "CategoryBottomSheet");
        }

        @Override
        public void selectDistance(String distance) {
            selectDistance = distance;
            activityBinding.tvDistanceCity.setText(distance);
        }

        @Override
        public void selectCategory(CategoryData category) {
            categoryId = String.valueOf(category.getId());
            activityBinding.tvCategory.setText(category.getName());
        }
    }

    private void moveActivity() {
        ChooseLocation chooseLocation = new ChooseLocation();
        chooseLocation.setLatLng(marker.getPosition());
        chooseLocation.setAddress(activityBinding.tvSearchCity.getText().toString());
        chooseLocation.setCategoryName(activityBinding.tvCategory.getText().toString());
        chooseLocation.setDistance(selectDistance);
        chooseLocation.setCategoryId(categoryId);

        sharedPreference.putString(Constant.LOCATION_DATA, new Gson().toJson(chooseLocation));
        sharedPreference.putBoolean(Constant.IS_SELECT_LOCATION, true);
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                activityBinding.ivCurrentLocation.setImageResource(R.drawable.ic_location_gray);
                activityBinding.tvSearchCity.setText(place.getAddress());
                latLng = place.getLatLng();
                addMarker(place.getLatLng(), 12);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
            }
        } else if (requestCode == Constant.LOCATION_RESULT) {
            if (hasPermissions(this, permissions)) {
                if (DialogUtility.isLocationEnable(context)) {
                    ShowLocationDialog();
                } else {
                    progressLoader.show();
                    locationBuilder = new LocationBuilder(this);
                    locationBuilder.getLocation(this);
                }
            }
        }
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void categoryService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).categoryApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CategoryRes>() {
                    @Override
                    public void onSuccess(@NonNull CategoryRes categoryRes) {
                        progressLoader.dismiss();
                        handleCategoryResponse(categoryRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                        Log.e("onError: ", e.getMessage());
                    }
                }));
    }

    private void handleCategoryResponse(CategoryRes categoryRes) {
        if (categoryRes.getStatus()) {
            categoryData = categoryRes.getData();
        } else {
            activityBinding.tvCategory.setText(R.string.not_available_category);
            Toast.makeText(context, categoryRes.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void ShowLocationDialog() {
        DialogUtility.alertMessageNoGps(context, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, Constant.LOCATION_RESULT);
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {
                    if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.e("msg", "<--location granted");
                            //count++;
                        } else {
                            // DialogUtility.showToast(this, "Permission is required...");
                        }
                    } else if (permissions[i].equals(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.e("msg", "<--read phone granted");
                            // count++;
                        } else {
                            //DialogUtility.showToast(this, "Permission is required...");
                        }
                    }
                }
            }

            if (hasPermissions(this, permissions)) {
                if (DialogUtility.isLocationEnable(context)) {
                    ShowLocationDialog();
                } else {
                    progressLoader.show();
                    locationBuilder = new LocationBuilder(this);
                    locationBuilder.getLocation(this);
                }
            }
            // activityBinding.ivSplash.post(new Starter());
        }
    }


}
