package com.app.baakki.services;


import com.app.baakki.model.addcampaignres.AddCampaignRes;
import com.app.baakki.model.blog.BlogRes;
import com.app.baakki.model.cancelorder.CancelOrderRes;
import com.app.baakki.model.category.CategoryRes;
import com.app.baakki.model.completecampaign.CompleteCampaignRes;
import com.app.baakki.model.country.CountryRes;
import com.app.baakki.model.createorder.CreateOrder;
import com.app.baakki.model.faq.FaqRes;
import com.app.baakki.model.filterapi.FilterRes;
import com.app.baakki.model.gelallfav.GetAllFavRes;
import com.app.baakki.model.joinbaakki.JoinBaakkiRes;
import com.app.baakki.model.login.LoginSignupRes;
import com.app.baakki.model.mycampaignlist.MyCampaignListRes;
import com.app.baakki.model.pendingcompletecampaign.PendingCompleteCampaignRes;
import com.app.baakki.model.profile.ProfileRes;
import com.app.baakki.model.rating.RatingRes;
import com.app.baakki.model.reserveorder.ReserveOrderRes;
import com.app.baakki.model.shoplist.ShopListRes;
import com.app.baakki.model.subcategory.SubcategoryRes;
import com.app.baakki.model.termpolicy.TermPolicyRes;
import com.app.baakki.model.usergetallcampaign.UserGetAllCampaignRes;
import com.app.baakki.model.vieworder.ViewOrderRes;
import com.google.gson.JsonElement;

import java.util.HashMap;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("login")
    Single<LoginSignupRes> loginApi(@Field("email") String phone, @Field("password") String name, @Field("role_id") Integer roleId);

    @FormUrlEncoded
    @POST("userregister")
    Single<LoginSignupRes> userRegisterApi(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password,
            @Field("password_confirmation") String password_confirmation,
            @Field("role_id") String role_id,
            @Field("country_id") String country_id,
            @Field("register_via") String register_via,
            @Field("device_token") String device_token,
            @Field("device_type") String device_type
          /*  @Field("lat") String lat,
            @Field("lon") String lon*/);

    @FormUrlEncoded
    @POST("send_emailverify_otp")
    Single<JsonElement> sendEmailVerifyOtp(@Field("email") String email);

    @FormUrlEncoded
    @POST("forgotpassword")
    Single<JsonElement> forgotpassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("socialLoginRegistrationForUser")
    Single<LoginSignupRes> userSocialRegisterApi(
            @Field("name") String name,
            @Field("email") String email,
            @Field("social_id") String social_id,
            @Field("register_via") String register_via,
            @Field("device_token") String device_token,
            @Field("device_type") String device_type
           /* @Field("lat") String lat,
            @Field("lon") String lon*/);

    @GET("logout")
    Single<JsonElement> logoutApi();

    @GET("myinfo")
    Single<LoginSignupRes> myInfoApi();

    @GET("countrylist")
    Single<CountryRes> countrylistApi();

    @FormUrlEncoded
    @POST("getcampaignlist")
    Single<UserGetAllCampaignRes> getCampaignListApi(
            @Field("page") Integer type,
            @Field("distance") Integer distance,
            @Field("category_id") String category_id,
            @Field("latitude") String lat,
            @Field("longitude") String lon);

    @FormUrlEncoded
    @POST("order_create") // reserve
    Single<CreateOrder> orderCreateApi(
            @Field("campaintimes_id") String campaintimesId,
          /*  @Field("user_lat") String lat,
            @Field("user_lng") String lon,*/
            @Field("quantity") Integer quantity);

    @FormUrlEncoded
    @POST("joinbaakki")
    Single<JoinBaakkiRes> joinBaakkiApi(
            @Field("business_name") String business_name,
            @Field("country_id") String country_id,
            @Field("email") String email,
            @Field("contact") String contact);

    @FormUrlEncoded
    @POST("submitratingreview")
    Single<RatingRes> submitRatingReviewApi(
            @Field("campaign_id") String campaignId,
            @Field("title") String title,
            @Field("comment") String comment,
            @Field(" rating") float rating);

    @FormUrlEncoded
    @POST("getcampaignlist_filter")
    Single<FilterRes> getCampaignFilterListApi(
            @Field("page") Integer type,
            @Field("latitude") String lat,
            @Field("longitude") String lon,
            @Field("distance") Integer distance,
            @Field("time_start") String time_start,
            @Field("time_end") String time_end,
            @Field("shop_name") String shop_name,
            @Field("product_name") String product_name,
            @Field("price") String price,
            @Field("rating") String rating,
            @Field("ending_soonest") String ending_soonest,
            @Field("newly_listed") String newly_listed,
            @Field("nearest_first") String nearest_first,
            @Field("category_id") String category_id,
            @Field("subcategory_id") String subcategory_id
    );

    @FormUrlEncoded
    @POST("my_reserve_campaign")
    Single<ReserveOrderRes> myReserveCampaignApi(
            @Field("user_lat") String lat,
            @Field("user_lng") String lon);

    @FormUrlEncoded
    @POST("cancel_reserve_campaign")
    Single<CancelOrderRes> cancelReserveApi(
            @Field("order_id") String orderId,
            @Field("quantity") String quantity,
            @Field("reason") String reason);

    @FormUrlEncoded
    @POST("further")
    Single<CancelOrderRes> furtherHelpApi(@Field("my_data") String message);

    @FormUrlEncoded
    @POST("add_and_remove_favorites")
    Single<JsonElement> addRemoveFavoritesApi(@Field("campaign_id") Integer campaignId);

    @FormUrlEncoded
    @POST("mark_as_completed_campaign")  //reedem
    Single<CompleteCampaignRes> completedCampaignApi(@Field("order_id") String orderId);

    @GET("get_my_favlist")
    Single<GetAllFavRes> getAllFavListApi();

    @GET("blog")
    Single<BlogRes> blogApi();

    @GET("faq")
    Single<FaqRes> faqApi();

    @GET("faq_categories")
    Single<JsonElement> faqCategoryApi();

    @GET("privacy_policy_and_term_condition")
    Single<TermPolicyRes> privacyPolicyAndTermConditionApi();

    @FormUrlEncoded
    @POST("getcompleted_campaign")
    Single<PendingCompleteCampaignRes> getPendingCompletedCampaignApi(@Field("user_lat") String lat,
                                                                      @Field("user_lng") String lon);

    @FormUrlEncoded
    @POST("updatepassword")
    Single<LoginSignupRes> updatePasswordApi(@Field("old_password") String old_password
            , @Field("password") String password, @Field("password_confirmation") String password_confirmation);

    @Multipart
    @POST("update_user_profile")
    Single<ProfileRes> userProfileUpdateApi(@PartMap() HashMap<String, RequestBody> data, @Part MultipartBody.Part EventImage);

    // ======================================================= Business Api ======================================================================================
    @GET("category")
    Single<CategoryRes> categoryApi();

    @FormUrlEncoded
    @POST("subcategory")
    Single<SubcategoryRes> subcategoryApi(@Field("category_id") String category_id);

    @FormUrlEncoded
    @POST("mycampaignlist")
    Single<MyCampaignListRes> myCampaignListApi(@Field("page") Integer type);

    @FormUrlEncoded
    @POST("deletecampaign")
    Single<JsonElement> deleteCampaignApi(@Field("campaign_id") Integer campaignId);


    @FormUrlEncoded
    @POST("updatecampaign")
    Single<AddCampaignRes> updateQuantityCampaignApi(@Field("remaining_quantity") String type,
                                                     @Field("campaign_id") Integer campaignId);


    @POST("vieworders")
    Single<ViewOrderRes> orderlistApi();

    @GET("shoplist")
    Single<ShopListRes> shopListApi();

    @Multipart
    @POST("addcampaign")
    Single<AddCampaignRes> addCampaignApi(@PartMap() HashMap<String, RequestBody> data, @Part MultipartBody.Part EventImage);

    @Multipart
    @POST("updatecampaign")
    Single<AddCampaignRes> updateCampaignApi(@PartMap() HashMap<String, RequestBody> data, @Part MultipartBody.Part EventImage);
}
