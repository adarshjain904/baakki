package com.app.baakki.services;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.app.baakki.BaakkiApplication;
import com.app.baakki.util.Constant;
import com.app.baakki.util.SharedPreference;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    //private static final String BASE_URL = "http://support24.in/baakkiwebandapp/api/";
    public static final String BASE_URL = "https://baakki.com/baakki/api/";
  //  public static final String BASE_URL_image = "https://savotechnologies.com/baakki/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(initOkHttp(BaakkiApplication.getAppContext()))
                    .build();
        }
        return retrofit;
    }


    private static OkHttpClient initOkHttp(final Context context) {
        //Cache code
        assert BaakkiApplication.getAppContext() != null;
        File httpCacheDirectory = new File(BaakkiApplication.getAppContext().getCacheDir(), "offlineCache");
        Cache cache = new Cache(httpCacheDirectory, 10 * 1024 * 1024);//10 MB

        //print Log
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        //OkHttpClient
        int REQUEST_TIMEOUT = 60;
        OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder()
                .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS);


        /* httpClient.addNetworkInterceptor(provideCacheInterceptor());
         *//* httpClient.addInterceptor(provideOfflineCacheInterceptor());*/
        httpClient.addInterceptor(interceptor);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Content-Type", "application/text");
                if (!TextUtils.isEmpty(SharedPreference.getInstance(BaakkiApplication.getAppContext()).getString(Constant.API_TOKEN))) {
                    Log.e("tokean-=","Bearer "+SharedPreference.getInstance(BaakkiApplication.getAppContext()).getString(Constant.API_TOKEN));
                    requestBuilder.addHeader("Authorization", "Bearer "+SharedPreference.getInstance(BaakkiApplication.getAppContext()).getString(Constant.API_TOKEN));
                }


                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        httpClient.cache(cache);

        return httpClient.build();
    }

    private static Interceptor provideCacheInterceptor() {
        return new Interceptor() {
            @NonNull
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                Request request = chain.request();

                Request.Builder requestBuilder = request.newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Content-Type", "application/json");

                // Adding Authorization token (API Key)
                // Requests will be denied without API key
                if (!TextUtils.isEmpty(SharedPreference.getInstance(BaakkiApplication.getAppContext()).getString(Constant.API_TOKEN))) {
                    requestBuilder.addHeader("Token", SharedPreference.getInstance(BaakkiApplication.getAppContext()).getString(Constant.API_TOKEN));
                }

                Response originalResponse = chain.proceed(requestBuilder.build());


                String cacheControl = originalResponse.header("Cache-Control");

                if (cacheControl == null || cacheControl.contains("no-store") || cacheControl.contains("no-cache") ||
                        cacheControl.contains("must-revalidate") || cacheControl.contains("max-stale=0")) {

                    CacheControl cc = new CacheControl.Builder()
                            .maxStale(1, TimeUnit.DAYS)
                            .build();

                    return originalResponse.newBuilder()
                            .header("Cache-Control", "public, max-stale=" + 60 * 60 * 24)
                            .removeHeader("Pragma")
                            .build();

                   /* request = request.newBuilder()
                            .cacheControl(cc)
                            .build();

                    return chain.proceed(request);*/

                } else {
                    return originalResponse;
                }
            }
        };
    }


    private static Interceptor provideOfflineCacheInterceptor() {
        return new Interceptor() {
            @NonNull
            @Override
            public Response intercept(Chain chain) throws IOException {
                try {
                    return chain.proceed(chain.request());
                } catch (Exception e) {
                    CacheControl cacheControl = new CacheControl.Builder()
                            .onlyIfCached()
                            .maxStale(1, TimeUnit.DAYS)
                            .build();

                    Request offlineRequest = chain.request().newBuilder()
                            .cacheControl(cacheControl)
                            .build();
                    return chain.proceed(offlineRequest);
                }
            }
        };
    }
}
