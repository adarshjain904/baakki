package com.app.baakki.business;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.databinding.ItemOrderDetailBinding;
import com.app.baakki.databinding.ItemViewOrderBinding;
import com.app.baakki.model.vieworder.Alluserdatum;

import java.util.List;


public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.ViewHolder> {
    private Context context;
    List<Alluserdatum> dataModelList;

    public OrderDetailAdapter(Context ctx) {
        context = ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemOrderDetailBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_order_detail, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemRowBinding.setData(dataModelList.get(position));
        holder.itemRowBinding.tvOrderId.setText("ORDER ID - #CD10000" + dataModelList.get(position).getCampaintimesCid());

        holder.itemRowBinding.tvQuantity.setText("Quantity - " + dataModelList.get(position).getQuantity());
        if (dataModelList.get(position).getMobile() != null)
            holder.itemRowBinding.tvNumber.setText("Phone Number - " + dataModelList.get(position).getMobile());
        else
            holder.itemRowBinding.tvNumber.setText("Phone Number - Not available");

        if (dataModelList.get(position).getOrderStatus().equals("complete")) {
            holder.itemRowBinding.tvStatus.setText("Completed");
            holder.itemRowBinding.tvStatus.setTextColor(context.getResources().getColor(R.color.green));
        } else if (dataModelList.get(position).getOrderStatus().equals("reserve")) {
            holder.itemRowBinding.tvStatus.setText("Reserve");
            holder.itemRowBinding.tvStatus.setTextColor(context.getResources().getColor(R.color.icon_gray));
        } else {
            holder.itemRowBinding.tvStatus.setText("Cancel");
            holder.itemRowBinding.tvStatus.setTextColor(context.getResources().getColor(R.color.red));
        }


        holder.itemRowBinding.executePendingBindings();
    }

    public void setData(List<Alluserdatum> dataModelList) {
        this.dataModelList = dataModelList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dataModelList == null ? 0 : dataModelList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemOrderDetailBinding itemRowBinding;

        public ViewHolder(ItemOrderDetailBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }


}