package com.app.baakki.business;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.databinding.ItemListShopBinding;
import com.app.baakki.listener.OnShopClickListener;
import com.app.baakki.model.shoplist.ShopListData;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.ViewHolder> {
    private Context context;
    List<ShopListData> dataModelList;
    ItemListShopBinding binding;
    private OnShopClickListener listener;
    private int selectedPos = 0;

    public ShopAdapter(Context ctx, OnShopClickListener listener) {
        context = ctx;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_list_shop, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemRowBinding.setHandler(new MyClickHandlers());
        holder.itemRowBinding.setShoplist(dataModelList.get(position));
        holder.itemRowBinding.setPos(position);
        holder.itemRowBinding.setChecked(selectedPos == position ? true : false);
    }

    public void setData(List<ShopListData> dataModelList) {
        this.dataModelList = dataModelList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dataModelList == null ? 0 : dataModelList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemListShopBinding itemRowBinding;

        public ViewHolder(ItemListShopBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }

    @BindingAdapter("app:srcShopPhoto")
    public static void srcShopPhoto(ImageView view, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            RequestOptions options = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.iv_shop_placeholder);
            Glide.with(view.getContext())
                    .load(imageUrl)
                    .apply(options)
                    .into(view);
        }
    }

    public class MyClickHandlers {
        public void onSelectListClick(int pos) {
            if (listener != null) {
                if (pos != RecyclerView.NO_POSITION) {
                    listener.onItemSelected(dataModelList.get(pos));
                    notifyItemChanged(selectedPos);
                    selectedPos = pos;
                    notifyItemChanged(selectedPos);
                }
            }
        }
    }
}