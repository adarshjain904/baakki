package com.app.baakki.business;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.app.baakki.R;
import com.app.baakki.databinding.ActivityBusinessHomeBinding;
import com.app.baakki.databinding.ActivityHomeBinding;
import com.app.baakki.login.SocialLoginActivity;
import com.app.baakki.model.category.CategoryRes;
import com.app.baakki.model.mycampaignlist.MyCampaignListRes;
import com.app.baakki.model.subcategory.SubcategoryRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.app.baakki.util.PaginationListener.PAGE_START;

public class HomeBusinessActivity extends AppCompatActivity  {
    Context context;
    ProgressLoader progressLoader;
    CompositeDisposable compositeDisposable;
    SharedPreference sharedPreference;
    ActivityBusinessHomeBinding activityBinding;
   public static NavController navController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_business_home);
        init();
    }

    private void init() {
        context = HomeBusinessActivity.this;
        progressLoader = new ProgressLoader(context);
        compositeDisposable=new CompositeDisposable();
        sharedPreference = SharedPreference.getInstance(context);

         navController = Navigation.findNavController(this, R.id.nav_fragment);
        NavigationUI.setupWithNavController(activityBinding.bnvTab, navController);
    }
}
