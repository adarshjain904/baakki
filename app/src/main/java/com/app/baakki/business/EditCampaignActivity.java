package com.app.baakki.business;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavGraph;
import androidx.navigation.Navigation;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.bottomsheet.CalenderBottomSheet;
import com.app.baakki.bottomsheet.EndTimeBottomSheet;
import com.app.baakki.bottomsheet.StartTimeBottomSheet;
import com.app.baakki.databinding.ActivityBusinessEditCampaignBinding;
import com.app.baakki.listener.CalenderDataListener;
import com.app.baakki.listener.EndTimeListener;
import com.app.baakki.listener.StartTimeListener;
import com.app.baakki.model.DateModel;
import com.app.baakki.model.addcampaignres.AddCampaignRes;
import com.app.baakki.model.category.CategoryData;
import com.app.baakki.model.category.CategoryRes;
import com.app.baakki.model.mycampaignlist.MyCampaignListData;
import com.app.baakki.model.subcategory.SubcategoryData;
import com.app.baakki.model.subcategory.SubcategoryRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.app.baakki.business.HomeBusinessActivity.navController;
import static com.app.baakki.util.Constant.EDIT_CAMP_RESULT;
import static com.app.baakki.util.Constant.SHOP_LIST_RESULT;
import static com.app.baakki.util.ManifestPermission.hasPermissions;


public class EditCampaignActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {
    String categoryId, subcategoryId, campaignType = Constant.ONE_TIME, picturePath, orignialPrice, offerPrice, startServerTiming, endServerTiming,
            campaignHeading, stockQuantity, description, howToRedeem, campaignDateJson;

    Context context;
    CompositeDisposable compositeDisposable;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityBusinessEditCampaignBinding dataBiding;
    ArrayList<String> categoryNameArray = new ArrayList<>();

    ArrayList<String> dateServerArray = new ArrayList<>();
    List<CategoryData> categoryData = new ArrayList<>();
    List<SubcategoryData> subcategoryData = new ArrayList<>();
    String[] PERMISSIONS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };
    int PERMISSION_ALL = 1;
    MyCampaignListData myCampaignListData;
    ArrayAdapter dataAdapter, subDataAdapter;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataBiding = DataBindingUtil.setContentView(this, R.layout.activity_business_edit_campaign);
        init();
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void init() {
        context = EditCampaignActivity.this;
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        dataBiding.setHandlers(new MyClickHandlers());
        dataBiding.spCategory.setOnItemSelectedListener(this);
        dataBiding.spSubcategory.setOnItemSelectedListener(this);

        categoryService();
    }

    public class MyClickHandlers implements CalenderDataListener, StartTimeListener, EndTimeListener {
        public void onDateClick(View view) {
            new CalenderBottomSheet(this, true, myCampaignListData.getCampaigntimes()).show(getSupportFragmentManager(), "CalenderBottomSheet");
        }

        public void onStartTimeClick(View view) {
            new StartTimeBottomSheet(this, true, myCampaignListData.getCampaigntimes()).show(getSupportFragmentManager(), "StartTimeBottomSheet");
        }

        public void onEndTimeClick(View view) {
            new EndTimeBottomSheet(this, true, myCampaignListData.getCampaigntimes()).show(getSupportFragmentManager(), "EndTimeBottomSheet");
        }

        public void onTimeClick(View view) {
            Navigation.findNavController(EditCampaignActivity.this, R.id.nav_fragment).navigateUp();
        }


        @RequiresApi(api = Build.VERSION_CODES.M)
        public void onImageClick(View view) {
            selectImage();
        }

        public void onBackClick(View view) {
            finish();
        }


        public void onScheduleClick(View view) {
            if (isValid())
                editCampaignService();

        }

        public void onShopListClick(View view) {
            Intent intent = new Intent(context, ShopListActivity.class);
            startActivityForResult(intent, SHOP_LIST_RESULT);
        }

        public void onRecurringClick(CompoundButton button, Boolean check) {
           /* if (check) {
                campaignType = Constant.RECURRING;
            }*/
        }

        public void onOneTimeClick(CompoundButton button, Boolean check) {
            /*if (check) {
                campaignType = Constant.ONE_TIME;
            }
       */ }


        @Override
        public void calenderDataListener(ArrayList<String> dataArray, String date) {
            dateServerArray = dataArray;
            dataBiding.tvDate.setText(date);
        }

        @Override
        public void startTimeListener(String startTiming) {
            startServerTiming = startTiming;
            SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
            SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
            try {
                dataBiding.tvStartTime.setText(parseFormat.format(displayFormat.parse(startTiming)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void endTimeListener(String endTiming) {
            endServerTiming = endTiming;
            SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
            SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
            try {
                dataBiding.tvEndTime.setText(parseFormat.format(displayFormat.parse(endTiming)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isValid() {
        boolean isValid = true;

        campaignHeading = dataBiding.etTitle.getText().toString();
        stockQuantity = dataBiding.etAvailability.getText().toString();
        orignialPrice = dataBiding.etOriginalPrice.getText().toString();
        offerPrice = dataBiding.etOfferPrice.getText().toString();
        description = dataBiding.etDetail.getText().toString();
        howToRedeem = dataBiding.etRedmee.getText().toString();

       if (TextUtils.isEmpty(campaignHeading)) {
            Toast.makeText(context, "Enter campaign title", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (dataBiding.tvDate.getText().equals("Select Data")) {
            Toast.makeText(context, "Please select date", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (dateServerArray.size() == 0) {
            Toast.makeText(context, "Please select start date", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (TextUtils.isEmpty(startServerTiming)) {
            Toast.makeText(context, "Please select start date", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (TextUtils.isEmpty(endServerTiming)) {
            Toast.makeText(context, "Please select end date", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (TextUtils.isEmpty(description)) {
            Toast.makeText(context, "Enter product detail", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (Integer.parseInt(stockQuantity) == 0) {
            Toast.makeText(context, "Enter product availability", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (Integer.parseInt(orignialPrice) == 0) {
            Toast.makeText(context, "Enter original price", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (Integer.parseInt(offerPrice) == 0) {
            Toast.makeText(context, "Enter offer price", Toast.LENGTH_SHORT).show();
            isValid = false;
        }else if (Integer.parseInt(orignialPrice)<Integer.parseInt(offerPrice)) {
            Toast.makeText(context, "Original Price greater then Offer Price", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (TextUtils.isEmpty(howToRedeem)) {
            Toast.makeText(context, "Enter redeem detail", Toast.LENGTH_SHORT).show();
            isValid = false;
        }
        return isValid;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.sp_category:
                categoryId = String.valueOf(categoryData.get(position).getId());
                subcategoryService(categoryId);
                break;
            case R.id.sp_subcategory:
                subcategoryId = String.valueOf(subcategoryData.get(position).getId());
                break;
        }
    }

    public void onNothingSelected(AdapterView<?> arg0) {
    }

    private void categoryService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).categoryApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CategoryRes>() {
                    @Override
                    public void onSuccess(@NonNull CategoryRes categoryRes) {
                        progressLoader.dismiss();
                        handleCategoryResponse(categoryRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                        Log.e("onError: ", e.getMessage());
                    }
                }));
    }


    private void subcategoryService(String categoryId) {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).subcategoryApi(categoryId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<SubcategoryRes>() {
                    @Override
                    public void onSuccess(@NonNull SubcategoryRes subcategoryRes) {
                        progressLoader.dismiss();
                        handleResponse(subcategoryRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                        Log.e("onError: ", e.getMessage());
                    }
                }));
    }


    private void handleCategoryResponse(CategoryRes categoryRes) {
        if (categoryRes.getStatus()) {
            categoryData = categoryRes.getData();
            for (int i = 0; i < categoryRes.getData().size(); i++) {
                categoryNameArray.add(categoryRes.getData().get(i).getName());
            }

            dataBiding.tvHintCategory.setText("");
            dataAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, categoryNameArray);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            dataBiding.spCategory.setAdapter(dataAdapter);
        } else {
            dataBiding.tvHintCategory.setText(R.string.not_available_category);
        //    Toast.makeText(context, categoryRes.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void handleResponse(SubcategoryRes subcategoryRes) {
        subcategoryData.clear();
        ArrayList<String> subcategoryNameArray = new ArrayList<>();
        if (subcategoryRes.getStatus()) {
            subcategoryData = subcategoryRes.getData();
            for (int i = 0; i < subcategoryRes.getData().size(); i++) {
                subcategoryNameArray.add(subcategoryRes.getData().get(i).getName());
            }

            dataBiding.tvHintSubcategory.setText("");
        } else {
            dataBiding.tvHintSubcategory.setText(R.string.not_available_subcategory);
          //  Toast.makeText(context, subcategoryRes.getMessage(), Toast.LENGTH_SHORT).show();
        }

        subDataAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, subcategoryNameArray);
        subDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dataBiding.spSubcategory.setAdapter(subDataAdapter);

        setData();
    }

    private void setData() {
        myCampaignListData = getIntent().getParcelableExtra("MyCampaignListData");
        dataBiding.setData(myCampaignListData);

        if (myCampaignListData.getCampaignType().equals("onetime")) {
            dataBiding.rbOneTimeCampaign.setText("One-time Campaign");

            campaignType = Constant.ONE_TIME;
        } else {
            dataBiding.rbOneTimeCampaign.setText("Recurring Campaign");
           // dataBiding.rbRecurring.setChecked(true);
            campaignType = Constant.RECURRING;
        }

        String categoryName = null, subCategoryName = null;
        for (int i = 0; i < categoryData.size(); i++) {
            if (myCampaignListData.getCategoryId().equals(categoryData.get(i).getId())) {
                categoryName = categoryData.get(i).getName();
            }
        }

        for (int i = 0; i < subcategoryData.size(); i++) {
            if (myCampaignListData.getSubcategoryId().equals(subcategoryData.get(i).getId())) {
                subCategoryName = subcategoryData.get(i).getName();
            }
        }

        int categoryValue = dataAdapter.getPosition(categoryName);
        int subCategoryValue = subDataAdapter.getPosition(subCategoryName);
        dataBiding.spCategory.setSelection(categoryValue);
        dataBiding.spSubcategory.setSelection(subCategoryValue);

        categoryId = String.valueOf(myCampaignListData.getCategoryId());
        subcategoryId = String.valueOf(myCampaignListData.getSubcategoryId());

        picturePath ="";

        ArrayList<DateModel> dateModelList = new ArrayList<>();
        dateServerArray.clear();
        for (int i = 0; i < myCampaignListData.getCampaigntimes().size(); i++) {
            dateServerArray.add(myCampaignListData.getCampaigntimes().get(i).getDate());
        }
        startServerTiming = myCampaignListData.getCampaigntimes().get(0).getFromTime();
        endServerTiming = myCampaignListData.getCampaigntimes().get(0).getToTime();
        campaignDateJson = new Gson().toJson(dateModelList);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void selectImage() {
        if (!hasPermissions(EditCampaignActivity.this, PERMISSIONS)) {
            boolean showRationale = shouldShowRequestPermissionRationale(Manifest.permission.CAMERA);
            if (!showRationale) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            } else {
                ActivityCompat.requestPermissions(EditCampaignActivity.this, PERMISSIONS, PERMISSION_ALL);
            }
        } else {
            onSelectImageClick();
        }
    }

    public void onSelectImageClick() {
        CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).start(this);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SHOP_LIST_RESULT && resultCode == RESULT_OK) {

        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            dataBiding.ivShopImage.setImageDrawable(null);
            dataBiding.ivShopImage.setImageURI(result.getUri());

            picturePath = result.getUri().getPath();
            Log.e("picturepath", picturePath);
        }
    }

    private void editCampaignService() {
        MultipartBody.Part image = null;
        HashMap<String, RequestBody> data = new HashMap<>();
        data.put("campaign_id", createRequestBody(String.valueOf(myCampaignListData.getId())));
        data.put("category_id", createRequestBody(categoryId));
        data.put("subcategory_id", createRequestBody(subcategoryId));
        data.put("campaign_type", createRequestBody(campaignType));
        data.put("original_price", createRequestBody(orignialPrice));
        data.put("offer_price", createRequestBody(offerPrice));
        data.put("campaign_heading", createRequestBody(campaignHeading));
        data.put("stock_quantity", createRequestBody(stockQuantity));
        data.put("description", createRequestBody(description));
        data.put("how_to_redeem", createRequestBody(howToRedeem));

        ArrayList<DateModel> dateModelList = new ArrayList<>();
        for (int i = 0; i < dateServerArray.size(); i++) {
            DateModel dateModel = new DateModel();
            dateModel.setDate(dateServerArray.get(i));
            dateModel.setTo_time(endServerTiming);
            dateModel.setFrom_time(startServerTiming);
            dateModelList.add(dateModel);
        }
        campaignDateJson = new Gson().toJson(dateModelList);
        data.put("extend_campaign_date_time_json", createRequestBody(campaignDateJson));

        if (!TextUtils.isEmpty(picturePath)) {
            File file = new File(picturePath);
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            image = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        }


        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).updateCampaignApi(data, image)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<AddCampaignRes>() {
                    @Override
                    public void onSuccess(@NonNull AddCampaignRes addCampaignRes) {
                     //   Toast.makeText(context, addCampaignRes.getMessage(), Toast.LENGTH_SHORT).show();

                       Intent intent=new Intent();
                       setResult(EDIT_CAMP_RESULT,intent);
                       finish();

                        progressLoader.dismiss();

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context,e.getMessage());
                    }
                }));
    }


    public RequestBody createRequestBody(@NonNull String s) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), s);
    }


    @BindingAdapter("app:srcEditProductPhoto")
    public static void srcEditProductPhoto(ImageView view, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            RequestOptions options = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.iv_shop_placeholder);

            Glide.with(view.getContext())
                    .load(imageUrl)
                    .apply(options)

                    .into(view);
        }
    }


    @BindingAdapter("bindServerDate")
    public static void bindServerDate(@NonNull TextView textView, String date) {
        if (!TextUtils.isEmpty(date)) {
            SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
            SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
            try {
                textView.setText(parseFormat.format(displayFormat.parse(date)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }
}
