package com.app.baakki.business;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.app.baakki.R;
import com.app.baakki.databinding.FragmentBusinessViewOrderBinding;
import com.app.baakki.databinding.FragmentDiscoverBinding;
import com.app.baakki.discover.DiscoverCategoryAdapter;
import com.app.baakki.discover.DiscoverFragment;
import com.app.baakki.model.shoplist.ShopListRes;
import com.app.baakki.model.vieworder.ViewOrderRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ViewDetailFragment extends Fragment {
    FragmentBusinessViewOrderBinding dataBiding;
    Context context;
    CompositeDisposable compositeDisposable;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ViewOrderAdapter viewOrderAdapter;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.fragment_business_view_order, null, false);
        init();
        return dataBiding.getRoot();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void init() {
        context = getActivity();
        compositeDisposable=new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);
        dataBiding.setHandlers(new MyClickHandlers());

        viewOrderAdapter = new ViewOrderAdapter(context);
        dataBiding.setBusinessViewOrderAdapter(viewOrderAdapter);

        dataBiding.includeCampaign.tvNoCampaignDes.setVisibility(View.GONE);
        dataBiding.includeCampaign.tvDontCampaign.setText("No Orders");

        shopListService();
    }


    public class MyClickHandlers {
        public void onBackClick(View view) {
            Navigation.findNavController(getActivity(), R.id.nav_fragment).navigateUp();
        }
    }

    private void shopListService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).orderlistApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ViewOrderRes>() {
                    @Override
                    public void onSuccess(@NonNull ViewOrderRes viewOrderRes) {
                        progressLoader.dismiss();
                        handleResponse(viewOrderRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                        Log.e("onError: ", e.getMessage());
                    }
                }));
    }

    private void handleResponse(ViewOrderRes viewOrderRes) {
        if (viewOrderRes.getStatus()) {
            dataBiding.includeCampaign.llNoItem.setVisibility(View.GONE);
            dataBiding.rvCategory.setVisibility(View.VISIBLE);
            viewOrderAdapter.setData(viewOrderRes.getUserData(),viewOrderRes);
        } else {
            dataBiding.includeCampaign.llNoItem.setVisibility(View.VISIBLE);
            dataBiding.rvCategory.setVisibility(View.GONE);
        }
    }

}
