package com.app.baakki.business;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityBusinessOrderDetailBinding;
import com.app.baakki.model.vieworder.Alluserdatum;
import com.app.baakki.model.vieworder.UserDatum;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.List;

import static com.app.baakki.util.Constant.serverDate;

public class OrderDetailActivity extends BaseActivity {
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityBusinessOrderDetailBinding activityBinding;
    OrderDetailAdapter orderDetailAdapter;
    UserDatum dataModelList;
    List<Alluserdatum> alluserdata;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_business_order_detail);
        setToolbar(activityBinding.tool.ivBack, activityBinding.tool.tvTitle, activityBinding.tool.vwLine, getString(R.string.order_detail));
        init();
    }

    private void init() {
        context = OrderDetailActivity.this;
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        orderDetailAdapter=new OrderDetailAdapter(context);
        activityBinding.setOrderDetailAdapter(orderDetailAdapter);

        dataModelList=getIntent().getParcelableExtra("data");
      //  String dataGson=getIntent().getStringExtra("userData");
        alluserdata=ViewOrderAdapter.alluserdata;
        activityBinding.setData(dataModelList);

        activityBinding.tvOriginalPrice.setPaintFlags( activityBinding.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        activityBinding.tvDiscoverTime.setText(serverDate(dataModelList
                .getFromTime()) + " to " + serverDate(dataModelList.getToTime()));
        int diff = (Integer.parseInt(dataModelList.getOriginalPrice())
                - Integer.parseInt(dataModelList.getOfferPrice())) * 100;
        double percentage = (diff) / Integer.parseInt(dataModelList.getOriginalPrice());
        activityBinding.tvPercentage.setText(String.valueOf((int) percentage) + "% off");

        orderDetailAdapter.setData(alluserdata);
    }
}
