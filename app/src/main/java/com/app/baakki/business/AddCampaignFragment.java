package com.app.baakki.business;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavGraph;
import androidx.navigation.Navigation;

import com.app.baakki.R;
import com.app.baakki.bottomsheet.BusinessCategoryBottomSheet;
import com.app.baakki.bottomsheet.BusinessSubcategoryBottomSheet;
import com.app.baakki.bottomsheet.CalenderBottomSheet;
import com.app.baakki.bottomsheet.EndTimeBottomSheet;
import com.app.baakki.bottomsheet.StartTimeBottomSheet;
import com.app.baakki.databinding.FragmentBusinessAddCampaignBinding;
import com.app.baakki.listener.CalenderDataListener;
import com.app.baakki.listener.EndTimeListener;
import com.app.baakki.listener.OnCategoryClickListener;
import com.app.baakki.listener.OnSubcategoryClickListener;
import com.app.baakki.listener.StartTimeListener;
import com.app.baakki.model.DateModel;
import com.app.baakki.model.addcampaignres.AddCampaignRes;
import com.app.baakki.model.category.CategoryData;
import com.app.baakki.model.category.CategoryRes;
import com.app.baakki.model.subcategory.SubcategoryData;
import com.app.baakki.model.subcategory.SubcategoryRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static com.app.baakki.business.HomeBusinessActivity.navController;
import static com.app.baakki.util.Constant.SHOP_LIST_RESULT;
import static com.app.baakki.util.ManifestPermission.hasPermissions;

public class AddCampaignFragment extends Fragment {
    String categoryId, subcategoryId, campaignType = Constant.ONE_TIME, picturePath,
            orignialPrice = "0", offerPrice = "0", startServerTiming, endServerTiming,cancellation="0",
            campaignHeading, stockQuantity = "0", description, howToRedeem, campaignDateJson;

    Context context;
    CompositeDisposable compositeDisposable;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    FragmentBusinessAddCampaignBinding dataBiding;

    ArrayList<String> dateServerArray = new ArrayList<>();
    List<CategoryData> categoryData = new ArrayList<>();
    List<SubcategoryData> subcategoryData = new ArrayList<>();
    String[] PERMISSIONS = {
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.CAMERA
    };
    int PERMISSION_ALL = 1;
    Date startDate, endDate, currentDate;
    String currentTime;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.fragment_business_add_campaign, null, false);
        init();
        return dataBiding.getRoot();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void init() {
        context = getActivity();
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        dataBiding.setHandlers(new MyClickHandlers());


        categoryService();
    }

    public class MyClickHandlers implements CalenderDataListener, StartTimeListener, EndTimeListener, OnCategoryClickListener, OnSubcategoryClickListener {
        public void onDateClick(View view) {
            new CalenderBottomSheet(this, false, null).show(getActivity().getSupportFragmentManager(), "CalenderBottomSheet");
        }

        public void onStartTimeClick(View view) {
            new StartTimeBottomSheet(this, false, null).show(getActivity().getSupportFragmentManager(), "StartTimeBottomSheet");
        }

        public void onEndTimeClick(View view) {
            new EndTimeBottomSheet(this, false, null).show(getActivity().getSupportFragmentManager(), "EndTimeBottomSheet");
        }

        public void onSelectCategoryClick(View view) {
            new BusinessCategoryBottomSheet(this, categoryData).show(getActivity().getSupportFragmentManager(), "BusinessCategoryBottomSheet");
        }


        public void onSelectSubcategoryClick(View view) {
            if (TextUtils.isEmpty(categoryId))
                Toast.makeText(context, "Please Select Category", Toast.LENGTH_SHORT).show();
            else if (subcategoryData.size() > 0)
                new BusinessSubcategoryBottomSheet(this, subcategoryData).show(getActivity().getSupportFragmentManager(), "BusinessSubcategoryBottomSheet");
            else
                Toast.makeText(context, R.string.not_available_category, Toast.LENGTH_SHORT).show();
        }


        @RequiresApi(api = Build.VERSION_CODES.M)
        public void onImageClick(View view) {
            selectImage();
        }

        public void onBackClick(View view) {
            Navigation.findNavController(getActivity(), R.id.nav_fragment).navigateUp();
        }

        public void onScheduleClick(View view) {
            if (isValid())
                addCampaignService();
        }

        public void onShopListClick(View view) {
            Intent intent = new Intent(context, ShopListActivity.class);
            startActivityForResult(intent, SHOP_LIST_RESULT);
        }

        public void onRecurringClick(CompoundButton button, Boolean check) {
            if (check) {
                campaignType = Constant.RECURRING;
            }
        }

        public void onOneTimeClick(CompoundButton button, Boolean check) {
            if (check) {
                campaignType = Constant.ONE_TIME;
            }
        }


        @Override
        public void calenderDataListener(ArrayList<String> dataArray, String date) {
            dateServerArray = dataArray;
            dataBiding.tvDate.setText(date);
        }

        @Override
        public void startTimeListener(String startTiming) {
            startServerTiming = startTiming;
            SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
            SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
            try {
                dataBiding.tvStartTime.setText(parseFormat.format(displayFormat.parse(startTiming)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void endTimeListener(String endTiming) {
            endServerTiming = endTiming;
            SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
            SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
            try {
                dataBiding.tvEndTime.setText(parseFormat.format(displayFormat.parse(endTiming)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onCategoryItemSelected(CategoryData item) {
            dataBiding.tvCategory.setText(item.getName());
            categoryId = String.valueOf(item.getId());
            subcategoryService(categoryId);

        }

        @Override
        public void onSubcategoryItemSelected(SubcategoryData item) {
            dataBiding.tvSubcategory.setText(item.getName());
            subcategoryId = String.valueOf(item.getId());
        }
    }

    private boolean isValid() {
        boolean isValid = true;

        campaignHeading = dataBiding.etTitle.getText().toString();
        stockQuantity = dataBiding.etAvailability.getText().toString();
        orignialPrice = dataBiding.etOriginalPrice.getText().toString();
        offerPrice = dataBiding.etOfferPrice.getText().toString();
        description = dataBiding.etDetail.getText().toString();
        howToRedeem = dataBiding.etRedmee.getText().toString();
        cancellation = dataBiding.etCancel.getText().toString();


        try {
            if (endServerTiming != null) {
                endDate = new SimpleDateFormat("HH:mm").parse(endServerTiming);
            }
            if (startServerTiming != null) {
                startDate = new SimpleDateFormat("HH:mm").parse(startServerTiming);
            }
            currentTime = new SimpleDateFormat("HH:mm a").format(Calendar.getInstance().getTime());
            currentDate = new SimpleDateFormat("HH:mm").parse(currentTime);
            Log.e("currentTime", currentTime + "");
        } catch (ParseException e) {
            e.printStackTrace();
        }


        if (TextUtils.isEmpty(picturePath)) {
            Toast.makeText(context, "Select campaign image", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (TextUtils.isEmpty(campaignHeading)) {
            Toast.makeText(context, "Enter campaign title", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (dataBiding.tvDate.getText().equals("Select Data")) {
            Toast.makeText(context, "Please select date", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (dateServerArray.size() == 0) {
            Toast.makeText(context, "Please select start date", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (TextUtils.isEmpty(startServerTiming)) {
            Toast.makeText(context, "Please select start date", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (!isStartValidDate()) {
            Toast.makeText(context, "Start time must be greater then current time ", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (TextUtils.isEmpty(endServerTiming)) {
            Toast.makeText(context, "Please select end date", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (!isEndValidDate()) {
            Toast.makeText(context, "End time must be greater then start time and end time must be enter before 12 Am", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (endDate.before(startDate) || endDate.equals(startDate)) {
            Toast.makeText(context, "End time must be greater then start time and end time must be enter before 12 Am", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (TextUtils.isEmpty(categoryId)) {
            Toast.makeText(context, "Please select category", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (TextUtils.isEmpty(description)) {
            Toast.makeText(context, "Enter product detail", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (TextUtils.isEmpty(stockQuantity)) {
            Toast.makeText(context, "Enter availability quantity", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (Integer.parseInt(stockQuantity) == 0) {
            Toast.makeText(context, "Availability quantity must be greater than 0", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (TextUtils.isEmpty(orignialPrice)) {
            Toast.makeText(context, "Enter original price", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (Integer.parseInt(orignialPrice) == 0) {
            Toast.makeText(context, "Price must be greater than 0", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (TextUtils.isEmpty(orignialPrice)) {
            Toast.makeText(context, "Enter offer price", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (Integer.parseInt(offerPrice) == 0) {
            Toast.makeText(context, "Price must be greater than 0", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (Integer.parseInt(orignialPrice) < Integer.parseInt(offerPrice)) {
            Toast.makeText(context, "Original Price greater then Offer Price", Toast.LENGTH_SHORT).show();
            isValid = false;
        }else if (TextUtils.isEmpty(cancellation)) {
            Toast.makeText(context, "Enter cancellation time", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (Integer.parseInt(cancellation) == 0) {
            Toast.makeText(context, "Cancellation time must be greater than 0", Toast.LENGTH_SHORT).show();
            isValid = false;
        }else if (Integer.parseInt(cancellation)>24) {
            Toast.makeText(context, "Cancellation time under 24 hours", Toast.LENGTH_SHORT).show();
            isValid = false;
        }  else if (TextUtils.isEmpty(howToRedeem)) {
            Toast.makeText(context, "Enter redeem detail", Toast.LENGTH_SHORT).show();
            isValid = false;
        }
        return isValid;
    }

    private boolean isEndValidDate() {
        boolean isValid = true;
        String currentSurverDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        Log.e("currentSurverDate", currentSurverDate);

        for (int i = 0; i < dateServerArray.size(); i++) {
            if (dateServerArray.get(i).equals(currentSurverDate)) {
                if (endDate.before(currentDate) || endDate.equals(currentDate)) {
                    isValid = false;
                } else {
                    isValid = true;
                }
            }
        }
        return isValid;
    }

    public boolean isStartValidDate() {
        boolean isValid = true;
        String currentSurverDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        Log.e("currentSurverDate", currentSurverDate);

        for (int i = 0; i < dateServerArray.size(); i++) {
            if (dateServerArray.get(i).equals(currentSurverDate)) {
                if (startDate.before(currentDate) || startDate.equals(currentDate)) {
                    isValid = false;
                } else {
                    isValid = true;
                }
            }
        }

        return isValid;
    }


    private void categoryService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).categoryApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CategoryRes>() {
                    @Override
                    public void onSuccess(@NonNull CategoryRes categoryRes) {
                        progressLoader.dismiss();
                        handleCategoryResponse(categoryRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                        Log.e("onError: ", e.getMessage());
                    }
                }));
    }


    private void subcategoryService(String categoryId) {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).subcategoryApi(categoryId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<SubcategoryRes>() {
                    @Override
                    public void onSuccess(@NonNull SubcategoryRes subcategoryRes) {
                        progressLoader.dismiss();
                        handleResponse(subcategoryRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                        Log.e("onError: ", e.getMessage());
                    }
                }));
    }


    private void handleCategoryResponse(CategoryRes categoryRes) {
        if (categoryRes.getStatus()) {
            categoryData = categoryRes.getData();


        } else {
            dataBiding.tvCategory.setText(R.string.not_available_category);
            Toast.makeText(context, categoryRes.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void handleResponse(SubcategoryRes subcategoryRes) {
        subcategoryData.clear();
        subcategoryId = "";
        if (subcategoryRes.getStatus()) {
            subcategoryData = subcategoryRes.getData();
            dataBiding.tvSubcategory.setText(R.string.select_sub_category);
        } else {
            dataBiding.tvSubcategory.setText(R.string.not_available_subcategory);
            // Toast.makeText(context, subcategoryRes.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void selectImage() {
        if (!hasPermissions(getActivity(), PERMISSIONS)) {
            /*boolean showRationale = shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA);
            if (!showRationale) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            } else*//* {*/
                ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);

        } else {
            onSelectImageClick();
        }
    }

    public void onSelectImageClick() {
        CropImage.activity().start(getContext(), this);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SHOP_LIST_RESULT && resultCode == RESULT_OK) {

        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            dataBiding.ivShopImage.setImageURI(result.getUri());
            picturePath = result.getUri().getPath();
            Log.e("picturepath", picturePath);

        }
    }

    private void addCampaignService() {
        MultipartBody.Part image = null;
        HashMap<String, RequestBody> data = new HashMap<>();
        data.put("category_id", createRequestBody(categoryId));
        data.put("subcategory_id", createRequestBody(subcategoryId));
        data.put("campaign_type", createRequestBody(campaignType));
        data.put("original_price", createRequestBody(orignialPrice));
        data.put("offer_price", createRequestBody(offerPrice));
        data.put("campaign_heading", createRequestBody(campaignHeading));
        data.put("stock_quantity", createRequestBody(stockQuantity));
        data.put("description", createRequestBody(description));
        data.put("how_to_redeem", createRequestBody(howToRedeem));
        data.put("cancelation_time", createRequestBody(cancellation));

        ArrayList<DateModel> dateModelList = new ArrayList<>();
        for (int i = 0; i < dateServerArray.size(); i++) {
            DateModel dateModel = new DateModel();
            dateModel.setDate(dateServerArray.get(i));
            dateModel.setTo_time(endServerTiming);
            dateModel.setFrom_time(startServerTiming);
            dateModelList.add(dateModel);
        }
        campaignDateJson = new Gson().toJson(dateModelList);
        data.put("campaign_date_json", createRequestBody(campaignDateJson));

        if (picturePath != null) {
            File file = new File(picturePath);
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            image = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        }


        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).addCampaignApi(data, image)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<AddCampaignRes>() {
                    @Override
                    public void onSuccess(@NonNull AddCampaignRes addCampaignRes) {
                      //  Toast.makeText(context, addCampaignRes.getMessage(), Toast.LENGTH_SHORT).show();

                        NavGraph graph = navController.getNavInflater().inflate(R.navigation.nav_business_graph);
                        graph.setStartDestination(R.id.fragment_business_campains);
                        navController.setGraph(graph);

                        progressLoader.dismiss();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }


    public RequestBody createRequestBody(@NonNull String s) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), s);
    }
}
