package com.app.baakki.business;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityBusinessSigninBinding;
import com.app.baakki.databinding.ActivitySigninBinding;
import com.app.baakki.home.HomeActivity;
import com.app.baakki.login.ForgotActivity;
import com.app.baakki.login.SignUpActivity;
import com.app.baakki.model.login.LoginSignupData;
import com.app.baakki.model.login.LoginSignupRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;


public class LoginBusinessActivity extends BaseActivity {
    String email, pass;
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    CompositeDisposable compositeDisposable;
    ActivityBusinessSigninBinding activityBinding;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_business_signin);
        setWhiteTitle(activityBinding.tool.ivBack, activityBinding.tool.tvNeedSomeHelp);
        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    private void init() {
        context = LoginBusinessActivity.this;
        progressLoader = new ProgressLoader(context);
        compositeDisposable = new CompositeDisposable();
        sharedPreference = SharedPreference.getInstance(context);
    }

    private boolean isValid() {
        boolean isValid = true;

        email = activityBinding.etEmail.getText().toString();
        pass = activityBinding.etPass.getText().toString();

        if (TextUtils.isEmpty(email)) {
            activityBinding.etEmail.setError(getString(R.string.enter_email));
            isValid = false;
        } else if (!ValidationUtils.isValidEmail(email)) {
            activityBinding.etEmail.setError(getString(R.string.valid_email));
            isValid = false;
        } else if (TextUtils.isEmpty(pass)) {
            activityBinding.etPass.setError(getString(R.string.enter_password));
            isValid = false;
        }
        return isValid;
    }

    public class MyClickHandlers {
        public void onSignInClick(View view) {
            if (isValid()) {
                loginService();
            }
        }
    }

    private void loginService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).loginApi(email, pass,3)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<LoginSignupRes>() {
                    @Override
                    public void onSuccess(@NonNull LoginSignupRes loginSignupRes) {
                        progressLoader.dismiss();
                        handleResponse(loginSignupRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context,e.getMessage());

                    }
                }));
    }

    private void handleResponse(LoginSignupRes loginSignupRes) {
        if (loginSignupRes.getStatus()) {
            Intent intent = new Intent(context, HomeBusinessActivity.class);
            sharedPreference.putBoolean(Constant.IS_LOGIN_BUSINESS, true);
            sharedPreference.putString(Constant.API_TOKEN, loginSignupRes.getLoginSignupData().getToken());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(context, loginSignupRes.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
