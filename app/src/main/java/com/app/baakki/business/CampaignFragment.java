package com.app.baakki.business;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.baakki.R;
import com.app.baakki.bottomsheet.DeleteCampaignBottomSheet;
import com.app.baakki.bottomsheet.EditCampaignBottomSheet;
import com.app.baakki.bottomsheet.LogoutBottomSheet;
import com.app.baakki.databinding.FragmentBusinessCampainsBinding;
import com.app.baakki.discover.DiscoverCategoryAdapter;
import com.app.baakki.listener.DeleteCampaignListener;
import com.app.baakki.listener.EditCampaignListener;
import com.app.baakki.model.mycampaignlist.MyCampaignListData;
import com.app.baakki.model.mycampaignlist.MyCampaignListRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.PaginationListener;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;
import static com.app.baakki.util.Constant.EDIT_CAMP_REQUEST;
import static com.app.baakki.util.Constant.EDIT_CAMP_RESULT;
import static com.app.baakki.util.PaginationListener.PAGE_START;

public class CampaignFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    FragmentBusinessCampainsBinding dataBiding;
    Context context;
    CompositeDisposable compositeDisposable;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    CampaignAdapter campaignAdapter;

    List<MyCampaignListData> data=new ArrayList<>();
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private boolean isLoading = false;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.fragment_business_campains, null, false);
        init();
        return dataBiding.getRoot();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void init() {
        context = getActivity();
        progressLoader = new ProgressLoader(context);
        compositeDisposable = new CompositeDisposable();
        sharedPreference = SharedPreference.getInstance(context);

        dataBiding.setHandlers(new MyClickHandlers());
        dataBiding.swipeRefresh.setOnRefreshListener(this);

        campaignAdapter = new CampaignAdapter(context,new MyClickHandlers());
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        dataBiding.rvCategory.setLayoutManager(layoutManager);
        dataBiding.setBusinessCampaignsAdapter(campaignAdapter);


        myCampaignService();


        dataBiding.rvCategory.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage++;
                myCampaignService();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }


    private void myCampaignService() {
        if (currentPage == 0) progressLoader.show();
        else  campaignAdapter.addLoading();


        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).myCampaignListApi(currentPage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<MyCampaignListRes>() {
                    @Override
                    public void onSuccess(@NonNull MyCampaignListRes myCampaignListRes) {
                        progressLoader.dismiss();
                        handleResponse(myCampaignListRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        isLastPage = true;
                        campaignAdapter.removeLoading();
                        progressLoader.dismiss();
                        dataBiding.swipeRefresh.setRefreshing(false);
                        ValidationUtils.errorCode(context,e.getMessage());
                    }
                }));
    }

    private void handleResponse(MyCampaignListRes myCampaignListRes) {
        dataBiding.swipeRefresh.setRefreshing(false);
        if (myCampaignListRes.getStatus()) {

            data.addAll(myCampaignListRes.getData());
            campaignAdapter.setData(data);

            isLoading = false;
        } else {
            isLastPage = true;
            campaignAdapter.removeLoading();
            //if (currentPage == 0)  Toast.makeText(context, myCampaignListRes.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    public class MyClickHandlers implements EditCampaignListener, DeleteCampaignListener {
        public void onLogoutClick(View view) {
            new LogoutBottomSheet().show(getActivity().getSupportFragmentManager(), "LogoutBottomSheet");
        }

        public void onEditClick(MyCampaignListData data) {
             new EditCampaignBottomSheet(this,data).show(((AppCompatActivity) context).getSupportFragmentManager(), "EditCampaignBottomSheet");
            /*Intent intent=new Intent(context,EditCampaignActivity.class);
            intent.putExtra("MyCampaignListData",data);
            startActivityForResult(intent,EDIT_CAMP_REQUEST);*/
        }
        public void onDeleteClick(MyCampaignListData data) {
            new DeleteCampaignBottomSheet(this,data).show(getActivity().getSupportFragmentManager(), "DeleteCampaignBottomSheet");
            /*Intent intent=new Intent(context,EditCampaignActivity.class);
            intent.putExtra("MyCampaignListData",data);
            startActivityForResult(intent,EDIT_CAMP_REQUEST);*/
        }

        @Override
        public void editCampaign() {
            currentPage = PAGE_START;
            isLastPage = false;
            data.clear();
            campaignAdapter.clear();
            myCampaignService();
        }

        @Override
        public void deleteCampaign() {
            currentPage = PAGE_START;
            isLastPage = false;
            data.clear();
            campaignAdapter.clear();
            myCampaignService();
        }
    }

    @Override
    public void onRefresh() {
        currentPage = PAGE_START;
        isLastPage = false;
        data.clear();
        campaignAdapter.clear();
        myCampaignService();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==EDIT_CAMP_REQUEST&&resultCode==EDIT_CAMP_RESULT){
            currentPage = PAGE_START;
            isLastPage = false;
            this.data.clear();
            campaignAdapter.clear();
            myCampaignService();
        }
    }

    private void deleteCampaignService(Integer campaignId) {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).deleteCampaignApi(campaignId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<JsonElement>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull JsonElement myCampaignListRes) {

                        handleFavoritesResponse(myCampaignListRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void handleFavoritesResponse(JsonElement jsonElement) {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
     //   Toast.makeText(context, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
        if (jsonObject.get("status").getAsBoolean()) {
            currentPage = PAGE_START;
            isLastPage = false;
            data.clear();
            campaignAdapter.clear();
            myCampaignService();
        } else {
            progressLoader.dismiss();
        }
    }
}
