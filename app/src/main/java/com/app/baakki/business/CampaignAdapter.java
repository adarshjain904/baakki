package com.app.baakki.business;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.bottomsheet.EditCampaignBottomSheet;
import com.app.baakki.databinding.ItemCampaignsBinding;
import com.app.baakki.databinding.ItemLoadingBinding;
import com.app.baakki.discover.ProductAdapter;
import com.app.baakki.model.mycampaignlist.MyCampaignListData;
import com.app.baakki.util.DialogUtility;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;


public class CampaignAdapter extends RecyclerView.Adapter {
    private static final int VIEW_TYPE_LOADING = 0;
    private static final int VIEW_TYPE_NORMAL = 1;
    private boolean isLoaderVisible = false;

    private Context context;
    List<MyCampaignListData> dataModelList;
    CampaignFragment.MyClickHandlers myClickHandlers;

    public CampaignAdapter(Context ctx, CampaignFragment.MyClickHandlers myClickHandlers) {
        context = ctx;
        this.myClickHandlers = myClickHandlers;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ItemCampaignsBinding binding = DataBindingUtil.inflate(LayoutInflater
                        .from(parent.getContext()), R.layout.item_campaigns, parent, false);
                binding.setHandlers(myClickHandlers);
                return new ViewHolder(binding);
            case VIEW_TYPE_LOADING:
                ItemLoadingBinding loadingBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_loading, parent, false);
                return new ProgressHolder(loadingBinding);
            default:
                return null;

        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).itemRowBinding.tvOriginalPrice.setPaintFlags(((ViewHolder) holder).itemRowBinding.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            ((ViewHolder) holder).itemRowBinding.executePendingBindings();
            ((ViewHolder) holder).itemRowBinding.setData(dataModelList.get(position));
            /*  if (DialogUtility.checkExpairVendor(dataModelList.get(position).getCampaignEndDate(), dataModelList.get(position).getCampaignEndTime())) {
                  ((ViewHolder) holder).itemRowBinding.tvRemaining.setText("Expired");
                  ((ViewHolder) holder).itemRowBinding.tvEdit.setVisibility(View.GONE);
              } else*/
            if (dataModelList.get(position).getRemainingQuantity() == null) {
                ((ViewHolder) holder).itemRowBinding.tvRemaining.setText("Sold out ");
                ((ViewHolder) holder).itemRowBinding.tvEdit.setVisibility(View.GONE);
            } else if (Integer.parseInt(dataModelList.get(position).getRemainingQuantity()) == 0) {
                ((ViewHolder) holder).itemRowBinding.tvRemaining.setText("Sold out ");
                ((ViewHolder) holder).itemRowBinding.tvEdit.setVisibility(View.GONE);
            } else{
                String currentSurverDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
                for (int i = 0; i <dataModelList.get(position).getCampaigntimes().size() ; i++) {
                    if (currentSurverDate.equals(dataModelList.get(position).getCampaigntimes().get(i).getDate())){
                        ((ViewHolder) holder).itemRowBinding.tvRemaining
                                .setText(dataModelList.get(position).getCampaigntimes().get(i).getRestQuantity() + " left");
                    }
                }
            }


        }

    }


    public void setData(List<MyCampaignListData> dataModelList) {
        this.dataModelList = dataModelList;
        notifyDataSetChanged();
    }


    @BindingAdapter("app:srcProductPhoto")
    public static void srcProductPhoto(ImageView view, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            RequestOptions options = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.iv_shop_placeholder);
            Glide.with(view.getContext())
                    .load(imageUrl)
                    .apply(options)
                    .into(view);
        }
    }

    @BindingAdapter("app:srcCategoryPhoto")
    public static void srcCategoryPhoto(ImageView view, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            RequestOptions options = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.ic_mcd_logo);
            Glide.with(view.getContext())
                    .load(imageUrl)
                    .apply(options)
                    .into(view);
        }
    }


    @Override
    public int getItemCount() {
        return dataModelList == null ? 0 : dataModelList.size();

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemCampaignsBinding itemRowBinding;

        public ViewHolder(ItemCampaignsBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }


    public class ProgressHolder extends RecyclerView.ViewHolder {
        public ItemLoadingBinding itemLoadingBinding;

        ProgressHolder(ItemLoadingBinding itemLoadingBinding) {
            super(itemLoadingBinding.getRoot());
            this.itemLoadingBinding = itemLoadingBinding;
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (isLoaderVisible) {
            return position == dataModelList.size() ? VIEW_TYPE_LOADING : VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_NORMAL;
        }
    }


    public void addLoading() {
        isLoaderVisible = true;
        notifyDataSetChanged();

    }

    public void removeLoading() {
        isLoaderVisible = false;
        notifyDataSetChanged();

    }

    public void clear() {
        if (dataModelList != null) {
            dataModelList.clear();
            notifyDataSetChanged();
        }
    }


}