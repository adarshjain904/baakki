package com.app.baakki.business;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.databinding.ItemViewOrderBinding;
import com.app.baakki.discover.ProductAdapter;
import com.app.baakki.model.vieworder.Alluserdatum;
import com.app.baakki.model.vieworder.UserDatum;
import com.app.baakki.model.vieworder.ViewOrderRes;
import com.app.baakki.services.ApiClient;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.api.Api;
import com.google.gson.Gson;

import java.util.List;

import static com.app.baakki.util.Constant.serverDate;


public class ViewOrderAdapter extends RecyclerView.Adapter<ViewOrderAdapter.ViewHolder> {
    private Context context;
    List<UserDatum> dataModelList;
    ViewOrderRes viewOrderRes;
    static  List<Alluserdatum> alluserdata;
    public ViewOrderAdapter(Context ctx) {
        context = ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemViewOrderBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_view_order, parent, false);
        binding.setHandlers(new MyClickHandlers());
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemRowBinding.executePendingBindings();
        holder.itemRowBinding.tvOriginalPrice.setPaintFlags( holder.itemRowBinding.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.itemRowBinding.setData(dataModelList.get(position));
        holder.itemRowBinding.setPos(position);
        holder.itemRowBinding.tvOrder.setText("Order - " + viewOrderRes.getTotalOrder().get(position));
        holder.itemRowBinding.tvQuantity.setText("Quantity - " + viewOrderRes.getTotalOrderQuantity().get(position));
        holder.itemRowBinding.tvDiscoverTime.setText(serverDate(dataModelList.get(position)
                .getFromTime()) + " to " + serverDate(dataModelList.get(position).getToTime()));
        int diff = (Integer.parseInt(dataModelList.get(position).getOriginalPrice())
                - Integer.parseInt(dataModelList.get(position).getOfferPrice())) * 100;
        double percentage = (diff) / Integer.parseInt(dataModelList.get(position).getOriginalPrice());
        holder.itemRowBinding.tvPercentage.setText(String.valueOf((int) percentage) + "% off");

    }

    public void setData(List<UserDatum> dataModelList, ViewOrderRes viewOrderRes) {
        this.dataModelList = dataModelList;
        this.viewOrderRes = viewOrderRes;
        notifyDataSetChanged();
    }

    @BindingAdapter("app:srcViewOrderImage")
    public static void srcViewOrderImage(ImageView view, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            RequestOptions options = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.iv_shop_placeholder);
            Glide.with(view.getContext())
                    .load(imageUrl)
                    .apply(options)
                    .into(view);
        }
    }

    public class MyClickHandlers {
        public void onItemClick(int pos) {
            alluserdata=  viewOrderRes.getAlluserdata().get(pos);
            Intent intent = new Intent(context, OrderDetailActivity.class);
            intent.putExtra("data", dataModelList.get(pos));
          /*  intent.putExtra("userData", new Gson().toJson());*/
            context.startActivity(intent);
        }


    }


    @Override
    public int getItemCount() {
        return dataModelList == null ? 0 : dataModelList.size();

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemViewOrderBinding itemRowBinding;

        public ViewHolder(ItemViewOrderBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }


}