package com.app.baakki.business;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActviityShopListBinding;
import com.app.baakki.listener.OnShopClickListener;
import com.app.baakki.model.shoplist.ShopListData;
import com.app.baakki.model.shoplist.ShopListRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ShopListActivity extends BaseActivity implements OnShopClickListener {
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    CompositeDisposable compositeDisposable;
    ActviityShopListBinding activityBinding;
    ShopAdapter shopAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.actviity_shop_list);
        setToolbar(activityBinding.tool.ivBack, activityBinding.tool.tvTitle, activityBinding.tool.vwLine, getString(R.string.shop_list));

        init();
    }

    private void init() {
        context = ShopListActivity.this;
        progressLoader = new ProgressLoader(context);
        compositeDisposable = new CompositeDisposable();
        sharedPreference = SharedPreference.getInstance(context);

        shopAdapter = new ShopAdapter(context,this);
        activityBinding.setShopListAdapter(shopAdapter);

        shopListService();
    }


    private void shopListService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).shopListApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ShopListRes>() {
                    @Override
                    public void onSuccess(@NonNull ShopListRes shopListRes) {
                        progressLoader.dismiss();
                        handleResponse(shopListRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                        Log.e("onError: ", e.getMessage());
                    }
                }));
    }

    private void handleResponse(ShopListRes shopListRes) {
        if (shopListRes.getStatus()) {
            shopAdapter.setData(shopListRes.getData());
        } else {
            Toast.makeText(context, shopListRes.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onItemSelected(ShopListData item) {
        Toast.makeText(context, item.getBusinessName(), Toast.LENGTH_SHORT).show();
    }
}
