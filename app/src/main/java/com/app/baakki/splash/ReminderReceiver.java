package com.app.baakki.splash;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ReminderReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //do task to show notification
        context.sendBroadcast(new Intent("INTERNET_LOST"));
    }
}
