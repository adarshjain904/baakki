package com.app.baakki.splash;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.AnimationDrawable;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.business.HomeBusinessActivity;

import com.app.baakki.home.HomeActivity;
import com.app.baakki.R;
import com.app.baakki.databinding.ActivitySplashBinding;
import com.app.baakki.home.SelectDistanceActivity;
import com.app.baakki.login.SocialLoginActivity;
import com.app.baakki.util.Constant;
import com.app.baakki.util.DialogUtility;
import com.app.baakki.util.SharedPreference;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashActivity extends AppCompatActivity {
    Context context;
    SharedPreference sharedPreference;
    AnimationDrawable progressAnimation;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 1;
    ActivitySplashBinding activityBinding;
    String[] permissions = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
          };
    int count = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);

        context = SplashActivity.this;
        sharedPreference = SharedPreference.getInstance(context);
        progressAnimation = (AnimationDrawable) activityBinding.ivSplash.getDrawable();
        progressAnimation.setCallback(activityBinding.ivSplash);
        progressAnimation.setVisible(true, true);
        hashkey();

        /*if (!hasPermissions(this, permissions)) {
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
        } else {
            statusCheck();
        }*/
        activityBinding.ivSplash.post(new Starter());
    }

    class Starter implements Runnable {
        public void run() {
            progressAnimation.start();
            openActivity();
        }
    }

    private void openActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressAnimation.stop();
                jump();
            }
        }, 7000);

    }


    private void jump() {
        if (sharedPreference.getBoolean(Constant.IS_SELECT_LOCATION)) {
            startActivity(new Intent(context, HomeActivity.class));
            finish();
        } else if (sharedPreference.getBoolean(Constant.IS_LOGIN)) {
            startActivity(new Intent(context, SelectDistanceActivity.class));
            finish();
        } else if (sharedPreference.getBoolean(Constant.IS_LOGIN_BUSINESS)) {
            startActivity(new Intent(context, HomeBusinessActivity.class));
            finish();
        } else {
            startActivity(new Intent(context, SocialLoginActivity.class));
            finish();
        }
    }

    public boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {

                   /* if (permissions[i].equals(Manifest.permission.CAMERA)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.e("msg", "<--accounts granted");
                            count++;
                        } else {
                           // DialogUtility.showToast(this, "Permission is required. ");
                        }
                    } else if (permissions[i].equals(Manifest.permission.READ_PHONE_STATE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.e("msg", "<--storage granted");
                            count++;
                        } else {
                          //  DialogUtility.showToast(this, "Permission is required...");
                        }
                    } else*/ if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.e("msg", "<--location granted");
                            count++;
                        } else {
                           // DialogUtility.showToast(this, "Permission is required...");
                        }
                    } else if (permissions[i].equals(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.e("msg", "<--read phone granted");
                            count++;
                        } else {
                            //DialogUtility.showToast(this, "Permission is required...");
                        }
                    } /*else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.e("msg", "<--read phone granted");
                            count++;
                        } else {
                           // DialogUtility.showToast(this, "Permission is required...");
                        }
                    } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.e("msg", "<--read phone granted");
                            count++;
                        } else {
                            //DialogUtility.showToast(this, "Permission is required...");
                        }
                    }*/
                }
            }
            statusCheck();
           // activityBinding.ivSplash.post(new Starter());
        }
    }

    public void statusCheck() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean isGpsProviderEnabled, isNetworkProviderEnabled;
        isGpsProviderEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkProviderEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGpsProviderEnabled && !isNetworkProviderEnabled) {
            buildAlertMessageNoGps();
        } else
            activityBinding.ivSplash.post(new Starter());
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Location Permission");
        builder.setMessage("The app needs location permissions. Please grant this permission to continue using the features of the app.");
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 101);
            }
        });

        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==101){
            statusCheck();
        }
    }

    public void hashkey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.app.baakki",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("facebook NameNotFoundEx", e.getMessage());

        } catch (NoSuchAlgorithmException e) {
            Log.e("fb NoSuchAlgorithmEx", e.getMessage());
        }
    }
}