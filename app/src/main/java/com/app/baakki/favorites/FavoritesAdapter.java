package com.app.baakki.favorites;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.databinding.ItemFavBinding;
import com.app.baakki.discover.ProductAdapter;
import com.app.baakki.discover.ProductDetailActivity;
import com.app.baakki.listener.OnFavProductClick;
import com.app.baakki.model.usergetallcampaign.CommonData;
import com.app.baakki.util.Constant;
import com.app.baakki.util.DialogUtility;

import java.text.ParseException;
import java.util.List;


public class FavoritesAdapter extends RecyclerView.Adapter<FavoritesAdapter.ViewHolder> {
    private Context context;
    List<CommonData> dataModelList;
    OnFavProductClick onFavProductClick;

    public FavoritesAdapter(Context ctx, OnFavProductClick onFavProductClick) {
        context = ctx;
        this.onFavProductClick = onFavProductClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemFavBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_fav, parent, false);
        binding.setHandlers(new MyClickHandlers());
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        dataModelList.get(position).setIsLike(1);
        holder.itemRowBinding.tvOriginalPrice.setPaintFlags(holder.itemRowBinding.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.itemRowBinding.executePendingBindings();
        holder.itemRowBinding.setData(dataModelList.get(position));
        holder.itemRowBinding.setPos(position);

        /*if (DialogUtility.checkExpairDate(dataModelList.get(position).getCamExpiry(), dataModelList.get(position).getToTime()))
            (holder).itemRowBinding.tvRemaining.setText("Expired");
        else*/
        if (dataModelList.get(position).getRemainingQuantity() == null) {
            holder.itemRowBinding.tvRemaining.setText("Sold out ");
        } else if (dataModelList.get(position).getRemainingQuantity() == 0) {
            holder.itemRowBinding.tvRemaining.setText("Sold out ");
        } else
            (holder).itemRowBinding.tvRemaining.setText(dataModelList.get(position).getRemainingQuantity() + " left");

    }

    public void setData(List<CommonData> dataModelList) {
        this.dataModelList = dataModelList;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return dataModelList == null ? 0 : dataModelList.size();

    }

    public class MyClickHandlers {
        public void onProductClick(int pos) {
            Intent intent = new Intent(context, ProductDetailActivity.class);
            intent.putExtra("isFav", true);
            intent.putExtra(Constant.PRODUCT_DETAIL_DATA, dataModelList.get(pos));
            context.startActivity(intent);
        }

        public void onFavClick(CommonData commonData) {
            onFavProductClick.onFavProductClick(commonData);
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemFavBinding itemRowBinding;

        public ViewHolder(ItemFavBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }

}