package com.app.baakki.favorites;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.app.baakki.R;
import com.app.baakki.databinding.FragmentFavoritesBinding;
import com.app.baakki.databinding.FragmentOtherBinding;
import com.app.baakki.home.ThankYouActivity;
import com.app.baakki.listener.OnFavProductClick;
import com.app.baakki.model.gelallfav.GetAllFavRes;
import com.app.baakki.model.usergetallcampaign.CommonData;
import com.app.baakki.model.usergetallcampaign.UserGetAllCampaignRes;
import com.app.baakki.other.OtherFragment;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.app.baakki.util.PaginationListener.PAGE_START;


public class FavoritesFragment extends Fragment implements OnFavProductClick {
    FragmentFavoritesBinding dataBiding;
    Context context;
    CompositeDisposable compositeDisposable;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    FavoritesAdapter favoritesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorites, null, false);
        dataBiding.setHandlers(new MyClickHandlers());
        init();
        return dataBiding.getRoot();
    }

    private void init() {
        context = getActivity();
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        favoritesAdapter = new FavoritesAdapter(context, this);
        dataBiding.setFavoritesAdapter(favoritesAdapter);

        dataBiding.includeCampaign.tvDontCampaign.setText("Add your favourites here");
        dataBiding.includeCampaign.tvNoCampaignDes.setText("Click the heart button on the listing to add it to your favourites!");

        // getAllFavoritesService();
    }

    @Override
    public void onFavProductClick(CommonData commonData) {
        addRemoveFavoritesService(Integer.valueOf(commonData.getCampaignId()));
    }

    public class MyClickHandlers {
        public void onBackClick(View view) {
            Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigateUp();
        }
    }

    private void getAllFavoritesService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).getAllFavListApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<GetAllFavRes>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull GetAllFavRes getAllFavRes) {
                        progressLoader.dismiss();
                        handleFavoritesResponse(getAllFavRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void handleFavoritesResponse(GetAllFavRes getAllFavRes) {
        dataBiding.includeCampaign.llNoItem.setVisibility(View.GONE);
        favoritesAdapter.setData(getAllFavRes.getData());
        if (!getAllFavRes.getStatus()) {
            dataBiding.includeCampaign.llNoItem.setVisibility(View.VISIBLE);
           // Toast.makeText(context, "Data not found", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getAllFavoritesService();
    }

    private void addRemoveFavoritesService(Integer campaignId) {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).addRemoveFavoritesApi(campaignId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<JsonElement>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull JsonElement myCampaignListRes) {

                        handleFavoritesResponse(myCampaignListRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void handleFavoritesResponse(JsonElement jsonElement) {
        progressLoader.dismiss();
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        //    Toast.makeText(context, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
        if (jsonObject.get("status").getAsBoolean()) {
            getAllFavoritesService();
        }
    }
}