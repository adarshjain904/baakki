package com.app.baakki.category;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.bottomsheet.CategoryFilterBottomSheet;
import com.app.baakki.bottomsheet.DistanceBottomSheet;
import com.app.baakki.bottomsheet.FilterByBottomSheet;
import com.app.baakki.bottomsheet.LogoutBottomSheet;
import com.app.baakki.bottomsheet.SaveSearchBottomSheet;
import com.app.baakki.bottomsheet.ShortByBottomSheet;
import com.app.baakki.databinding.ActivityCategoryDetailBinding;
import com.app.baakki.discover.ProductAdapter;
import com.app.baakki.listener.LocationHelper;
import com.app.baakki.listener.OnFavProductClick;
import com.app.baakki.listener.OnFilterByClick;
import com.app.baakki.listener.OnSortByClick;
import com.app.baakki.listener.OnSubcategoryClickListener;
import com.app.baakki.listener.SelectDistanceClick;
import com.app.baakki.model.ChooseLocation;
import com.app.baakki.model.category.CategoryData;
import com.app.baakki.model.filterapi.FilterRes;
import com.app.baakki.model.subcategory.SubcategoryData;
import com.app.baakki.model.subcategory.SubcategoryRes;
import com.app.baakki.model.usergetallcampaign.CommonData;
import com.app.baakki.model.usergetallcampaign.UserGelAllCampaignData;
import com.app.baakki.model.usergetallcampaign.UserGetAllCampaignRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.DialogUtility;
import com.app.baakki.util.LocationBuilder;
import com.app.baakki.util.PaginationListener;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationResult;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.app.baakki.util.PaginationListener.PAGE_START;
import static com.facebook.FacebookSdk.getApplicationContext;

public class CategoryDetailActivity extends BaseActivity implements /*LocationHelper,*/ OnFavProductClick {
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityCategoryDetailBinding activityBinding;
    CompositeDisposable compositeDisposable;
    CategoryDetailAdapter categoryDetailAdapter;
    CategoryData categoryData;
    LocationBuilder locationBuilder;
    int distance = 0;
    String lat, lon, address, timeStart, timeEnd, shopName, productName, price, rating,
            ending_soonest, newly_listed, nearest_first, categoryId, subcategoryId, soldHideOn;
    List<SubcategoryData> subcategoryData = new ArrayList<>();
    List<CommonData> commonList = new ArrayList<>();
    boolean isLocation = false;

    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private boolean isLoading = false;

    ChooseLocation chooseLocation;
    List<Place.Field> fields;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_category_detail);

        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    private void init() {
        context = CategoryDetailActivity.this;
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        categoryData = getIntent().getParcelableExtra(Constant.CATEGORY_DATA);
        categoryId = String.valueOf(categoryData.getId());
        activityBinding.tvCategory.setText(categoryData.getName());
        subcategoryService(categoryId);


        Places.initialize(getApplicationContext(), getString(R.string.google_map_key));
        fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);

        chooseLocation = new Gson().fromJson(sharedPreference.getString(Constant.LOCATION_DATA), ChooseLocation.class);
        activityBinding.tvDistanceCity.setText(chooseLocation.getDistance());
        activityBinding.tvSearchCity.setText(chooseLocation.getAddress());

        lat = String.valueOf(chooseLocation.getLatLng().latitude);
        lon = String.valueOf(chooseLocation.getLatLng().longitude);
        if (!TextUtils.isEmpty(chooseLocation.getDistance()))
            distance = Integer.valueOf(chooseLocation.getDistance().substring(0, chooseLocation.getDistance().length() - 3));
        else
            activityBinding.tvDistanceCity.setText(getResources().getString(R.string.select_radius));



        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        activityBinding.rvCategoryFilter.setLayoutManager(layoutManager);
        categoryDetailAdapter = new CategoryDetailAdapter(context, this);
        activityBinding.setCategoryDetailAdapter(categoryDetailAdapter);

        myCampaignFilterService();

        activityBinding.rvCategoryFilter.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage++;
                myCampaignFilterService();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }


    public class MyClickHandlers implements SelectDistanceClick, OnSubcategoryClickListener, OnSortByClick, OnFilterByClick {
        public void onSearchClick(View view) {
            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                    .setCountry(sharedPreference.getString(Constant.COUNTRY_CODE))
                    .build(CategoryDetailActivity.this);
            startActivityForResult(intent, Constant.AUTOCOMPLETE_REQUEST_CODE);
        }

        public void onCategoryClick(View view) {
            new CategoryFilterBottomSheet(this, subcategoryData, categoryData.getName()).show(getSupportFragmentManager(), "CategoryFilterBottomSheet");
        }

        public void onBackClick(View view) {
            finish();
        }

        public void onSaveClick(View view) {
            new SaveSearchBottomSheet().show(getSupportFragmentManager(), "SaveSearchBottomSheet");
        }

        public void onSortByClick(View view) {
            new ShortByBottomSheet(this).show(getSupportFragmentManager(), "SortByBottomSheet");
        }

        public void onFilterClick(View view) {
            new FilterByBottomSheet(this).show(getSupportFragmentManager(), "FilterByBottomSheet");
        }

        public void onDistanceClick(View view) {
            new DistanceBottomSheet(this).show(getSupportFragmentManager(), "DistanceBottomSheet");
        }


        @Override
        public void selectDistance(String selectedDistance) {
            distance = Integer.parseInt(selectedDistance.substring(0, selectedDistance.length() - 3));
            chooseLocation.setDistance(selectedDistance);
            sharedPreference.putString(Constant.LOCATION_DATA, new Gson().toJson(chooseLocation));
            activityBinding.tvDistanceCity.setText(selectedDistance);
            clearList();

        }

        @Override
        public void onSubcategoryItemSelected(SubcategoryData item) {
            subcategoryId = String.valueOf(item.getId());
            activityBinding.tvSubcategory.setText(item.getName());
            clearList();
        }

        @Override
        public void onSortByClick(String ratingShort, String priceShort, String ending, String newly, String nearest) {
            price = priceShort;
            rating = ratingShort;
            ending_soonest = ending;
            newly_listed = newly;
            nearest_first = nearest;
            clearList();

        }

        @Override
        public void onFilterByClick(String serviceName, String storeName, String collectionStartTime, String collectionEndTime, String soldOut) {
            timeStart = collectionStartTime;
            timeEnd = collectionEndTime;
            shopName = storeName;
            productName = serviceName;
            soldHideOn = soldOut;
            clearList();
        }
    }

    private void subcategoryService(String categoryId) {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).subcategoryApi(categoryId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<SubcategoryRes>() {
                    @Override
                    public void onSuccess(@NonNull SubcategoryRes subcategoryRes) {
                        progressLoader.dismiss();
                        handleResponse(subcategoryRes);
                    }


                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        //  ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    private void handleResponse(SubcategoryRes subcategoryRes) {
        if (subcategoryRes.getStatus()) {
            subcategoryData = subcategoryRes.getData();
        } else {
            Toast.makeText(context, subcategoryRes.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /*private void ShowLocationDialog() {
        DialogUtility.alertMessageNoGps(context, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, Constant.LOCATION_RESULT);
            }
        });
    }*/

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.LOCATION_RESULT) {
            if (DialogUtility.isLocationEnable(context)) {
                ShowLocationDialog();
            } else

        }
    }*/

    private void myCampaignFilterService() {
        if (currentPage == 0) progressLoader.show();
        else categoryDetailAdapter.addLoading();

        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).getCampaignFilterListApi(
                currentPage, lat, lon, distance, timeStart, timeEnd, shopName, productName, price, rating,
                ending_soonest, newly_listed, nearest_first, categoryId, subcategoryId)

                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<FilterRes>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull FilterRes myCampaignListRes) {
                        progressLoader.dismiss();
                        handleFilterResponse(myCampaignListRes);
                    }


                    @Override
                    public void onError(@NonNull Throwable e) {
                        isLastPage = true;
                        categoryDetailAdapter.removeLoading();
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void handleFilterResponse(FilterRes myCampaignListRes) {
        if (myCampaignListRes.getStatus()) {
            activityBinding.includeCampaign.llNoItem.setVisibility(View.GONE);
            List<CommonData> getFilterdata=new ArrayList<>();
            for (int i = 0; i <myCampaignListRes.getData().getFilterdata().size() ; i++) {
                if (myCampaignListRes.getData().getFilterdata().get(i).getCampaignType().equals("onetime")) {
                    if (!myCampaignListRes.getData().getFilterdata().get(i).getRemainingQuantity().equals(0)) {
                        getFilterdata.add(myCampaignListRes.getData().getFilterdata().get(i));
                    }
                }else {
                    getFilterdata.add(myCampaignListRes.getData().getFilterdata().get(i));
                }
            }


            commonList.addAll(getFilterdata);
            categoryDetailAdapter.setData(commonList);
            isLoading = false;
        } else {
            isLastPage = true;
            categoryDetailAdapter.removeLoading();

            if (currentPage == 0){
           //     Toast.makeText(context, myCampaignListRes.getMessage(), Toast.LENGTH_SHORT).show();
                activityBinding.includeCampaign.llNoItem.setVisibility(View.VISIBLE);
            }

        }
    }

    @Override
    public void onFavProductClick(CommonData commonData) {
        addRemoveFavoritesService(commonData.getId());
    }

    private void addRemoveFavoritesService(Integer campaignId) {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).addRemoveFavoritesApi(campaignId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<JsonElement>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull JsonElement myCampaignListRes) {

                        handleFavoritesResponse(myCampaignListRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void handleFavoritesResponse(JsonElement jsonElement) {
        JsonObject jsonObject = jsonElement.getAsJsonObject();

        if (jsonObject.get("status").getAsBoolean()) {
            clearList();
        } else {
            Toast.makeText(context, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
            progressLoader.dismiss();
        }
    }

    private void clearList() {
        currentPage = PAGE_START;
        isLastPage = false;
        commonList.clear();
        categoryDetailAdapter.clear();
        myCampaignFilterService();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                activityBinding.tvSearchCity.setText(place.getAddress());
                lat = String.valueOf(place.getLatLng().latitude);
                lon = String.valueOf(place.getLatLng().longitude);

                chooseLocation.setLatLng(place.getLatLng());
                chooseLocation.setAddress(place.getAddress());
                sharedPreference.putString(Constant.LOCATION_DATA, new Gson().toJson(chooseLocation));
                clearList();
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {

                Status status = Autocomplete.getStatusFromIntent(data);

            } else if (resultCode == RESULT_CANCELED) {

            }
        }
    }


}


