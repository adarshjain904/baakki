package com.app.baakki.category;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.app.baakki.R;
import com.app.baakki.bottomsheet.CategoryFilterBottomSheet;
import com.app.baakki.bottomsheet.DistanceBottomSheet;

import com.app.baakki.databinding.FragmentCategoriesBinding;
import com.app.baakki.listener.SelectDistanceClick;
import com.app.baakki.model.ChooseLocation;
import com.app.baakki.model.category.CategoryRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;

public class CategoryFragment extends Fragment/* implements AdapterView.OnItemSelectedListener*/ {

    FragmentCategoriesBinding dataBiding;
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    CategoryAdapter categoryAdapter;
    CompositeDisposable compositeDisposable;
    ChooseLocation chooseLocation;
    List<Place.Field> fields;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.fragment_categories, null, false);
        dataBiding.setHandlers(new MyClickHandlers());
        init();
        return dataBiding.getRoot();
    }

    private void init() {
        context = getActivity();
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        Places.initialize(getApplicationContext(), getString(R.string.google_map_key));
        fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);

        chooseLocation = new Gson().fromJson(sharedPreference.getString(Constant.LOCATION_DATA), ChooseLocation.class);
        dataBiding.tvDistanceCity.setText(chooseLocation.getDistance());
        dataBiding.tvSearchCity.setText(chooseLocation.getAddress());
        if (TextUtils.isEmpty(chooseLocation.getDistance()))
            dataBiding.tvDistanceCity.setText(getResources().getString(R.string.select_radius));


        categoryAdapter = new CategoryAdapter(context);
        dataBiding.setCategoryAdapter(categoryAdapter);
        categoryService();
    }

    public class MyClickHandlers implements SelectDistanceClick {
        public void onBackClick(View view) {
            Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigateUp();
        }

        public void onSearchClick(View view) {
            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                    .setCountry(sharedPreference.getString(Constant.COUNTRY_CODE))
                    .build(getActivity());
            startActivityForResult(intent, Constant.AUTOCOMPLETE_REQUEST_CODE);
        }

        public void onDistanceClick(View view) {
            new DistanceBottomSheet(this).show(getActivity().getSupportFragmentManager(), "DistanceBottomSheet");
        }

        @Override
        public void selectDistance(String selectedDistance) {
            chooseLocation.setDistance(selectedDistance);
            sharedPreference.putString(Constant.LOCATION_DATA, new Gson().toJson(chooseLocation));
            dataBiding.tvDistanceCity.setText(selectedDistance);

        }
    }

    private void categoryService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).categoryApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CategoryRes>() {
                    @Override
                    public void onSuccess(@NonNull CategoryRes categoryRes) {
                        progressLoader.dismiss();
                        handleCategoryResponse(categoryRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                        Log.e("onError: ", e.getMessage());
                    }
                }));
    }

    private void handleCategoryResponse(CategoryRes categoryRes) {
        if (categoryRes.getStatus()) {
            categoryAdapter.setData(categoryRes.getData());
        } else {
            Toast.makeText(context, categoryRes.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                dataBiding.tvSearchCity.setText(place.getAddress());

                chooseLocation.setLatLng(place.getLatLng());
                chooseLocation.setAddress(place.getAddress());
                sharedPreference.putString(Constant.LOCATION_DATA, new Gson().toJson(chooseLocation));
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {

                Status status = Autocomplete.getStatusFromIntent(data);

            } else if (resultCode == RESULT_CANCELED) {

            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        chooseLocation = new Gson().fromJson(sharedPreference.getString(Constant.LOCATION_DATA), ChooseLocation.class);
        dataBiding.tvDistanceCity.setText(chooseLocation.getDistance());
        dataBiding.tvSearchCity.setText(chooseLocation.getAddress());
        if (TextUtils.isEmpty(chooseLocation.getDistance()))
            dataBiding.tvDistanceCity.setText(getResources().getString(R.string.select_radius));
    }
}