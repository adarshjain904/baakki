package com.app.baakki.category;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.bottomsheet.CategoryFilterBottomSheet;
import com.app.baakki.bottomsheet.DistanceBottomSheet;
import com.app.baakki.databinding.ItemCategoryFilterBinding;
import com.app.baakki.databinding.ItemCategoryListBinding;
import com.app.baakki.databinding.ItemLoadingBinding;
import com.app.baakki.databinding.ItemProductBinding;

import com.app.baakki.discover.ProductAdapter;
import com.app.baakki.discover.ProductDetailActivity;
import com.app.baakki.listener.OnFavProductClick;
import com.app.baakki.model.usergetallcampaign.CommonData;
import com.app.baakki.util.Constant;
import com.app.baakki.util.DialogUtility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import static com.app.baakki.util.DialogUtility.serverDate;

public class CategoryDetailAdapter extends RecyclerView.Adapter  {
    private Context context;
    private boolean isLoaderVisible = false;
    private static final int VIEW_TYPE_LOADING = 0;
    private static final int VIEW_TYPE_NORMAL = 1;
    List<CommonData> dataModelList;
    OnFavProductClick onFavProductClick;

    public CategoryDetailAdapter(Context ctx,    OnFavProductClick onFavProductClick) {
        context = ctx;
        this.onFavProductClick = onFavProductClick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ItemCategoryListBinding binding = DataBindingUtil.inflate(LayoutInflater.
                        from(parent.getContext()), R.layout.item_category_list, parent, false);
                return new ViewHolder(binding);
            case VIEW_TYPE_LOADING:
                ItemLoadingBinding loadingBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_loading, parent, false);
                return new ProgressHolder(loadingBinding);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).itemRowBinding.tvOriginalPrice.setPaintFlags(((ViewHolder) holder).itemRowBinding.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            ((ViewHolder) holder).itemRowBinding.setFilterData(dataModelList.get(position));
            ((ViewHolder) holder).itemRowBinding.tvDiscoverTime.setText(serverDate(dataModelList.get(position)
                    .getFromTime()) + " To " + serverDate(dataModelList.get(position).getToTime())+"  \u2022 "+dataModelList.get(position).getDistance()+"Km");
            ((ViewHolder) holder).itemRowBinding.tvDiscoverDate.setText("Start from "+DialogUtility.serverDateTime(dataModelList.get(position).getDate()));
            ((ViewHolder) holder).itemRowBinding.setHandlers(new MyClickHandlers());
            ((ViewHolder) holder).itemRowBinding.setPos(position);
            ((ViewHolder) holder).itemRowBinding.executePendingBindings();

            /*if (DialogUtility.checkExpairDate(dataModelList.get(position).getCamExpiry(), dataModelList.get(position).getToTime()))
                ((ViewHolder) holder).itemRowBinding.tvRemaining.setText("Expired");
            else*/
            if (dataModelList.get(position).getRemainingQuantity() == null) {
                ((ViewHolder) holder).itemRowBinding.tvRemaining.setText("Sold out ");
            } else if (dataModelList.get(position).getRemainingQuantity() == 0) {
                ((ViewHolder) holder).itemRowBinding.tvRemaining.setText("Sold out ");
            }  else
                ((ViewHolder) holder).itemRowBinding.tvRemaining.setText(dataModelList.get(position).getRemainingQuantity() + " left");
        }
    }

    public void setData(List<CommonData> dataModelList){
        this.dataModelList = dataModelList;
        notifyDataSetChanged();
    }

    public class MyClickHandlers {
        public void onProductClick(int pos) {
            Intent intent = new Intent(context, ProductDetailActivity.class);
            intent.putExtra(Constant.PRODUCT_DETAIL_DATA, dataModelList.get(pos));
            context.startActivity(intent);
        }

        public void onFavClick(CommonData commonData) {
            onFavProductClick.onFavProductClick(commonData);
        }
    }

    @Override
    public int getItemCount() {
       return dataModelList==null?0:dataModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isLoaderVisible) {
            return position == dataModelList.size() ? VIEW_TYPE_LOADING : VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_NORMAL;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemCategoryListBinding itemRowBinding;

        public ViewHolder(ItemCategoryListBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }

    public class ProgressHolder extends RecyclerView.ViewHolder {
        public ItemLoadingBinding itemLoadingBinding;

        ProgressHolder(ItemLoadingBinding itemLoadingBinding) {
            super(itemLoadingBinding.getRoot());
            this.itemLoadingBinding = itemLoadingBinding;
        }
    }

    public void addLoading() {
        isLoaderVisible = true;
        notifyDataSetChanged();
    }

    public void removeLoading() {
        isLoaderVisible = false;
        notifyDataSetChanged();
    }

    public void clear() {
        if (dataModelList != null) {
            dataModelList.clear();
            notifyDataSetChanged();
        }
    }


}