package com.app.baakki.category;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.databinding.ItemCategoryBinding;
import com.app.baakki.model.category.CategoryData;
import com.app.baakki.util.Constant;

import java.util.List;


public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    private Context context;
    List<CategoryData> dataModelList;
    public CategoryAdapter(Context ctx) {
        context = ctx;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCategoryBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_category, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemRowBinding.setPos(position);
        holder.itemRowBinding.setCategoryData(dataModelList.get(position));
        holder.itemRowBinding.setHandlers(new MyClickHandlers());
    }

    public void setData(List<CategoryData> dataModelList){
        this.dataModelList = dataModelList;
        notifyDataSetChanged();
    }


    public class MyClickHandlers {
        public void onCategoryClick(int position) {
            Intent intent = new Intent(context, CategoryDetailActivity.class);
            intent.putExtra(Constant.CATEGORY_DATA,dataModelList.get(position));
            context.startActivity(intent);
        }

    }


    @Override
    public int getItemCount() {
         return dataModelList==null?0:dataModelList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemCategoryBinding itemRowBinding;

        public ViewHolder(ItemCategoryBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }

}