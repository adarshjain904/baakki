package com.app.baakki.bottomsheet;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Consumer;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;

import com.app.baakki.databinding.DialogCalenderBinding;
import com.app.baakki.databinding.DialogSortByBinding;
import com.app.baakki.listener.CalenderDataListener;
import com.app.baakki.model.mycampaignlist.Campaigntime;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class CalenderBottomSheet extends BottomSheetDialogFragment implements OnDateSelectedListener {
    DialogCalenderBinding dataBiding;
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ArrayList<String> dataArray = new ArrayList<>();
    ArrayList<Date> sortDataArray = new ArrayList<>();
    CalenderDataListener calenderDataListener;
    List<Campaigntime> campaigntimes;
    boolean isEdit;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat showDateFormat = new SimpleDateFormat("dd-MMM-yyyy");

    public CalenderBottomSheet(CalenderDataListener calenderDataListener, boolean isEdit, List<Campaigntime> campaigntimes) {
        this.calenderDataListener = calenderDataListener;
        this.isEdit = isEdit;
        this.campaigntimes = campaigntimes;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.dialog_calender, null, false);
        dataBiding.setHandlers(new MyClickHandlers());

        init();
        return dataBiding.getRoot();
    }

    private void init() {
        context = getActivity();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);



        getDialog().setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                View bottomSheetInternal = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheetInternal).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        dataBiding.calendarView.setOnDateChangedListener(this);

        Calendar mcurrentTime = Calendar.getInstance();
        if (isEdit){
            for (int i = 0; i <campaigntimes.size() ; i++) {
                sortList(campaigntimes.get(i).getDate(),true);

                Date readDate = null;
                try {
                    readDate = new SimpleDateFormat("yyyy-MM-dd").parse(campaigntimes.get(i).getDate());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                mcurrentTime.setTimeInMillis(readDate.getTime());
                dataBiding.calendarView.setDateSelected(CalendarDay.from(mcurrentTime.get(Calendar.YEAR), mcurrentTime.get(Calendar.MONTH) + 1, mcurrentTime.get(Calendar.DAY_OF_MONTH)),true);
            }
        }else {
            dataBiding.calendarView.state().edit()
                    .setMinimumDate(CalendarDay.from(mcurrentTime.get(Calendar.YEAR), mcurrentTime.get(Calendar.MONTH) + 1, mcurrentTime.get(Calendar.DAY_OF_MONTH)))
                    .commit();
        }
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        sortList(date.getYear() + "-" + date.getMonth() + "-" + date.getDay(), selected);
    }

    private void sortList(String selectDate, boolean isSelected) {
        try {
            Date date = simpleDateFormat.parse(selectDate);
            if (isSelected) {
                sortDataArray.add(date);
                Collections.sort(sortDataArray);
            } else {
                sortDataArray.remove(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        dataBiding.tvNoDate.setText(sortDataArray.size() + " Days Campaign");
        if (sortDataArray.size()>0) {
            if (sortDataArray.size() > 1)
                dataBiding.tvStartEndDate.setText(showDateFormat.format(sortDataArray.get(0)) + " To " + showDateFormat.format(sortDataArray.get(sortDataArray.size() - 1)));
            else
                dataBiding.tvStartEndDate.setText(showDateFormat.format(sortDataArray.get(0)));
        }else {
            dataBiding.tvStartEndDate.setText("");
        }


    }

    public class MyClickHandlers {
        public void onDoneClick(View view) {
            dataArray.clear();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            for (int i = 0; i < sortDataArray.size(); i++) {
                dataArray.add(simpleDateFormat.format(sortDataArray.get(i)));
            }

            calenderDataListener.calenderDataListener(dataArray, dataBiding.tvStartEndDate.getText().toString());
            dismiss();
        }
    }

    public static void showDatePickerDialog(
            Context context, CalendarDay day,
            DatePickerDialog.OnDateSetListener callback) {
        if (day == null) {
            day = CalendarDay.today();
        }
        DatePickerDialog dialog = new DatePickerDialog(
                context, 0, callback, day.getYear(), day.getMonth() - 1, day.getDay()
        );
        dialog.show();
    }

}