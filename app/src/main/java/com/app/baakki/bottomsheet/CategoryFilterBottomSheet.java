package com.app.baakki.bottomsheet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.databinding.DialogCategoryFilterBinding;
import com.app.baakki.databinding.DialogLogoutBinding;
import com.app.baakki.listener.OnSubcategoryClickListener;
import com.app.baakki.login.SignInActivity;
import com.app.baakki.model.subcategory.SubcategoryData;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.List;

public class CategoryFilterBottomSheet extends BottomSheetDialogFragment implements OnSubcategoryClickListener {

    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;


    CategoryFilterAdapter categoryFilterAdapter;
    DialogCategoryFilterBinding dataBiding;

    List<SubcategoryData> dataModelList;
    SubcategoryData item;
    OnSubcategoryClickListener selectSubcategoryClick;
    String name;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.dialog_category_filter, null, false);
        dataBiding.setHandlers(new MyClickHandlers());

        init();
        return dataBiding.getRoot();
    }

    public CategoryFilterBottomSheet(OnSubcategoryClickListener selectSubcategoryClick, List<SubcategoryData> dataModelList, String name) {
        this.name = name;
        this.dataModelList = dataModelList;
        this.selectSubcategoryClick = selectSubcategoryClick;
    }


    private void init() {
        context = getActivity();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        dataBiding.tvCategory.setText(name);
        categoryFilterAdapter=new CategoryFilterAdapter(context,this,dataModelList);
        dataBiding.setCategoryAdapter(categoryFilterAdapter);
    }

    @Override
    public void onSubcategoryItemSelected(SubcategoryData item) {
        this.item=item;
    }

    public class MyClickHandlers {
        public void onSaveClick(View view) {
            if (item != null)
                selectSubcategoryClick.onSubcategoryItemSelected(item);
            else
                selectSubcategoryClick.onSubcategoryItemSelected(dataModelList.get(0));


            dismiss();
        }

        public void onResetClick(View view) {
            dismiss();
        }

    }

}
