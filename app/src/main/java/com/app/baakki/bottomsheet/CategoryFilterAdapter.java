package com.app.baakki.bottomsheet;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.category.CategoryDetailActivity;
import com.app.baakki.databinding.ItemBusinessSubcategoryBinding;
import com.app.baakki.databinding.ItemCategoryBinding;
import com.app.baakki.databinding.ItemCategoryFilterBinding;
import com.app.baakki.listener.OnSubcategoryClickListener;
import com.app.baakki.model.subcategory.SubcategoryData;

import java.util.List;


public class CategoryFilterAdapter extends RecyclerView.Adapter<CategoryFilterAdapter.ViewHolder>  {
    private Context context;
    List<SubcategoryData> dataModelList;
    ItemBusinessSubcategoryBinding binding;
    private OnSubcategoryClickListener listener;
    private int selectedPos = 0;

    public CategoryFilterAdapter(Context ctx, OnSubcategoryClickListener listener, List<SubcategoryData> dataModelList) {
        context = ctx;
        this.listener = listener;
        this.dataModelList=dataModelList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemCategoryFilterBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_category_filter, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemRowBinding.setHandler(new MyClickHandlers());
        holder.itemRowBinding.setSubcategoryData(dataModelList.get(position));
        holder.itemRowBinding.setPos(position);
        holder.itemRowBinding.setChecked(selectedPos == position);
    }

    public class MyClickHandlers {
        public void onSelectListClick(int pos) {
            if (listener != null) {
                if (pos != RecyclerView.NO_POSITION) {
                    listener.onSubcategoryItemSelected(dataModelList.get(pos));
                    notifyItemChanged(selectedPos);
                    selectedPos = pos;
                    notifyItemChanged(selectedPos);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataModelList==null?0:dataModelList.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemCategoryFilterBinding itemRowBinding;

        public ViewHolder(ItemCategoryFilterBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }
}