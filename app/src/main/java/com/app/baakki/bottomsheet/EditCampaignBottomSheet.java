package com.app.baakki.bottomsheet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.business.CampaignAdapter;
import com.app.baakki.databinding.DialogEditCampaignBinding;
import com.app.baakki.databinding.DialogSortByBinding;
import com.app.baakki.listener.EditCampaignListener;
import com.app.baakki.model.DateModel;
import com.app.baakki.model.addcampaignres.AddCampaignRes;
import com.app.baakki.model.mycampaignlist.MyCampaignListData;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.Gson;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.app.baakki.util.Constant.EDIT_CAMP_RESULT;

public class EditCampaignBottomSheet extends BottomSheetDialogFragment {
    DialogEditCampaignBinding dataBiding;
    Context context;
    String remainingQuantity;
    CompositeDisposable compositeDisposable;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    EditCampaignListener editCampaignBottomSheet;
    MyCampaignListData myCampaignListData;

    public EditCampaignBottomSheet(EditCampaignListener editCampaignBottomSheet, MyCampaignListData myCampaignListData) {
        this.editCampaignBottomSheet = editCampaignBottomSheet;
        this.myCampaignListData = myCampaignListData;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.dialog_edit_campaign, null, false);
        dataBiding.setHandlers(new MyClickHandlers());

        init();
        return dataBiding.getRoot();
    }

    private void init() {
        context = getActivity();
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        String currentSurverDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        for (int i = 0; i < myCampaignListData.getCampaigntimes().size(); i++) {
            if (currentSurverDate.equals(myCampaignListData.getCampaigntimes().get(i).getDate())) {
                dataBiding.tvAvailability.setText("" + myCampaignListData.getCampaigntimes().get(i).getStockQuantity());
                dataBiding.etRemainingQuantity.setText(myCampaignListData.getCampaigntimes().get(i).getRestQuantity());
            }
        }


    }

    public class MyClickHandlers {
        public void onUpdateClick(View view) {
            if (isValid())
                editCampaignService();
        }
    }

    private boolean isValid() {
        boolean isValid = true;
        remainingQuantity = dataBiding.etRemainingQuantity.getText().toString();
        if (TextUtils.isEmpty(remainingQuantity)) {
            Toast.makeText(context, "Enter Remaining Quantity", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        return isValid;
    }

    private void editCampaignService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).updateQuantityCampaignApi(dataBiding.etRemainingQuantity.getText().toString(), myCampaignListData.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<AddCampaignRes>() {
                    @Override
                    public void onSuccess(@NonNull AddCampaignRes addCampaignRes) {
                        dismiss();
                        editCampaignBottomSheet.editCampaign();
                        progressLoader.dismiss();

                        if (!addCampaignRes.getStatus())
                            Toast.makeText(context, addCampaignRes.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        dismiss();
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

}
