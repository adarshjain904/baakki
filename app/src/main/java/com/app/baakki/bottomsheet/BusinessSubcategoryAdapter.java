package com.app.baakki.bottomsheet;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.databinding.ItemBusinessSubcategoryBinding;
import com.app.baakki.databinding.ItemListShopBinding;
import com.app.baakki.listener.OnShopClickListener;
import com.app.baakki.listener.OnSubcategoryClickListener;
import com.app.baakki.model.shoplist.ShopListData;
import com.app.baakki.model.subcategory.SubcategoryData;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

public class BusinessSubcategoryAdapter extends RecyclerView.Adapter<BusinessSubcategoryAdapter.ViewHolder> {
    private Context context;
    List<SubcategoryData> dataModelList;
    ItemBusinessSubcategoryBinding binding;
    private OnSubcategoryClickListener listener;
    private int selectedPos = 0;

    public BusinessSubcategoryAdapter(Context context,OnSubcategoryClickListener listener, List<SubcategoryData> dataModelList) {
        this.context = context;
        this.listener = listener;
        this.dataModelList=dataModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_business_subcategory, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemRowBinding.setHandler(new MyClickHandlers());
        holder.itemRowBinding.setSubcategoryData(dataModelList.get(position));
        holder.itemRowBinding.setPos(position);
        holder.itemRowBinding.setChecked(selectedPos == position);
    }


    @Override
    public int getItemCount() {
        return dataModelList == null ? 0 : dataModelList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemBusinessSubcategoryBinding itemRowBinding;

        public ViewHolder(ItemBusinessSubcategoryBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }


    public class MyClickHandlers {
        public void onSelectListClick(int pos) {
            if (listener != null) {
                if (pos != RecyclerView.NO_POSITION) {
                    listener.onSubcategoryItemSelected(dataModelList.get(pos));
                    notifyItemChanged(selectedPos);
                    selectedPos = pos;
                    notifyItemChanged(selectedPos);
                }
            }
        }
    }
}