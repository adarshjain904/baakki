package com.app.baakki.bottomsheet;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.databinding.ItemBusinessCategoryBinding;
import com.app.baakki.databinding.ItemCountryBinding;
import com.app.baakki.listener.OnCategoryClickListener;
import com.app.baakki.listener.OnCountryClickListener;
import com.app.baakki.model.category.CategoryData;
import com.app.baakki.model.country.CountryData;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.ViewHolder> {
    private Context context;
    List<CountryData> dataModelList;
    ItemCountryBinding binding;
    private OnCountryClickListener listener;
    private int selectedPos = 0;

    public CountryAdapter(Context context, OnCountryClickListener listener, List<CountryData> dataModelList) {
        this.context = context;
        this.listener = listener;
        this.dataModelList = dataModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_country, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemRowBinding.setHandler(new MyClickHandlers());
        holder.itemRowBinding.setCountryData(dataModelList.get(position));
        holder.itemRowBinding.setPos(position);
        holder.itemRowBinding.setChecked(selectedPos == position);
    }

    @Override
    public int getItemCount() {
        return dataModelList == null ? 0 : dataModelList.size();
    }




    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemCountryBinding itemRowBinding;

        public ViewHolder(ItemCountryBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }

    public class MyClickHandlers {
        public void onSelectListClick(int pos) {
            if (listener != null) {
                if (pos != RecyclerView.NO_POSITION) {
                    listener.onCategoryItemSelected(dataModelList.get(pos));
                    notifyItemChanged(selectedPos);
                    selectedPos = pos;
                    notifyItemChanged(selectedPos);
                }
            }
        }
    }
}