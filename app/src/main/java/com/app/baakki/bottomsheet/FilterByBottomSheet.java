package com.app.baakki.bottomsheet;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.databinding.DialogCategoryFilterBinding;
import com.app.baakki.databinding.DialogFilterByBinding;
import com.app.baakki.listener.OnFilterByClick;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class FilterByBottomSheet extends BottomSheetDialogFragment implements RadioGroup.OnCheckedChangeListener {
    DialogFilterByBinding dataBiding;
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    boolean isSoldOn = false;
    OnFilterByClick onFilterByClick;
    String serviceName,StoreName,collectionStartTime,collectionEndTime,soldOut;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.dialog_filter_by, null, false);
        dataBiding.setHandlers(new MyClickHandlers());

        init();
        return dataBiding.getRoot();
    }

    public   FilterByBottomSheet(OnFilterByClick onFilterByClick ){
     this.onFilterByClick=onFilterByClick;
    }

    private void init() {
        context = getActivity();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        dataBiding.rgSold.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
        switch (checkedId) {
            case R.id.rb_sold_yes:
                soldOut = "Yes";
                dataBiding.tvSoldVal.setText("Yes");
                break;
            case R.id.rb_sold_no:
                soldOut = "No";
                dataBiding.tvSoldVal.setText("No");
                break;
        }
        isSoldOn = false;
        dataBiding.rgSold.setVisibility(View.GONE);
        dataBiding.ivSoldArrow.startAnimation(animate(isSoldOn));
    }

    public class MyClickHandlers {
        public void onApplyClick(View view) {
            onFilterByClick.onFilterByClick(dataBiding.etSerProName.getText().toString(),
                    dataBiding.etShopName.getText().toString(),
                    collectionStartTime,collectionEndTime,soldOut);
            dismiss();
        }

        public void onSold(View view) {
            if (!isSoldOn) {
                isSoldOn = true;
                dataBiding.rgSold.setVisibility(View.VISIBLE);
                dataBiding.ivSoldArrow.startAnimation(animate(isSoldOn));// r TextView
            } else {
                isSoldOn = false;
                dataBiding.rgSold.setVisibility(View.GONE);
                dataBiding.ivSoldArrow.startAnimation(animate(isSoldOn));
            }
        }

    }

    private Animation animate(boolean up) {
        Animation anim = AnimationUtils.loadAnimation(context, up ? R.anim.rotate_up : R.anim.rotate_down);
        anim.setInterpolator(new LinearInterpolator()); // for smooth animation
        return anim;
    }

}
