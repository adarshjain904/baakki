package com.app.baakki.bottomsheet;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TimePicker;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.databinding.DialogEndTimeBinding;
import com.app.baakki.listener.EndTimeListener;
import com.app.baakki.model.mycampaignlist.Campaigntime;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class EndTimeBottomSheet extends BottomSheetDialogFragment implements TimePicker.OnTimeChangedListener {
    DialogEndTimeBinding dataBiding;
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    EndTimeListener EndTimeListener;
    String time;
    boolean isEdit;
    List<Campaigntime> campaigntimes;

    public EndTimeBottomSheet(EndTimeListener EndTimeListener, boolean isEdit, List<Campaigntime> campaigntimes) {
        this.EndTimeListener = EndTimeListener;
        this.isEdit = isEdit;
        this.campaigntimes = campaigntimes;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.dialog_end_time, null, false);
        dataBiding.setHandlers(new MyClickHandlers());

        init();
        return dataBiding.getRoot();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void init() {
        context = getActivity();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        dataBiding.simpleTimePicker.setOnTimeChangedListener(this);

        final Calendar c = Calendar.getInstance();
        if (isEdit) {
            time = campaigntimes.get(0).getToTime();
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
            Date date = null;
            try {
                date = sdf.parse(campaigntimes.get(0).getToTime());
            } catch (ParseException e) {
            }
            c.setTime(date);
        } else {
            time = c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE);
        }

        dataBiding.simpleTimePicker.setHour(c.get(Calendar.HOUR_OF_DAY));
        dataBiding.simpleTimePicker.setMinute(c.get(Calendar.MINUTE));
    }


    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        String hour,mint;

        if (hourOfDay<10){
            hour="0"+hourOfDay;

        }else {
            hour= String.valueOf(hourOfDay);

        }
        if (minute<10){
            mint="0"+minute;

        }else {
            mint= String.valueOf(minute);

        }
        time = hour + ":" + mint;
    }

    public class MyClickHandlers {
        public void onDoneClick(View view) {
            EndTimeListener.endTimeListener(time);
            dismiss();
        }
    }


}