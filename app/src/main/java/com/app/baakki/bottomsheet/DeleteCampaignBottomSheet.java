package com.app.baakki.bottomsheet;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.databinding.DialogDeleteBinding;
import com.app.baakki.databinding.DialogLogoutBinding;
import com.app.baakki.listener.DeleteCampaignListener;
import com.app.baakki.listener.EditCampaignListener;
import com.app.baakki.login.SocialLoginActivity;
import com.app.baakki.model.mycampaignlist.MyCampaignListData;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.app.baakki.util.PaginationListener.PAGE_START;

public class DeleteCampaignBottomSheet extends BottomSheetDialogFragment {
    DialogDeleteBinding dataBiding;
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    CompositeDisposable compositeDisposable;
    DeleteCampaignListener deleteCampaignListener;
    MyCampaignListData myCampaignListData;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.dialog_delete, null, false);
        dataBiding.setHandlers(new MyClickHandlers());

        init();
        return dataBiding.getRoot();
    }

    public DeleteCampaignBottomSheet(DeleteCampaignListener deleteCampaignListener, MyCampaignListData myCampaignListData) {
        this.deleteCampaignListener=deleteCampaignListener;
        this.myCampaignListData=myCampaignListData;
    }

    private void init() {
        context = getActivity();
        progressLoader = new ProgressLoader(context);
        compositeDisposable = new CompositeDisposable();
        sharedPreference = SharedPreference.getInstance(context);
    }

    public class MyClickHandlers {
        public void onLogoutClick(View view) {
            deleteCampaignService(myCampaignListData.getId());

        }

        public void onCancelClick(View view) {
            dismiss();
        }

    }

    private void deleteCampaignService(Integer campaignId) {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).deleteCampaignApi(campaignId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<JsonElement>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull JsonElement myCampaignListRes) {

                        handleFavoritesResponse(myCampaignListRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void handleFavoritesResponse(JsonElement jsonElement) {
        JsonObject jsonObject = jsonElement.getAsJsonObject();

        if (jsonObject.get("status").getAsBoolean()) {
            progressLoader.dismiss();
            deleteCampaignListener.deleteCampaign();
            dismiss();
        } else {
            Toast.makeText(context, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
            progressLoader.dismiss();
        }
    }

}
