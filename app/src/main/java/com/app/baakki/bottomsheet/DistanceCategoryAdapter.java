package com.app.baakki.bottomsheet;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.databinding.ItemBusinessCategoryBinding;
import com.app.baakki.databinding.ItemSubcategoryBinding;
import com.app.baakki.listener.OnCategoryClickListener;
import com.app.baakki.listener.SelectCategoryClick;
import com.app.baakki.model.category.CategoryData;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;


public class DistanceCategoryAdapter extends RecyclerView.Adapter<DistanceCategoryAdapter.ViewHolder> {
    private Context context;
    List<CategoryData> categoryData;
    SelectCategoryClick selectCategoryClick;

    private int selectedPos = 0;


    public DistanceCategoryAdapter(Context ctx, SelectCategoryClick selectCategoryClick,  List<CategoryData> categoryData) {
        context = ctx;
        this.categoryData = categoryData;
        this.selectCategoryClick = selectCategoryClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemSubcategoryBinding binding = DataBindingUtil.
                inflate(LayoutInflater.from(parent.getContext()), R.layout.item_subcategory, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemRowBinding.setCategoryData(categoryData.get(position).getName());
        holder.itemRowBinding.setHandler(new MyClickHandlers());
        holder.itemRowBinding.setPos(position);
        holder.itemRowBinding.setChecked(selectedPos == position);
    }




    @Override
    public int getItemCount() {
        return categoryData == null ? 0 : categoryData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemSubcategoryBinding itemRowBinding;

        public ViewHolder(ItemSubcategoryBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }



    public class MyClickHandlers {
        public void onSelectListClick(int pos) {
            if (selectCategoryClick != null) {
                if (pos != RecyclerView.NO_POSITION) {
                    selectCategoryClick.selectCategory(categoryData.get(pos));
                    notifyItemChanged(selectedPos);
                    selectedPos = pos;
                    notifyItemChanged(selectedPos);
                }
            }
        }
    }


}