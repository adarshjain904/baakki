package com.app.baakki.bottomsheet;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.databinding.DialogBusinessCategoryBinding;
import com.app.baakki.databinding.DialogCountryDialogBinding;
import com.app.baakki.listener.OnCategoryClickListener;
import com.app.baakki.listener.OnCountryClickListener;
import com.app.baakki.model.category.CategoryData;
import com.app.baakki.model.country.CountryData;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.List;

public class CountryBottomSheet extends BottomSheetDialogFragment implements OnCountryClickListener {
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;

    DialogCountryDialogBinding dataBiding;
    CountryAdapter countryAdapter;
    List<CountryData> dataModelList;
    CountryData item;
    OnCountryClickListener selectCategoryClick;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.dialog_country_dialog, null, false);
        dataBiding.setHandlers(new MyClickHandlers());

        init();
        return dataBiding.getRoot();
    }

    public CountryBottomSheet(OnCountryClickListener selectCategoryClick, List<CountryData> dataModelList) {
        this.selectCategoryClick = selectCategoryClick;
        this.dataModelList = dataModelList;
    }

    private void init() {
        context = getActivity();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        countryAdapter = new CountryAdapter(context, this, dataModelList);
        dataBiding.setCountryAdapter(countryAdapter);
    }

    @Override
    public void onCategoryItemSelected(CountryData item) {
        this.item = item;
    }

    public class MyClickHandlers {
        public void onDoneClick(View view) {
            if (item != null)
                selectCategoryClick.onCategoryItemSelected(item);
            else
                selectCategoryClick.onCategoryItemSelected(dataModelList.get(0));

            dismiss();
        }

        public void onCancelClick(View view) {
            dismiss();
        }

    }

}
