package com.app.baakki.bottomsheet;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.databinding.DialogBusinessSubcategoryBinding;
import com.app.baakki.databinding.DialogSubcategoryBinding;
import com.app.baakki.listener.OnSubcategoryClickListener;
import com.app.baakki.listener.SelectCategoryClick;
import com.app.baakki.model.category.CategoryData;
import com.app.baakki.model.subcategory.SubcategoryData;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.List;

public class BusinessSubcategoryBottomSheet extends BottomSheetDialogFragment implements OnSubcategoryClickListener {
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;

    DialogBusinessSubcategoryBinding dataBiding;
    BusinessSubcategoryAdapter subcategoryAdapter;
    List<SubcategoryData> dataModelList;
    SubcategoryData item;
    OnSubcategoryClickListener selectSubcategoryClick;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.dialog_business_subcategory, null, false);
        dataBiding.setHandlers(new MyClickHandlers());

        init();
        return dataBiding.getRoot();
    }

    public BusinessSubcategoryBottomSheet(OnSubcategoryClickListener selectSubcategoryClick, List<SubcategoryData> dataModelList) {
        this.dataModelList = dataModelList;
        this.selectSubcategoryClick = selectSubcategoryClick;
    }

    private void init() {
        context = getActivity();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        subcategoryAdapter = new BusinessSubcategoryAdapter(context,this,dataModelList);
        dataBiding.setBusinessSubcategoryAdapter(subcategoryAdapter);
    }

    @Override
    public void onSubcategoryItemSelected(SubcategoryData item) {
        this.item=item;

    }

    public class MyClickHandlers {
        public void onDoneClick(View view) {
            if (item != null)
                selectSubcategoryClick.onSubcategoryItemSelected(item);
            else
                selectSubcategoryClick.onSubcategoryItemSelected(dataModelList.get(0));


            dismiss();
        }

        public void onCancelClick(View view) {
            dismiss();
        }

    }

}
