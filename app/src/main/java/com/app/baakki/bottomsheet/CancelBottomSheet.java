package com.app.baakki.bottomsheet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.databinding.DialogCancelBinding;
import com.app.baakki.databinding.DialogCountryDialogBinding;
import com.app.baakki.discover.RatingActivity;
import com.app.baakki.home.HomeActivity;
import com.app.baakki.listener.CancelListener;
import com.app.baakki.listener.OnCountryClickListener;
import com.app.baakki.model.cancelorder.CancelOrderRes;
import com.app.baakki.model.country.CountryData;
import com.app.baakki.model.reserveorder.ReserveOrderRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class CancelBottomSheet extends BottomSheetDialogFragment  {
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    CompositeDisposable compositeDisposable;
    DialogCancelBinding dataBiding;
    String orderId,quantity;
    CancelListener cancelListener;

    public CancelBottomSheet(String orderId,String quantity, CancelListener cancelListener) {
        this.orderId=orderId;
        this.quantity=quantity;
        this.cancelListener=cancelListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.dialog_cancel, null, false);
        dataBiding.setHandlers(new MyClickHandlers());

        init();
        return dataBiding.getRoot();
    }


    private void init() {
        context = getActivity();
        compositeDisposable=new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);
    }

    public class MyClickHandlers {
        public void onDoneClick(View view) {
            if (TextUtils.isEmpty(dataBiding.etComment.getText().toString())){
                Toast.makeText(context, "Please Enter Reason", Toast.LENGTH_SHORT).show();
            }else {
                cancelReserveService();
                dismiss();
            }

        }
        public void onCancelClick(View view) {
            dismiss();
        }
    }

    private void cancelReserveService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).cancelReserveApi(orderId,quantity, dataBiding.etComment.getText().toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CancelOrderRes>() {
                    @Override
                    public void onSuccess(@NonNull CancelOrderRes cancelOrderRes) {
                        progressLoader.dismiss();
                        handleCategoryResponse(cancelOrderRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context,e.getMessage());
                        Log.e("onError: ", e.getMessage());
                    }
                }));
    }

    private void handleCategoryResponse(CancelOrderRes reserveOrderRes) {

        if (reserveOrderRes.getStatus()) {
            cancelListener.onCancelCLick();
        }else
            Toast.makeText(context,  reserveOrderRes.getMessage(), Toast.LENGTH_SHORT).show();
    }

}
