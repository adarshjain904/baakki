package com.app.baakki.bottomsheet;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.databinding.DialogLogoutBinding;
import com.app.baakki.login.ForgotActivity;
import com.app.baakki.login.SignInActivity;
import com.app.baakki.login.SocialLoginActivity;
import com.app.baakki.model.login.LoginSignupRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.DialogUtility;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.facebook.FacebookSdk.getApplicationContext;

public class LogoutBottomSheet extends BottomSheetDialogFragment {
    DialogLogoutBinding dataBiding;
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    CompositeDisposable compositeDisposable;
    GoogleSignInClient mGoogleSignInClient;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.dialog_logout, null, false);
        dataBiding.setHandlers(new MyClickHandlers());

        init();
        return dataBiding.getRoot();
    }

    private void init() {

        context = getActivity();
        progressLoader = new ProgressLoader(context);
        compositeDisposable = new CompositeDisposable();
        sharedPreference = SharedPreference.getInstance(context);
        googleIntegration();
    }

    public void googleIntegration() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail().build();
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
    }


    public class MyClickHandlers {
        public void onLogoutClick(View view) {
            if (sharedPreference.getBoolean(Constant.IS_GOOGLE)) {
                mGoogleSignInClient.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            logoutService();
                        }
                    }
                });
            } else if (sharedPreference.getBoolean(Constant.IS_FACEBOOK)) {
                LoginManager.getInstance().logOut();
                logoutService();
            } else
                logoutService();


        }

        public void onCancelClick(View view) {
            dismiss();
        }

    }

    private void logoutService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).logoutApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<JsonElement>() {
                    @Override
                    public void onSuccess(@NonNull JsonElement jsonElement) {
                        progressLoader.dismiss();
                        handleRes(jsonElement);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    private void handleRes(JsonElement jsonElement) {
        JsonObject jsonObject = jsonElement.getAsJsonObject();

        if (jsonObject.get("status").getAsBoolean()) {

            Intent intent = new Intent(context, SocialLoginActivity.class);
            sharedPreference.clearData();
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            dismiss();
        } else {
            Toast.makeText(context, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
        }

    }

}
