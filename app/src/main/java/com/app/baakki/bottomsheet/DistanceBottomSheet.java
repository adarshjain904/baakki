package com.app.baakki.bottomsheet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.databinding.DialogDistanceBinding;
import com.app.baakki.databinding.DialogLogoutBinding;
import com.app.baakki.listener.SelectDistanceClick;
import com.app.baakki.login.SignInActivity;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class DistanceBottomSheet extends BottomSheetDialogFragment {
    DialogDistanceBinding dataBiding;
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    SelectDistanceClick selectDistanceClick;
    private RadioButton radioSexButton;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.dialog_distance, null, false);
        dataBiding.setHandlers(new MyClickHandlers());

        init();
        return dataBiding.getRoot();
    }

    public DistanceBottomSheet(SelectDistanceClick selectDistanceClick) {
        this.selectDistanceClick = selectDistanceClick;
    }

    private void init() {
        context = getActivity();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);


    }

    public class MyClickHandlers {
        public void onDoneClick(View view) {
            RadioButton rbDistance = dataBiding.radioGroup.findViewById(dataBiding.radioGroup.getCheckedRadioButtonId());
            if (rbDistance !=null) {
                selectDistanceClick.selectDistance(rbDistance.getText().toString());
                dismiss();
            }else {
                Toast.makeText(context, "Please select Distance ", Toast.LENGTH_SHORT).show();
            }
        }

        public void onCancelClick(View view) {
            dismiss();
        }

    }

}
