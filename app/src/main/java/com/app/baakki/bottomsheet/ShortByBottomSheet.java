package com.app.baakki.bottomsheet;

import android.animation.LayoutTransition;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.databinding.DialogFilterByBinding;
import com.app.baakki.databinding.DialogSortByBinding;
import com.app.baakki.listener.OnSortByClick;
import com.app.baakki.listener.OnSubcategoryClickListener;
import com.app.baakki.model.subcategory.SubcategoryData;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.List;

public class ShortByBottomSheet extends BottomSheetDialogFragment implements RadioGroup.OnCheckedChangeListener {
    DialogSortByBinding dataBiding;
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    boolean isRating = false, isPrice = false, isEnding = false, isNewly = false, isNearest = false;
    String rating, price, ending, newly, nearest;
    OnSortByClick onSortByClick;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.dialog_sort_by, null, false);
        dataBiding.setHandlers(new MyClickHandlers());

        init();
        return dataBiding.getRoot();
    }

    public ShortByBottomSheet(OnSortByClick onSortByClick ) {
        this.onSortByClick = onSortByClick;
    }

    private void init() {
        context = getActivity();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        dataBiding.rgRating.setOnCheckedChangeListener(this);
        dataBiding.rgPrice.setOnCheckedChangeListener(this);
        dataBiding.rgEnding.setOnCheckedChangeListener(this);
        dataBiding.rgNewly.setOnCheckedChangeListener(this);
        dataBiding.rgNearest.setOnCheckedChangeListener(this);

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
        switch (radioGroup.getId()) {
            case R.id.rg_rating:
                switch (checkedId) {
                    case R.id.rb_rating_htl:
                        rating = "ASC";
                        dataBiding.tvRatingVal.setText("High to Low");
                        break;
                    case R.id.rb_rating_lth:
                        rating = "DESC";
                        dataBiding.tvRatingVal.setText("Low to High");
                        break;
                }

                isRating = false;
                dataBiding.rgRating.setVisibility(View.GONE);
                dataBiding.ivArrowRating.startAnimation(animate(isRating));
                break;
            case R.id.rg_price:
                switch (checkedId) {
                    case R.id.rb_price_htl:
                        price = "ASC";
                        dataBiding.tvPriceVal.setText("High to Low");
                        break;
                    case R.id.rb_price_lth:
                        price = "DESC";
                        dataBiding.tvPriceVal.setText("Low to High");
                        break;
                }
                isPrice = false;
                dataBiding.rgPrice.setVisibility(View.GONE);
                dataBiding.ivArrowPrice.startAnimation(animate(isPrice));
                break;
            case R.id.rg_ending:
                switch (checkedId) {
                    case R.id.rb_ending_yes:
                        ending = "DESC";
                        dataBiding.tvEndingVal.setText("Yes");
                        break;
                    case R.id.rb_ending_no:
                        ending = "ASC";
                        dataBiding.tvEndingVal.setText("No");
                        break;
                }
                isEnding = false;
                dataBiding.rgEnding.setVisibility(View.GONE);
                dataBiding.ivArrowEnding.startAnimation(animate(isEnding));
                break;
            case R.id.rg_newly:
                switch (checkedId) {
                    case R.id.rb_newly_yes:
                        newly = "ASC";
                        dataBiding.tvNewlyVal.setText("Yes");
                        break;
                    case R.id.rb_newly_no:
                        newly = "DESC";
                        dataBiding.tvNewlyVal.setText("No");
                        break;
                }
                isNewly = false;
                dataBiding.rgNewly.setVisibility(View.GONE);
                dataBiding.ivArrowNewly.startAnimation(animate(isNewly));
                break;
            case R.id.rg_nearest:
                switch (checkedId) {
                    case R.id.rb_nearest_yes:
                        nearest = "DESC";
                        dataBiding.tvNearestVal.setText("Yes");
                        break;
                    case R.id.rb_nearest_no:
                        nearest = "ASC";
                        dataBiding.tvNearestVal.setText("No");
                        break;
                }
                isNearest = false;
                dataBiding.rgNearest.setVisibility(View.GONE);
                dataBiding.ivArrowNearest.startAnimation(animate(isNearest));
                break;
        }
    }

    public class MyClickHandlers {
        public void onApplyClick(View view) {
            onSortByClick.onSortByClick(rating,price,ending,newly,nearest);
            dismiss();
        }

        public void onRating(View view) {
            if (!isRating) {
                isRating = true;
                dataBiding.rgRating.setVisibility(View.VISIBLE);
                dataBiding.ivArrowRating.startAnimation(animate(isRating));// r TextView
            } else {
                isRating = false;
                dataBiding.rgRating.setVisibility(View.GONE);
                dataBiding.ivArrowRating.startAnimation(animate(isRating));
            }
        }

        public void onPrice(View view) {
            if (!isPrice) {
                isPrice = true;
                dataBiding.rgPrice.setVisibility(View.VISIBLE);
                dataBiding.ivArrowPrice.startAnimation(animate(isPrice));// r TextView
            } else {
                isPrice = false;
                dataBiding.rgPrice.setVisibility(View.GONE);
                dataBiding.ivArrowPrice.startAnimation(animate(isPrice));
            }
        }

        public void onEnding(View view) {
            if (!isEnding) {
                isEnding = true;
                dataBiding.rgEnding.setVisibility(View.VISIBLE);
                dataBiding.ivArrowEnding.startAnimation(animate(isEnding));// r TextView
            } else {
                isEnding = false;
                dataBiding.rgEnding.setVisibility(View.GONE);
                dataBiding.ivArrowEnding.startAnimation(animate(isEnding));
            }
        }

        public void onNewly(View view) {
            if (!isNewly) {
                isNewly = true;
                dataBiding.rgNewly.setVisibility(View.VISIBLE);
                dataBiding.ivArrowNewly.startAnimation(animate(isNewly));// r TextView
            } else {
                isNewly = false;
                dataBiding.rgNewly.setVisibility(View.GONE);
                dataBiding.ivArrowNewly.startAnimation(animate(isNewly));
            }
        }

        public void onNearest(View view) {
            if (!isNearest) {
                isNearest = true;
                dataBiding.rgNearest.setVisibility(View.VISIBLE);
                dataBiding.ivArrowNearest.startAnimation(animate(isNearest));// r TextView
            } else {
                isNearest = false;
                dataBiding.rgNearest.setVisibility(View.GONE);
                dataBiding.ivArrowNearest.startAnimation(animate(isNearest));
            }
        }
    }

    private Animation animate(boolean up) {
        Animation anim = AnimationUtils.loadAnimation(context, up ? R.anim.rotate_up : R.anim.rotate_down);
        anim.setInterpolator(new LinearInterpolator()); // for smooth animation
        return anim;
    }
}
