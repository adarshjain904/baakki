package com.app.baakki.bottomsheet;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.databinding.DialogSubcategoryBinding;
import com.app.baakki.listener.SelectCategoryClick;
import com.app.baakki.model.category.CategoryData;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.List;

public class DistanceCategoryBottomSheet extends BottomSheetDialogFragment implements SelectCategoryClick {
    DialogSubcategoryBinding dataBiding;
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    DistanceCategoryAdapter subcategoryAdapter;
    SelectCategoryClick selectCategoryClick;
    List<CategoryData> categoryData;
    CategoryData categoryItem;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.dialog_subcategory, null, false);
        dataBiding.setHandlers(new MyClickHandlers());

        init();
        return dataBiding.getRoot();
    }

    public DistanceCategoryBottomSheet(SelectCategoryClick selectCategoryClick, List<CategoryData> categoryData) {
        this.selectCategoryClick = selectCategoryClick;
        this.categoryData = categoryData;
    }

    private void init() {
        context = getActivity();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        subcategoryAdapter = new DistanceCategoryAdapter(context, this, categoryData);
        dataBiding.setSubcategoryAdapter(subcategoryAdapter);
    }

    @Override
    public void selectCategory(CategoryData category) {
        categoryItem = category;
    }

    public class MyClickHandlers {
        public void onSaveClick(View view) {
            if (categoryItem != null)
                selectCategoryClick.selectCategory(categoryItem);
            else
                selectCategoryClick.selectCategory(categoryData.get(0));
            dismiss();
        }

        public void onCancelClick(View view) {
            dismiss();
        }

    }

}
