package com.app.baakki.bottomsheet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.databinding.DialogLogoutBinding;
import com.app.baakki.databinding.DialogSaveBinding;
import com.app.baakki.login.SignInActivity;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class SaveSearchBottomSheet extends BottomSheetDialogFragment {
    DialogSaveBinding dataBiding;
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.dialog_save, null, false);
        dataBiding.setHandlers(new MyClickHandlers());

        init();
        return dataBiding.getRoot();
    }

    private void init() {
        context = getActivity();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);
    }

    public class MyClickHandlers {
        public void onSaveClick(View view) {
            dismiss();
        }

        public void onCancelClick(View view) {
            dismiss();
        }

    }

}
