package com.app.baakki.bottomsheet;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.databinding.DialogCancelBinding;
import com.app.baakki.databinding.DialogFurtherHelpBinding;
import com.app.baakki.listener.CancelListener;
import com.app.baakki.model.cancelorder.CancelOrderRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class FurtherBottomSheet extends BottomSheetDialogFragment implements TextWatcher {
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    CompositeDisposable compositeDisposable;
    DialogFurtherHelpBinding dataBiding;
    int currentLine = 0;
    // String orderId;
    // CancelListener cancelListener;

    /*   public FurtherBottomSheet(String orderId*//*,CancelListener cancelListener*//*) {
        this.orderId=orderId;
       *//* this.cancelListener=cancelListener;*//*
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.dialog_further_help, null, false);
        dataBiding.setHandlers(new MyClickHandlers());

        init();
        return dataBiding.getRoot();
    }


    private void init() {
        context = getActivity();
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        dataBiding.etComment.addTextChangedListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (count == 0) {
            currentLine = currentLine - before;
        } else {
            currentLine = currentLine + count;
        }
        Log.d("new line ", currentLine + "");

        dataBiding.tvCount.setText(currentLine+"/500");


    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public class MyClickHandlers {
        public void onDoneClick(View view) {
            if (TextUtils.isEmpty(dataBiding.etComment.getText().toString())) {
                Toast.makeText(context, "Please Enter Message", Toast.LENGTH_SHORT).show();
            } else {
                furtherHelpService();
                dismiss();
            }

        }

        public void onCancelClick(View view) {
            dismiss();
        }
    }

    private void furtherHelpService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).furtherHelpApi(dataBiding.etComment.getText().toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CancelOrderRes>() {
                    @Override
                    public void onSuccess(@NonNull CancelOrderRes cancelOrderRes) {
                        progressLoader.dismiss();
                        handleCategoryResponse(cancelOrderRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                        Log.e("onError: ", e.getMessage());
                    }
                }));
    }

    private void handleCategoryResponse(CancelOrderRes reserveOrderRes) {

        dismiss();
        if (reserveOrderRes.getStatus()) {
            Toast.makeText(context, "Submitted Successfully", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(context, reserveOrderRes.getMessage(), Toast.LENGTH_SHORT).show();
    }

}
