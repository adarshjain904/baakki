package com.app.baakki.bottomsheet;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.databinding.ItemBusinessCategoryBinding;
import com.app.baakki.databinding.ItemListShopBinding;
import com.app.baakki.listener.OnCategoryClickListener;
import com.app.baakki.listener.OnShopClickListener;
import com.app.baakki.model.category.CategoryData;
import com.app.baakki.model.shoplist.ShopListData;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

public class BusinessCategoryAdapter extends RecyclerView.Adapter<BusinessCategoryAdapter.ViewHolder> {
    private Context context;
    List<CategoryData> dataModelList;

    private OnCategoryClickListener listener;
    private int selectedPos = 0;

    public BusinessCategoryAdapter(Context context,  OnCategoryClickListener listener,List<CategoryData> dataModelList) {
        this.context = context;
        this.listener = listener;
        this.dataModelList = dataModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemBusinessCategoryBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_business_category, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemRowBinding.setHandler(new MyClickHandlers());
        holder.itemRowBinding.setCategoryData(dataModelList.get(position));
        holder.itemRowBinding.setPos(position);
        holder.itemRowBinding.setChecked(selectedPos == position);
    }

    @Override
    public int getItemCount() {
        return dataModelList == null ? 0 : dataModelList.size();
    }


    @BindingAdapter("app:srcBusinessCategoryPhoto")
    public static void srcBusinessCategoryPhoto(ImageView view, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            RequestOptions options = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.iv_shop_placeholder);
            Glide.with(view.getContext())
                    .load(imageUrl)
                    .apply(options)
                    .into(view);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemBusinessCategoryBinding itemRowBinding;

        public ViewHolder(ItemBusinessCategoryBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }

    public class MyClickHandlers {
        public void onSelectListClick(int pos) {
            if (listener != null) {
                if (pos != RecyclerView.NO_POSITION) {
                    listener.onCategoryItemSelected(dataModelList.get(pos));
                    notifyItemChanged(selectedPos);
                    selectedPos = pos;
                    notifyItemChanged(selectedPos);
                }
            }
        }
    }
}