package com.app.baakki.bottomsheet;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;

import com.app.baakki.databinding.DialogCalenderBinding;
import com.app.baakki.databinding.DialogStartTimeBinding;
import com.app.baakki.listener.StartTimeListener;
import com.app.baakki.model.mycampaignlist.Campaigntime;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class StartTimeBottomSheet extends BottomSheetDialogFragment implements TimePicker.OnTimeChangedListener {
    DialogStartTimeBinding dataBiding;
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    StartTimeListener startTimeListener;
    String time;
    List<Campaigntime> campaigntimes;
    Calendar c;
    boolean isEdit;
    public StartTimeBottomSheet(StartTimeListener startTimeListener, boolean isEdit, List<Campaigntime> campaigntimes) {
        this.startTimeListener = startTimeListener;
        this.isEdit = isEdit;
        this.campaigntimes = campaigntimes;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.dialog_start_time, null, false);
        dataBiding.setHandlers(new MyClickHandlers());

        init();
        return dataBiding.getRoot();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void init() {
        context = getActivity();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        dataBiding.simpleTimePicker.setOnTimeChangedListener(this);



         c = Calendar.getInstance();
        if (isEdit) {
            time = campaigntimes.get(0).getFromTime();
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
            Date date = null;
            try {
                date = sdf.parse(campaigntimes.get(0).getFromTime());
            } catch (ParseException e) {
            }
            c.setTime(date);
        } else {
            time = c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE);
        }

        dataBiding.simpleTimePicker.setHour(c.get(Calendar.HOUR_OF_DAY));
        dataBiding.simpleTimePicker.setMinute(c.get(Calendar.MINUTE));

    }

    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        String hour,mint;
        c=Calendar.getInstance();

        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, 0);
        if (hourOfDay<10){
            hour="0"+hourOfDay;

        }else {
            hour= String.valueOf(hourOfDay);

        }
        if (minute<10){
            mint="0"+minute;

        }else {
            mint= String.valueOf(minute);

        }
        time = hour + ":" + mint;

    }


    public class MyClickHandlers {
        public void onDoneClick(View view) {
            startTimeListener.startTimeListener(time);
            dismiss();
        }
    }
}