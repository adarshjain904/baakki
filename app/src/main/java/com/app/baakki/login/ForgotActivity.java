package com.app.baakki.login;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityForgotBinding;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.DialogUtility;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;


public class ForgotActivity extends BaseActivity {
    String email;
    Context context;
    CompositeDisposable compositeDisposable;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityForgotBinding activityBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot);
        setWhiteTitle(activityBinding.tool.ivBack, activityBinding.tool.tvNeedSomeHelp);
        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    private void init() {
        context = ForgotActivity.this;
        compositeDisposable=new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);
    }

    private boolean isValid() {
        boolean isValid = true;

        email = activityBinding.etEmail.getText().toString();

        if (TextUtils.isEmpty(activityBinding.etEmail.getText().toString())) {
            activityBinding.etEmail.setError("Enter Email Number");
            isValid = false;
        } else if (!ValidationUtils.isValidEmail(email)) {
            activityBinding.etEmail.setError("Enter valid Email");
            isValid = false;
        }
        return isValid;
    }

    public class MyClickHandlers {
        public void onNextClick(View view) {
            if (isValid()) {
                forgotpasswordService();
            }
        }

    }

    private void forgotpasswordService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class)
                .forgotpassword(email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<JsonElement>() {
                    @Override
                    public void onSuccess(@NonNull JsonElement jsonElement) {
                        progressLoader.dismiss();
                        handleResponse(jsonElement);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    private void handleResponse(JsonElement jsonElement) {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
      //
        if (jsonObject.get("status").getAsBoolean()) {
          //  JsonObject jsonDataObj = jsonObject.get("data").getAsJsonObject();


            DialogUtility.showErrorDialog(context, "Please check your email for a temporary password.", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, SignInActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    Dialog dialog=(Dialog) v.getTag();
                    dialog.dismiss();
                }
            });
        }else
            Toast.makeText(context, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();

    }

}
