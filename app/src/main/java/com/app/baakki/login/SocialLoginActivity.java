package com.app.baakki.login;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.business.HomeBusinessActivity;
import com.app.baakki.business.LoginBusinessActivity;
import com.app.baakki.databinding.ActivityForgotBinding;
import com.app.baakki.databinding.ActivitySocialLoginBinding;

import com.app.baakki.databinding.ActivitySplashBinding;
import com.app.baakki.home.SelectDistanceActivity;
import com.app.baakki.home.ThankYouActivity;
import com.app.baakki.listener.LocationHelper;
import com.app.baakki.model.RegisterRequest;
import com.app.baakki.model.login.LoginSignupData;
import com.app.baakki.model.login.LoginSignupRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.splash.SplashActivity;
import com.app.baakki.util.Constant;
import com.app.baakki.util.DialogUtility;
import com.app.baakki.util.LocationBuilder;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Arrays;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.app.baakki.util.Constant.GOOGLE_SIGN_IN;
import static com.app.baakki.util.Constant.EMAIL;
import static com.app.baakki.util.ManifestPermission.hasPermissions;

public class SocialLoginActivity extends BaseActivity /*implements LocationHelper*/ {
    Context context;
    ProgressLoader progressLoader;
    CompositeDisposable compositeDisposable;
    SharedPreference sharedPreference;
    ActivitySocialLoginBinding activityBinding;
    GoogleSignInClient mGoogleSignInClient;
    CallbackManager mCallbackManager;
    String deviceId,facebookEmail; /*lat, lng,
     LocationBuilder locationBuilder;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 1;
    String[] permissions = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
    };*/

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCallbackManager = CallbackManager.Factory.create();
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_social_login);
        setWhiteTitle(activityBinding.tool.ivBack, activityBinding.tool.tvNeedSomeHelp);
        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    @SuppressLint("HardwareIds")
    private void init() {
        context = SocialLoginActivity.this;
        progressLoader = new ProgressLoader(context);
        compositeDisposable = new CompositeDisposable();
        sharedPreference = SharedPreference.getInstance(context);

        deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        setSignUpClick();
        setBusinessSignInClick();
        googleIntegration();

       /* if (!hasPermissions(this, permissions)) {
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
        } else {
            statusCheck();
        }*/
    }


    public class MyClickHandlers {
        public void onGoogleClick(View view) {
           /* if (DialogUtility.isLocationEnable(context)) {
                ShowLocationDialog();
            } else*/
                signInGoogle();
        }

        public void onFacebookClick(View view) {
           /* if (DialogUtility.isLocationEnable(context)) {
                ShowLocationDialog();
            } else {*/
                activityBinding.loginButton.performClick();
                facebookIntegration();
           /* }*/

        }

        public void onNormalClick(View view) {
            Intent intent = new Intent(context, SignInActivity.class);
            startActivity(intent);
        }

    }


    private void setSignUpClick() {
        SpannableString spannableString = new SpannableString(activityBinding.tvDontAccount.getText().toString());
        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent intent = new Intent(context, SignUpActivity.class);
                startActivity(intent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(context.getResources().getColor(R.color.yellow));
                ds.setUnderlineText(false);
            }
        };
        spannableString.setSpan(clickableSpan1, 23, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        activityBinding.tvDontAccount.setText(spannableString);
        activityBinding.tvDontAccount.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void setBusinessSignInClick() {
        SpannableString spannableString = new SpannableString(activityBinding.tvBusinessLogin.getText().toString());
        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent intent = new Intent(context, LoginBusinessActivity.class);
                startActivity(intent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(context.getResources().getColor(R.color.yellow));
                ds.setUnderlineText(false);
            }
        };
        spannableString.setSpan(clickableSpan1, 1, 8, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        activityBinding.tvBusinessLogin.setText(spannableString);
        activityBinding.tvBusinessLogin.setMovementMethod(LinkMovementMethod.getInstance());
    }

    // google integration
    public void googleIntegration() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail().build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void signInGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, GOOGLE_SIGN_IN);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            sharedPreference.putBoolean(Constant.IS_GOOGLE,true);
            socialLoginService(account.getId(), account.getDisplayName(), account.getEmail(), Constant.GOOGLE);
        } catch (ApiException e) {
            Log.e("signInResult:failedcode", "=-=" + e.getStatusCode());
        }
    }/*--------*/


    //facebook Integration
    private void facebookIntegration() {
       // activityBinding.loginButton.setPermissions(Arrays.asList(EMAIL));
        activityBinding.loginButton.setPermissions("email");

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        requestObjectUser(loginResult.getAccessToken());
                        Log.e("facebook", "====================>>>>>> onSuccess");
                    }

                    @Override
                    public void onCancel() {
                        Log.e("facebook", "====================>>>>>> onCancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.e("facebook", "====================>>>>>> onError");
                    }
                });
    }

    private void requestObjectUser(final AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                if (object != null) {
                    Log.e("facebook json is", "====================>>>>>>" + object);
                    try {
                        String facebookId = object.getString("id");
                        String facebookName = object.getString("name");
                        if (object.has("email")) {
                            facebookEmail = object.getString("email");
                        }
                        sharedPreference.putBoolean(Constant.IS_FACEBOOK,true);
                        socialLoginService(facebookId, facebookName, facebookEmail, Constant.FACEBOOK);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "facebook login failed", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }   /*--------*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
         // facebook callback
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        // google callback
        if (requestCode == GOOGLE_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }

       /* if (requestCode==1001){
            statusCheck();
        }*/
    }

    private void socialLoginService(String id, String name, String email, String type) {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).userSocialRegisterApi(name, email, id, type, deviceId, Constant.DEVICE_TYPE/*lat, lng*/)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<LoginSignupRes>() {
                    @Override
                    public void onSuccess(@NonNull LoginSignupRes loginSignupRes) {
                        progressLoader.dismiss();
                        handleResponse(loginSignupRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    private void handleResponse(@NonNull LoginSignupRes loginSignupRes) {
        if (loginSignupRes.getStatus()) {
            sharedPreference.putBoolean(Constant.IS_LOGIN, true);

            sharedPreference.putString(Constant.API_TOKEN, loginSignupRes.getLoginSignupData().getToken());
          //  sharedPreference.putString(Constant.COUNTRY_CODE,"GB");

            Intent intent = new Intent(context, SelectDistanceActivity.class);
            sharedPreference.putBoolean(Constant.IS_NEW_USER,true);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(context, loginSignupRes.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

   /* @Override
    public void getLocation(LocationResult locationResult) {
        for (Location location : locationResult.getLocations()) {
            if (location.getLatitude() == 0 && location.getLongitude() == 0) {
                Log.e("null lat log", "=--=-=");
                return;
            }
            lat = String.valueOf(location.getLatitude());
            lng = String.valueOf(location.getLongitude());
            //  address = locationBuilder.getAddress(context, location.getLatitude(), location.getLongitude());
        }
    }*/

   /* @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {
                    if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.e("msg", "<--location granted");
                            //count++;
                        } else {
                            // DialogUtility.showToast(this, "Permission is required...");
                        }
                    } else if (permissions[i].equals(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.e("msg", "<--read phone granted");
                           // count++;
                        } else {
                            //DialogUtility.showToast(this, "Permission is required...");
                        }
                    }
                }
            }
            if (hasPermissions(this, permissions)) {
                statusCheck();
            }

            // activityBinding.ivSplash.post(new Starter());
        }
    }

    public void statusCheck() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean isGpsProviderEnabled, isNetworkProviderEnabled;
        isGpsProviderEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkProviderEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGpsProviderEnabled && !isNetworkProviderEnabled) {
            buildAlertMessageNoGps();
        } else{
            locationBuilder = new LocationBuilder(SocialLoginActivity.this);
            locationBuilder.getLocation(SocialLoginActivity.this);
        }

    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Location Permission");
        builder.setMessage("The app needs location permissions. Please grant this permission to continue using the features of the app.");
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 1001);
            }
        });

        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.show();
    }*/


}