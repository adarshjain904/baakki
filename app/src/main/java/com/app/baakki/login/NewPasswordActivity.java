package com.app.baakki.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityChangePasswordBinding;
import com.app.baakki.databinding.ActivityForgotPasswordBinding;
import com.app.baakki.other.ChangedPasswordActivity;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;

public class NewPasswordActivity extends BaseActivity {
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityForgotPasswordBinding activityBinding;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        setWhiteTitle(activityBinding.tool.ivBack, activityBinding.tool.tvNeedSomeHelp);
        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    private void init() {
        context = NewPasswordActivity.this;
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

    }

    public class MyClickHandlers {
        public void onChangedPasswordClick(View view) {
            Intent intent = new Intent(context, SignInActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

        }
    }
}

