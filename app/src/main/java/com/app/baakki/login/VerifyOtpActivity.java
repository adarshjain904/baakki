package com.app.baakki.login;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;


import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityOtpBinding;
import com.app.baakki.home.ThankYouActivity;
import com.app.baakki.model.RegisterRequest;
import com.app.baakki.model.login.LoginSignupData;
import com.app.baakki.model.login.LoginSignupRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class VerifyOtpActivity extends BaseActivity {
    Context context;
    SharedPreference sharedPreference;
    ProgressLoader progressLoader;
    RegisterRequest registerRequest;
    CompositeDisposable compositeDisposable;
    ActivityOtpBinding activityVerifyOtpBinding;
    int counter;
    String code, otp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityVerifyOtpBinding = DataBindingUtil.setContentView(this, R.layout.activity_otp);
        activityVerifyOtpBinding.setHandlers(new MyClickHandlers());

        setWhiteTitle(activityVerifyOtpBinding.tool.ivBack, activityVerifyOtpBinding.tool.tvNeedSomeHelp);

        init();
    }

    private void init() {
        context = VerifyOtpActivity.this;
        compositeDisposable = new CompositeDisposable();
        sharedPreference = SharedPreference.getInstance(this);
        progressLoader = new ProgressLoader(this);

        registerRequest = new Gson().fromJson(getIntent().getStringExtra(Constant.REGISTER_DATA), RegisterRequest.class);
        otp = getIntent().getStringExtra(Constant.OTP);

        activityVerifyOtpBinding.et1.addTextChangedListener(new GenericTextWatcher(activityVerifyOtpBinding.et2, activityVerifyOtpBinding.et1));
        activityVerifyOtpBinding.et2.addTextChangedListener(new GenericTextWatcher(activityVerifyOtpBinding.et3, activityVerifyOtpBinding.et1));
        activityVerifyOtpBinding.et3.addTextChangedListener(new GenericTextWatcher(activityVerifyOtpBinding.et4, activityVerifyOtpBinding.et2));
        activityVerifyOtpBinding.et4.addTextChangedListener(new GenericTextWatcher(activityVerifyOtpBinding.et4, activityVerifyOtpBinding.et3));

        countdownTime();
    }

    private boolean isValid() {
        code = activityVerifyOtpBinding.et1.getText().toString() + activityVerifyOtpBinding.et2.getText().toString() +
                activityVerifyOtpBinding.et3.getText().toString() + activityVerifyOtpBinding.et4.getText().toString();

        boolean isValid = true;
        if (TextUtils.isEmpty(code) || code.length() != 4) {
            Toast.makeText(context, context.getResources().getString(R.string.invalid_code), Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (!otp.equals(code)) {
            Toast.makeText(context, context.getResources().getString(R.string.incorrect_code), Toast.LENGTH_SHORT).show();
            isValid = false;
        }
        return isValid;
    }

    public class MyClickHandlers {
        public void onNextClick(View view) {
            if (isValid()) {
                if (getIntent().getBooleanExtra(Constant.IS_SIGNUP, true)) {
                    userRegisterService();
                } else {
                    Intent intent = new Intent(context, NewPasswordActivity.class);
                    startActivity(intent);
                }

            }
        }

        public void onResendClick(View view) {
            if (activityVerifyOtpBinding.tvResend.getText().equals(getResources().getString(R.string.resend_code))) {
                counter = 0;
                ResendOtpService();

            }
        }
    }

    private void countdownTime() {
        new CountDownTimer(30000, 1000) {
            @SuppressLint("SetTextI18n")
            @Override
            public void onTick(long millisUntilFinished) {
                if (counter < 10) {
                    activityVerifyOtpBinding.tvResend.setText("00:0" + counter);
                } else
                    activityVerifyOtpBinding.tvResend.setText("00:" + counter);
                counter++;
            }

            @Override
            public void onFinish() {
                activityVerifyOtpBinding.tvResend.setText(context.getResources().getString(R.string.resend_code));
            }
        }.start();
    }

    public static class GenericTextWatcher implements TextWatcher {
        private final EditText etPrev;
        private final EditText etNext;

        public GenericTextWatcher(EditText etNext, EditText etPrev) {
            this.etPrev = etPrev;
            this.etNext = etNext;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            if (text.length() == 1)
                etNext.requestFocus();
            else if (text.length() == 0)
                etPrev.requestFocus();
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }
    }


    private void userRegisterService() {
        progressLoader.show();

        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).
                userRegisterApi(registerRequest.getName(), registerRequest.getEmail(), registerRequest.getPassword(),
                        registerRequest.getPassword_confirmation(), registerRequest.getRole_id(), "225",
                        registerRequest.getRegister_via(), registerRequest.getDevice_token(), registerRequest.getDevice_type()
                       /* registerRequest.getLat(), registerRequest.getLon()*/)

                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<LoginSignupRes>() {
                    @Override
                    public void onSuccess(@NonNull LoginSignupRes loginSignupRes) {
                        progressLoader.dismiss();
                        handleResponse(loginSignupRes);

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    private void handleResponse(LoginSignupRes loginSignupRes) {
        if (loginSignupRes.getStatus()) {
            sharedPreference.putBoolean(Constant.IS_LOGIN, true);
            sharedPreference.putString(Constant.API_TOKEN, loginSignupRes.getLoginSignupData().getToken());
            //sharedPreference.putString(Constant.COUNTRY_CODE,loginSignupRes.getLoginSignupData().getCountryIso());
            sharedPreference.putString(Constant.COUNTRY_CODE, "GB");
            sharedPreference.putString(Constant.REGISTER_DATA, new Gson().toJson(registerRequest));

            Intent intent = new Intent(context, ThankYouActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(context, loginSignupRes.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }


    private void ResendOtpService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class)
                .sendEmailVerifyOtp(registerRequest.getEmail())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<JsonElement>() {
                    @Override
                    public void onSuccess(@NonNull JsonElement jsonElement) {
                        progressLoader.dismiss();
                        handleResendResponse(jsonElement);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    private void handleResendResponse(JsonElement jsonElement) {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
      //  Toast.makeText(context, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
        if (jsonObject.get("status").getAsBoolean()) {
            JsonObject jsonDataObj = jsonObject.get("data").getAsJsonObject();

            Log.e(Constant.RESEND_OTP, jsonDataObj.get("otp").getAsString());
            otp = jsonDataObj.get("otp").getAsString();
        }
        countdownTime();
    }


}
