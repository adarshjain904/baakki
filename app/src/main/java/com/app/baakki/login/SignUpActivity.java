package com.app.baakki.login;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;

import com.app.baakki.bottomsheet.BusinessCategoryBottomSheet;
import com.app.baakki.bottomsheet.CountryBottomSheet;
import com.app.baakki.databinding.ActivitySignupBinding;
import com.app.baakki.home.ThankYouActivity;
import com.app.baakki.listener.LocationHelper;
import com.app.baakki.listener.OnCountryClickListener;
import com.app.baakki.model.RegisterRequest;
import com.app.baakki.model.country.CountryData;
import com.app.baakki.model.country.CountryRes;
import com.app.baakki.model.login.LoginSignupData;
import com.app.baakki.model.login.LoginSignupRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.DialogUtility;
import com.app.baakki.util.LocationBuilder;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationResult;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SignUpActivity extends BaseActivity /*implements LocationHelper */{
    String name, email, pass, confirmPass, deviceId; /*address, lat, lon*/; /*countryId, countryCode*/;
    Context context;
    CompositeDisposable compositeDisposable;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivitySignupBinding activityBinding;
    LocationBuilder locationBuilder;
    List<CountryData> countryData;
    RegisterRequest registerRequest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_signup);
        setWhiteTitle(activityBinding.tool.ivBack, activityBinding.tool.tvNeedSomeHelp);
        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    @SuppressLint("HardwareIds")
    private void init() {
        context = SignUpActivity.this;
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);
        deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

      /*  locationBuilder = new LocationBuilder(SignUpActivity.this);
        locationBuilder.getLocation(SignUpActivity.this);*/

        setSignInClick();
       // countryService();
    }


    private boolean isValid() {
        boolean isValid = true;

        name = activityBinding.etName.getText().toString();
        email = activityBinding.etEmail.getText().toString();
        pass = activityBinding.etPass.getText().toString();
        confirmPass = activityBinding.etConPass.getText().toString();

        if (TextUtils.isEmpty(name)) {
            activityBinding.etName.setError(getString(R.string.enter_name));
            isValid = false;
        } else if (TextUtils.isEmpty(activityBinding.etEmail.getText().toString())) {
            activityBinding.etEmail.setError(getString(R.string.enter_email));
            isValid = false;
        } else if (!ValidationUtils.isValidEmail(email)) {
            activityBinding.etEmail.setError(getString(R.string.valid_email));
            isValid = false;
        } else if (TextUtils.isEmpty(pass)) {
            activityBinding.etPass.setError(getString(R.string.enter_password));
            isValid = false;
        } else if (pass.length() < 6) {
            activityBinding.etPass.setError(getString(R.string.enter_4_digit));
            isValid = false;
        } else if (TextUtils.isEmpty(confirmPass)) {
            activityBinding.etConPass.setError(getString(R.string.enter_con_password));
            isValid = false;
        } else if (!pass.equals(confirmPass)) {
            activityBinding.etConPass.setError(getString(R.string.pass_dont_match));
            isValid = false;
        }/* else if (TextUtils.isEmpty(countryId)) {
            Toast.makeText(context, "Please select Country", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (!activityBinding.cbAllowStore.isChecked()) {
            Toast.makeText(context, "Please select privacy policy and terms & condition", Toast.LENGTH_SHORT).show();
            isValid = false;
        }else if (!activityBinding.cbTermCondition.isChecked()) {
            Toast.makeText(context, "Please select terms & condition", Toast.LENGTH_SHORT).show();
            isValid = false;
        }*/

        return isValid;
    }

    public class MyClickHandlers implements OnCountryClickListener {
        public void onSignUpClick(View view) {
            if (isValid())
                getData();
        }

        public void onCountryClick(View view) {
            new CountryBottomSheet(this, countryData).show(getSupportFragmentManager(), "BusinessCategoryBottomSheet");
        }

        @Override
        public void onCategoryItemSelected(CountryData item) {
           /* countryCode = String.valueOf(item.getIso());
            countryId = String.valueOf(item.getId());*/
            activityBinding.tvCountry.setText(item.getName());
        }
    }


   /* @Override
    public void getLocation(LocationResult locationResult) {
        for (Location location : locationResult.getLocations()) {

            if (location.getLatitude() == 0 && location.getLongitude() == 0) {
                Log.e("null lat log", "=--=-=");
                return;
            }

            lat = String.valueOf(location.getLatitude());
            lon = String.valueOf(location.getLongitude());
            address = locationBuilder.getAddress(context, location.getLatitude(), location.getLongitude());

        }

    }*/

    private void getData() {
        registerRequest = new RegisterRequest();
        registerRequest.setName(name);
        registerRequest.setEmail(email);
        registerRequest.setPassword(pass);
       /* registerRequest.setLat(lat);
        registerRequest.setLon(lon);*/
        registerRequest.setPassword_confirmation(confirmPass);
        registerRequest.setRole_id(Constant.USER_ROLE_ID);
        registerRequest.setRegister_via(Constant.NORMAL);
        registerRequest.setDevice_token(deviceId);
        registerRequest.setDevice_type(Constant.DEVICE_TYPE);
    /*    registerRequest.setCountry_id(countryId);
        registerRequest.setCountryCode(countryCode);*/

     /*   if (DialogUtility.isLocationEnable(context)) {
            ShowLocationDialog();
        } else*/
            signupService();

    }

    private void ShowLocationDialog() {
        DialogUtility.alertMessageNoGps(context, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, Constant.LOCATION_RESULT);
            }
        });
    }

    private void setSignInClick() {
        SpannableString spannableString = new SpannableString(activityBinding.tvHaveAccount.getText().toString());
        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent intent = new Intent(context, SignInActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(context.getResources().getColor(R.color.yellow));
                ds.setUnderlineText(false);
            }
        };

        spannableString.setSpan(clickableSpan1, 25, 33, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        activityBinding.tvHaveAccount.setText(spannableString);
        activityBinding.tvHaveAccount.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void countryService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).countrylistApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CountryRes>() {
                    @Override
                    public void onSuccess(@NonNull CountryRes countryRes) {
                        progressLoader.dismiss();
                        handleResponse(countryRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    private void handleResponse(CountryRes countryRes) {
        if (countryRes.getStatus()) {
            countryData = countryRes.getData();
        } else {
            Toast.makeText(context, countryRes.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    private void signupService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class)
                .sendEmailVerifyOtp(registerRequest.getEmail())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<JsonElement>() {
                    @Override
                    public void onSuccess(@NonNull JsonElement jsonElement) {
                        progressLoader.dismiss();
                        handleResponse(jsonElement);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    private void handleResponse(JsonElement jsonElement) {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
      //  Toast.makeText(context, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
        if (jsonObject.get("status").getAsBoolean()) {
            JsonObject jsonDataObj = jsonObject.get("data").getAsJsonObject();

            Log.e(Constant.OTP, jsonDataObj.get("otp").getAsString());
            Intent intent = new Intent(context, VerifyOtpActivity.class);
            intent.putExtra(Constant.REGISTER_DATA, new Gson().toJson(registerRequest));
            intent.putExtra(Constant.OTP, jsonDataObj.get("otp").getAsString());
            intent.putExtra(Constant.IS_SIGNUP, true);
            startActivity(intent);
        }else
            Toast.makeText(context, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
    }

   /* @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.LOCATION_RESULT) {
            if (DialogUtility.isLocationEnable(context)) {
                ShowLocationDialog();
            } else
                signupService();
        }
    }*/
}
