package com.app.baakki.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.app.baakki.business.LoginBusinessActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Husain
 */
public class ValidationUtils {
    private Context context;
    private final static String NAME_PATTERN = "^[a-zA-ZéüöêåÁÅÉá .´'`-].{1,50}$";
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]{1,2}+)*(\\.[A-Za-z]{1,})$";
    private static final String USERNAME_PATTERN = "^[a-zA-Z0-9_-]{3,15}$";
    private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})";
    private static final String MobilePattern = "[0-9]{6,10}";
    private static final String UK_POSTCODE_PATTERN = "(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})";
    private static final String CARD_DATE_VALID = ("(?:0[1-9]|1[0-2])/[0-9]{2}");

    /**
     * Validates email with regular expression
     *
     * @param email the email to validate
     * @return true if email is valid, false otherwise
     */
    public static boolean isValidEmail(String email) {

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email.toLowerCase());

        return matcher.matches();
    }

    /**
     * Validates user name with regular expression
     *
     * @param username the username to validate
     * @return true if user name is valid, false otherwise
     */
    public static boolean isValidUsername(String username) {
        Pattern pattern = Pattern.compile(USERNAME_PATTERN);
        Matcher matcher = pattern.matcher(username);

        return matcher.matches();
    }

    public static boolean isValidString(String username, int limit) {
        return (username != null && username.length() >= 6 || username.length() >= limit) ? true : false;
    }

    public static boolean isValidFirstName(String firstname) {
        Pattern pattern = Pattern.compile(NAME_PATTERN);
        Matcher matcher = pattern.matcher(firstname);
        return matcher.matches();
    }

    /**
     * Validate password with regular expression
     *
     * @param password password for validation
     * @return true valid password, false invalid password
     */
    public static boolean isValidPassword(final String password) {

        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();

    }

    //---- validation on mobile number..
    public static boolean isValidMobile(String phone) {
        Pattern pattern = Pattern.compile(MobilePattern);
        Matcher matcher = pattern.matcher(phone);
        return matcher.matches();
    }

    public static boolean isCardDateValid(String input) {
        Pattern pattern = Pattern.compile(CARD_DATE_VALID);
        Matcher matcher = pattern.matcher(input);
        return matcher.matches();
    }

    public static boolean isCreditCardDateValid(String input) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/yy");
        simpleDateFormat.setLenient(false);
        Date expiry = null;
        try {
            expiry = simpleDateFormat.parse(input);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
        return expiry.before(new Date());
    }

    public static boolean isCheckPassLength(final String word) {
        if (word.length() < 4) {
            return true;
        }
        return false;
    }

    public static boolean isEmptyString(final String word) {
        if (word == null || word.equalsIgnoreCase("")) {
            return true;
        }
        return false;
    }

    public static boolean isEqualString(String word, String word2) {
        if ((word != null && !word.equalsIgnoreCase("")) && (word2 != null || !word2.equalsIgnoreCase(""))) {
            if (word.equalsIgnoreCase(word2)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static void errorCode(Context context, String errorMsg) {
        Log.e("onError: ", errorMsg);
        if (errorMsg.contains("HTTP 400"))
            Toast.makeText(context, "Not Found", Toast.LENGTH_SHORT).show();
        else if (errorMsg.contains("HTTP 422"))
            Toast.makeText(context, "The request could not be completed due to a conflict with the current state of the resource", Toast.LENGTH_SHORT).show();
        else if (errorMsg.contains("HTTP 404"))
            Toast.makeText(context, "Unauthorised User", Toast.LENGTH_SHORT).show();
        else if (errorMsg.contains("HTTP 500"))
            Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show();
    }
}
