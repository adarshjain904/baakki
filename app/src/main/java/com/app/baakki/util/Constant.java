package com.app.baakki.util;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Constant {
    public static final int AUTOCOMPLETE_REQUEST_CODE = 11;
    public static final Integer GOOGLE_SIGN_IN=101;
    public static final Integer FACEBOOK_SIGN_IN=201;
    public static final Integer SHOP_LIST_RESULT=301;
    public static final Integer EDIT_CAMP_REQUEST=401;
    public static final Integer EDIT_CAMP_RESULT=501;
    public static final Integer LOCATION_RESULT=601;
    public static final Integer RESERVE_CODE=701;


    public static final String API_TOKEN = "api_token";
    public static final String IS_LOGIN = "is_login";
    public static final String IS_GOOGLE = "is_google";
    public static final String IS_FACEBOOK = "is_fb";
    public static final String IS_SIGNUP ="is_signup" ;
    public static final String IS_LOGIN_BUSINESS = "is_login_business";
    public static final String IS_SELECT_LOCATION = "is_location";
    public static final String IS_SELECT_CITY = "is_city";

    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_IMAGE ="user_image";

    public static final String LOGIN = "login";
    public static final String SIGNUP = "singup";
    public static final String FORGOT = "forgot";
    public static final String TOKEN = "token";

    public static final String USER_ROLE_ID="2";
    public static final String VENDER_ROLE_ID="3";

    public static final String ONE_TIME="onetime";
    public static final String RECURRING="recurring";

    public static final String NORMAL = "normal";
    public static final String GOOGLE = "google";
    public static final String FACEBOOK = "facebook";

    public static final String DEVICE_TYPE = "android";


    public static final String REGISTER_DATA = "register_data";
    public static final String RESERVE_DATA = "reserve_data";
    public static final String RESERVE_DATA_ARRAY = "reserve_data_array";
    public static final String CATEGORY_DATA = "category_data";
    public static final String PRODUCT_DETAIL_DATA = "product_detail_data";
    public static final String EMAIL = "email";
    public static final String COUNTRY_CODE = "countryCode";

    public static final String LOCATION_DATA ="location_data" ;
    public static final String OTP ="otp" ;
    public static final String RESEND_OTP ="re-otp" ;
    public static final String IS_NEW_USER ="isUser" ;

    public static  String serverDate(String date) {
        if (!TextUtils.isEmpty(date)) {
            SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm");
            SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
            try {
                return parseFormat.format(displayFormat.parse(date));
            } catch (ParseException e) {
                e.printStackTrace();
                return date;
            }
        } else {
            return date;
        }
    }
}
