package com.app.baakki.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.ProgressBar;

import com.app.baakki.R;


public class ProgressLoader extends Dialog {

	public ProgressLoader(Context context) {
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		setContentView(R.layout.dialog_progress);
		ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);
		progressBar.setIndeterminate(true);
		setCancelable(false);
	}
}
