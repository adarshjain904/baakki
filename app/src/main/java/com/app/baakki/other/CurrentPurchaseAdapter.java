package com.app.baakki.other;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.databinding.ItemCurrentPurchaseBinding;
import com.app.baakki.databinding.ItemFavBinding;
import com.app.baakki.discover.ReserveOrderActivity;
import com.app.baakki.model.reserveorder.ReserveOrderData;

import java.util.List;

import static com.app.baakki.util.Constant.serverDate;

public class CurrentPurchaseAdapter extends RecyclerView.Adapter<CurrentPurchaseAdapter.ViewHolder> {
    private Context context;
    List<ReserveOrderData> dataModelList;
    CurrentPurchaseFragment.MyClickListener myClickListener;

    public CurrentPurchaseAdapter(Context ctx, CurrentPurchaseFragment.MyClickListener myClickListener) {
        context = ctx;
        this.myClickListener = myClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemCurrentPurchaseBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_current_purchase, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemRowBinding.setData(dataModelList.get(position));
        holder.itemRowBinding.setHandler(myClickListener);
        holder.itemRowBinding.tvOriginalPrice.setPaintFlags(holder.itemRowBinding.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.itemRowBinding.tvDiscoverTime.setText(serverDate(dataModelList.get(position)
                .getFromTime()) + " to " + serverDate(dataModelList.get(position).getToTime()) + "  \u2022 " + dataModelList.get(position).getDistance() + " km");

        int diff = (dataModelList.get(position).getOriginalPrice() - dataModelList.get(position).getOfferPrice()) * 100;
        double percentage = (diff) / dataModelList.get(position).getOriginalPrice();
        holder.itemRowBinding.tvPercentage.setText(String.valueOf((int) percentage) + "% off");
    }

    public void setData(List<ReserveOrderData> dataModelList) {
        this.dataModelList = dataModelList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dataModelList == null ? 0 : dataModelList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemCurrentPurchaseBinding itemRowBinding;

        public ViewHolder(ItemCurrentPurchaseBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }
}