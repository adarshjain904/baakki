package com.app.baakki.other;

import android.Manifest;
import android.content.Context;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.bottomsheet.CountryBottomSheet;
import com.app.baakki.databinding.ActivityMyProfileBinding;;
import com.app.baakki.home.HomeActivity;
import com.app.baakki.listener.LocationHelper;
import com.app.baakki.listener.OnCountryClickListener;
import com.app.baakki.model.country.CountryData;
import com.app.baakki.model.country.CountryRes;
import com.app.baakki.model.login.LoginSignupData;
import com.app.baakki.model.login.LoginSignupRes;
import com.app.baakki.model.pendingcompletecampaign.PendingCompleteCampaignRes;
import com.app.baakki.model.profile.ProfileRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.DialogUtility;
import com.app.baakki.util.LocationBuilder;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.location.LocationResult;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;

import static com.app.baakki.util.Constant.SHOP_LIST_RESULT;
import static com.app.baakki.util.ManifestPermission.hasPermissions;

public class MyProfileActivity extends BaseActivity /*implements LocationHelper*/ {
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityMyProfileBinding activityBinding;
    CompositeDisposable compositeDisposable;
    String picturePath;
    String[] PERMISSIONS = {
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.CAMERA
    };
    int PERMISSION_ALL = 1;
    /*  LocationBuilder locationBuilder;

    */
    List<CountryData> countryData;

    String name, mobile, countryId, device_token, device_type, currentLat, currentLong, address, countryCode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_profile);
        setToolbar(activityBinding.tool.ivBack, activityBinding.tool.tvTitle, activityBinding.tool.vwLine, getString(R.string.my_profile));
        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    private void init() {
        context = MyProfileActivity.this;
        progressLoader = new ProgressLoader(context);
        compositeDisposable = new CompositeDisposable();
        sharedPreference = SharedPreference.getInstance(context);

       /* locationBuilder = new LocationBuilder(this);
        locationBuilder.getLocation(this);
        if (DialogUtility.isLocationEnable(context)) {
            ShowLocationDialog();
        }*/

        activityBinding.tool.ivSave.setVisibility(View.VISIBLE);
        myInfoService();

    }


    public class MyClickHandlers implements OnCountryClickListener {
        public void onCountryClick(View view) {
            new CountryBottomSheet(this, countryData).show(getSupportFragmentManager(), "BusinessCategoryBottomSheet");
        }

        @Override
        public void onCategoryItemSelected(CountryData item) {
            countryCode = String.valueOf(item.getIso());
            countryId = String.valueOf(item.getId());
            activityBinding.tvCountry.setText(item.getName());
        }

        public void onSaveClick(View view) {
          /*  if (DialogUtility.isLocationEnable(context)) {
                ShowLocationDialog();
            } else*/
            if (isValid())
                updateProfileService();

        }

        @RequiresApi(api = Build.VERSION_CODES.M)
        public void onSelectImageClick(View view) {
            selectImage();
        }
    }

    private void countryService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).countrylistApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CountryRes>() {
                    @Override
                    public void onSuccess(@NonNull CountryRes countryRes) {
                        progressLoader.dismiss();
                        handleResponse(countryRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    private void handleResponse(CountryRes countryRes) {
        if (countryRes.getStatus()) {
            countryData = countryRes.getData();
        } else {
            Toast.makeText(context, countryRes.getMessage(), Toast.LENGTH_SHORT).show();
        }

        //  myInfoService();
    }

    private void myInfoService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).myInfoApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<LoginSignupRes>() {
                    @Override
                    public void onSuccess(@NonNull LoginSignupRes loginSignupRes) {
                        progressLoader.dismiss();
                        if (loginSignupRes.getStatus()) {
                            handleResponse(loginSignupRes.getLoginSignupData());
                        } else {
                            Toast.makeText(context, loginSignupRes.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    private void handleResponse(LoginSignupData loginSignupData) {
        activityBinding.setProfileData(loginSignupData);
       /* countryId = loginSignupData.getCountryId();
        countryCode = loginSignupData.getCountryIso();
        for (CountryData countryModel : countryData) {
            if (String.valueOf(countryModel.getId()).equals(loginSignupData.getCountryId())) {
                activityBinding.tvCountry.setText(countryModel.getName());
            }
        }*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }

    @BindingAdapter("app:srcProfilePhoto")
    public static void srcProfilePhoto(ImageView view, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            RequestOptions options = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.ic_user_placeholder);
            Glide.with(view.getContext())
                    .load(imageUrl)
                    .apply(options)
                    .into(view);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void selectImage() {
        if (!hasPermissions(this, PERMISSIONS)) {
            Log.e("enter", "=-=-");
           /* boolean showRationale = shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA);
            if (!showRationale) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            } else */
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);

        } else {
            onSelectImageClick();
        }
    }

    public void onSelectImageClick() {
        CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).start(this);
    }

   /* private void ShowLocationDialog() {
        DialogUtility.alertMessageNoGps(context, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, Constant.LOCATION_RESULT);
            }
        });
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       /* if (requestCode == Constant.LOCATION_RESULT) {
            if (DialogUtility.isLocationEnable(context)) {
                ShowLocationDialog();
            }
        } else*/ if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            activityBinding.ivProfileImage.setImageURI(result.getUri());
            picturePath = result.getUri().getPath();
            Log.e("picturepath", picturePath);
        }
    }

   /* @Override
    public void getLocation(LocationResult locationResult) {
        for (Location location : locationResult.getLocations()) {
            if (location.getLatitude() == 0 && location.getLongitude() == 0) {
                Log.e("null lat log", "=--=-=");
                return;
            }
            currentLat = String.valueOf(location.getLatitude());
            currentLong = String.valueOf(location.getLongitude());
            address = locationBuilder.getAddress(context, location.getLatitude(), location.getLongitude());
        }
    }*/

    private boolean isValid() {
        boolean isValid = true;

        name = activityBinding.etName.getText().toString();
        mobile = activityBinding.etMob.getText().toString();

        device_token = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

       /* if (TextUtils.isEmpty(picturePath)) {
            Toast.makeText(context, "Select campaign image", Toast.LENGTH_SHORT).show();
            isValid = false;
        }else*/
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(context, "Please enter name", Toast.LENGTH_SHORT).show();
            isValid = false;
        }/* else if (TextUtils.isEmpty(mobile)) {
            Toast.makeText(context, "Please enter mobile number", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (TextUtils.isEmpty(countryId)) {
            Toast.makeText(context, "Please select Country", Toast.LENGTH_SHORT).show();
            isValid = false;
        }*/
        return isValid;
    }

    private void updateProfileService() {
        MultipartBody.Part image = null;
        HashMap<String, RequestBody> data = new HashMap<>();
        data.put("name", createRequestBody(name));
        data.put("mobile", createRequestBody("0123456789"));
        //data.put("mobile", createRequestBody(mobile));
        //  data.put("country_id", createRequestBody(countryId));
        data.put("device_token", createRequestBody(device_token));
        data.put("device_type", createRequestBody(Constant.DEVICE_TYPE));
      /*  data.put("lat", createRequestBody(currentLat));
        data.put("lon", createRequestBody(currentLong));*/

        if (picturePath != null) {
            File file = new File(picturePath);
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            image = MultipartBody.Part.createFormData("profile", file.getName(), requestFile);
        }

        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).userProfileUpdateApi(data, image)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ProfileRes>() {
                    @Override
                    public void onSuccess(@NonNull ProfileRes profileRes) {
                        progressLoader.dismiss();
                        handleUpdateResponse(profileRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    private void handleUpdateResponse(ProfileRes profileRes) {

        if (profileRes.getStatus()) {
            // sharedPreference.putString(Constant.COUNTRY_CODE, countryCode);
            finish();
        } else
            Toast.makeText(context, profileRes.getMessage(), Toast.LENGTH_SHORT).show();
    }

    public RequestBody createRequestBody(@NonNull String s) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), s);
    }


}
