package com.app.baakki.other;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityMyPurchaseBinding;
import com.app.baakki.listener.LocationHelper;
import com.app.baakki.listener.PurchaseDataListnere;
import com.app.baakki.model.ChooseLocation;
import com.app.baakki.model.gelallfav.GetAllFavRes;
import com.app.baakki.model.pendingcompletecampaign.PendingCompleteCampaignRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.DialogUtility;
import com.app.baakki.util.LocationBuilder;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.android.gms.location.LocationResult;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.app.baakki.util.ManifestPermission.hasPermissions;

public class MyPurchaseActivity extends BaseActivity implements LocationHelper {
    ActivityMyPurchaseBinding activityMyPurchaseBinding;
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    CompositeDisposable compositeDisposable;
    PendingCompleteCampaignRes pendingCompleteCampaignRes;
    private int indicatorWidth;
    ChooseLocation chooseLocation;
    String currentLat, currentLong, address;
    LocationBuilder locationBuilder;
    boolean isLocation = false;
    String[] permissions = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
    };
   /*
   */
    // adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMyPurchaseBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_purchase);
        setToolbar(activityMyPurchaseBinding.tool.ivBack, activityMyPurchaseBinding.tool.tvTitle, activityMyPurchaseBinding.tool.vwLine, getString(R.string.my_purchase));

        context = MyPurchaseActivity.this;
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        if (hasPermissions(this, permissions)) {
            if (!DialogUtility.isLocationEnable(context)) {
                progressLoader.show();

                locationBuilder = new LocationBuilder(this);
                locationBuilder.getLocation(this);
            }else {
                getLocalLatLog();
            }
        } else {
            getLocalLatLog();
        }
          /* locationBuilder = new LocationBuilder(this);
        locationBuilder.getLocation(this);

        if (DialogUtility.isLocationEnable(context))
            ShowLocationDialog();*/
    }

   public void getLocalLatLog(){
        chooseLocation = new Gson().fromJson(sharedPreference.getString(Constant.LOCATION_DATA), ChooseLocation.class);
        currentLat = String.valueOf(chooseLocation.getLatLng().latitude);
        currentLong = String.valueOf(chooseLocation.getLatLng().longitude);
        getPendingCompletedCampaignService();
    }


    @Override
    public void getLocation(LocationResult locationResult) {
        for (Location location : locationResult.getLocations()) {
            if (location.getLatitude() == 0 && location.getLongitude() == 0) {
                Log.e("null lat log", "=--=-=");
                return;
            }
            currentLat = String.valueOf(location.getLatitude());
            currentLong = String.valueOf(location.getLongitude());
            address = locationBuilder.getAddress(context, location.getLatitude(), location.getLongitude());


            if (!isLocation) {
                isLocation = true;
                getPendingCompletedCampaignService();
            }
        }
    }

    private void getPendingCompletedCampaignService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).
                getPendingCompletedCampaignApi(currentLat, currentLong)
                /* getPendingCompletedCampaignApi()*/
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<PendingCompleteCampaignRes>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull PendingCompleteCampaignRes pendingCompleteCampaignRes) {
                        progressLoader.dismiss();
                        handleFavoritesResponse(pendingCompleteCampaignRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        activityMyPurchaseBinding.tab.setVisibility(View.GONE);
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void handleFavoritesResponse(PendingCompleteCampaignRes pendingCompleteCampaignRes) {
        this.pendingCompleteCampaignRes = pendingCompleteCampaignRes;
        setUpTab();
    }

    private void setUpTab() {
        //Set up the view pager and fragments
        TabFragmentAdapter adapter = new TabFragmentAdapter(getSupportFragmentManager());
        activityMyPurchaseBinding.viewPager.setAdapter(adapter);
        activityMyPurchaseBinding.tab.setupWithViewPager(activityMyPurchaseBinding.viewPager);


        activityMyPurchaseBinding.tab.post(new Runnable() {
            @Override
            public void run() {
                indicatorWidth = activityMyPurchaseBinding.tab.getWidth() / activityMyPurchaseBinding.tab.getTabCount();
                //Assign new width
                FrameLayout.LayoutParams indicatorParams = (FrameLayout.LayoutParams) activityMyPurchaseBinding.indicator.getLayoutParams();
                indicatorParams.width = indicatorWidth;
                activityMyPurchaseBinding.indicator.setLayoutParams(indicatorParams);
            }
        });

        activityMyPurchaseBinding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float positionOffset, int positionOffsetPx) {
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) activityMyPurchaseBinding.indicator.getLayoutParams();
                float translationOffset = (positionOffset + i) * indicatorWidth;
                params.leftMargin = (int) translationOffset;
                activityMyPurchaseBinding.indicator.setLayoutParams(params);
            }

            @Override
            public void onPageSelected(int i) {


            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    public class TabFragmentAdapter extends FragmentPagerAdapter {
        private String tabTitles[] = new String[]{"Previous", "Current"};

        public TabFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = new PreviousPurchaseFragment().newInstance(context, pendingCompleteCampaignRes.getData().getCompleted());
                    break;
                case 1:
                    fragment = new CurrentPurchaseFragment().newInstance(context, pendingCompleteCampaignRes.getData().getPending());
                    break;
                default:
                    break;
            }
            return fragment;
        }


        @Override
        public int getCount() {
            return tabTitles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }


    }


}