package com.app.baakki.other;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.databinding.ItemFavBinding;
import com.app.baakki.databinding.ItemPreviousPurchaseBinding;
import com.app.baakki.model.reserveorder.ReserveOrderData;

import java.util.List;

import static com.app.baakki.util.Constant.serverDate;


public class PreviousPurchaseAdapter extends RecyclerView.Adapter<PreviousPurchaseAdapter.ViewHolder> {
    private Context context;
    List<ReserveOrderData> dataModelList;

    public PreviousPurchaseAdapter(Context ctx) {
        context = ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemPreviousPurchaseBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_previous_purchase, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemRowBinding.setData(dataModelList.get(position));
        //    holder.itemRowBinding.setHandler(myClickListener);
        holder.itemRowBinding.tvOriginalPrice.setPaintFlags(holder.itemRowBinding.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.itemRowBinding.tvDiscoverTime.setText(serverDate(dataModelList.get(position)
                .getFromTime()) + " to " + serverDate(dataModelList.get(position).getToTime()) + "  \u2022 " + dataModelList.get(position).getDistance() + " km");

        int diff = (dataModelList.get(position).getOriginalPrice() - dataModelList.get(position).getOfferPrice()) * 100;
        double percentage = (diff) / dataModelList.get(position).getOriginalPrice();
        holder.itemRowBinding.tvPercentage.setText(String.valueOf((int) percentage) + "% off");

        if (dataModelList.get(position).getOrderStatus().equals("reserve")) {
            holder.itemRowBinding.orderStatus.setText("Pending");
            holder.itemRowBinding.orderStatus.setTextColor(context.getResources().getColor(R.color.red));
        }
        else {
            holder.itemRowBinding.orderStatus.setText("Completed");
            holder.itemRowBinding.orderStatus.setTextColor(context.getResources().getColor(R.color.green));
        }
    }

    public void setData(List<ReserveOrderData> dataModelList) {
        this.dataModelList = dataModelList;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return dataModelList == null ? 0 : dataModelList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemPreviousPurchaseBinding itemRowBinding;

        public ViewHolder(ItemPreviousPurchaseBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }

}