package com.app.baakki.other;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.app.baakki.R;
import com.app.baakki.bottomsheet.LogoutBottomSheet;
import com.app.baakki.databinding.FragmentOtherBinding;
import com.app.baakki.databinding.FragmentPreviousBinding;
import com.app.baakki.listener.PurchaseDataListnere;
import com.app.baakki.model.pendingcompletecampaign.Completed;
import com.app.baakki.model.pendingcompletecampaign.PendingCompleteCampaignRes;
import com.app.baakki.model.reserveorder.ReserveOrderData;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;

import java.util.ArrayList;
import java.util.List;


public class PreviousPurchaseFragment extends Fragment {
    FragmentPreviousBinding dataBiding;
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    PreviousPurchaseAdapter previousPurchaseAdapter;
    ArrayList<ReserveOrderData> arrayList;

    public static PreviousPurchaseFragment newInstance(Context context, ArrayList<ReserveOrderData> arrayList) {
        PreviousPurchaseFragment fragment = new PreviousPurchaseFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("previous", arrayList);
        fragment.setArguments(bundle);


        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.fragment_previous, null, false);

        init();
        return dataBiding.getRoot();
    }

    private void init() {
        context = getActivity();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        previousPurchaseAdapter = new PreviousPurchaseAdapter(context);
        dataBiding.setPreviousPurchaseAdapter(previousPurchaseAdapter);

        if (getArguments() != null) {
            arrayList=getArguments().getParcelableArrayList("previous");
            if (arrayList != null && arrayList.size()>0) {
                dataBiding.llNoItem.setVisibility(View.GONE);
                dataBiding.rvPrevious.setVisibility(View.VISIBLE);
                previousPurchaseAdapter.setData(arrayList);
            } else {
                dataBiding.llNoItem.setVisibility(View.VISIBLE);
                dataBiding.rvPrevious.setVisibility(View.GONE);
            }
        } else {
            dataBiding.llNoItem.setVisibility(View.VISIBLE);
            dataBiding.rvPrevious.setVisibility(View.GONE);
        }


    }


}