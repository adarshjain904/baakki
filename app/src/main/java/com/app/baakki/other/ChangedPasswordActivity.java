package com.app.baakki.other;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityChangePasswordBinding;
import com.app.baakki.model.login.LoginSignupRes;
import com.app.baakki.model.pendingcompletecampaign.PendingCompleteCampaignRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ChangedPasswordActivity extends BaseActivity {
    Context context;
    CompositeDisposable compositeDisposable;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityChangePasswordBinding activityBinding;
    String oldPass, newPass, confirmPass;


    @Override

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        setWhiteTitle(activityBinding.tool.ivBack, activityBinding.tool.tvNeedSomeHelp);
        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    private void init() {
        context = ChangedPasswordActivity.this;
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

    }

    public class MyClickHandlers {
        public void onChangedPasswordClick(View view) {
            if (isValid()) {
                updatePasswordService();
            }
        }
    }

    private boolean isValid() {
        boolean isValid = true;

        oldPass = activityBinding.etOldPassword.getText().toString();
        newPass = activityBinding.etNewPassword.getText().toString();
        confirmPass = activityBinding.etConNewPassword.getText().toString();

        if (TextUtils.isEmpty(oldPass)) {
            activityBinding.etOldPassword.setError(getString(R.string.enter_old_password));
            isValid = false;
        } else if (TextUtils.isEmpty(newPass)) {
            activityBinding.etNewPassword.setError(getString(R.string.enter_password));
            isValid = false;
        } else if (newPass.length() < 6) {
            activityBinding.etNewPassword.setError(getString(R.string.enter_4_digit));
            isValid = false;
        } else if (TextUtils.isEmpty(confirmPass)) {
            activityBinding.etConNewPassword.setError(getString(R.string.enter_con_password));
            isValid = false;
        } else if (!newPass.equals(confirmPass)) {
            activityBinding.etConNewPassword.setError(getString(R.string.pass_dont_match));
            isValid = false;
        }
        return isValid;
    }

    private void updatePasswordService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).
                updatePasswordApi(oldPass, newPass, confirmPass)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<LoginSignupRes>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull LoginSignupRes loginSignupRes) {
                        progressLoader.dismiss();
                        handleResponse(loginSignupRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void handleResponse(LoginSignupRes loginSignupRes) {

        if (loginSignupRes.getStatus()) {
            finish();
        }else
            Toast.makeText(context, loginSignupRes.getMessage(), Toast.LENGTH_SHORT).show();
    }
}

