package com.app.baakki.other;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityPolicyBinding;
import com.app.baakki.model.faq.FaqRes;
import com.app.baakki.model.termpolicy.TermPolicyRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class PrivacyPolicyActivity extends BaseActivity {
    Context context;
    ProgressLoader progressLoader;
    CompositeDisposable compositeDisposable;
    SharedPreference sharedPreference;
    ActivityPolicyBinding activityBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_policy);

        setToolbar(activityBinding.tool.ivBack, activityBinding.tool.tvTitle, activityBinding.tool.vwLine, getString(R.string.privacy_policy));

        init();
    }

    private void init() {
        context = PrivacyPolicyActivity.this;
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);
        activityBinding.tvPrivacyPolicy.setMovementMethod(new ScrollingMovementMethod());

        privacyPolicyAndTermConditionService();
    }

    private void privacyPolicyAndTermConditionService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).privacyPolicyAndTermConditionApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<TermPolicyRes>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull TermPolicyRes termPolicyRes) {
                        progressLoader.dismiss();
                        handleFavoritesResponse(termPolicyRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }


    private void handleFavoritesResponse(TermPolicyRes termPolicyRes) {
        if (termPolicyRes.getStatus()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                activityBinding.tvPrivacyPolicy.setText(Html.fromHtml(termPolicyRes.getPrivacyPolicy(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                activityBinding.tvPrivacyPolicy.setText(Html.fromHtml(termPolicyRes.getPrivacyPolicy()));
            }
        }
    }

}
