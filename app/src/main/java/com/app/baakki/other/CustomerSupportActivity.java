package com.app.baakki.other;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.bottomsheet.CancelBottomSheet;
import com.app.baakki.bottomsheet.FurtherBottomSheet;
import com.app.baakki.databinding.ActivityHelpCenterBinding;
import com.app.baakki.listener.FurtherClick;
import com.app.baakki.model.faq.FaqData;
import com.app.baakki.model.faq.FaqRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.app.baakki.util.PaginationListener.PAGE_START;

public class CustomerSupportActivity extends BaseActivity implements FurtherClick , TabLayout.OnTabSelectedListener {
    Context context;
    CompositeDisposable compositeDisposable;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityHelpCenterBinding activityBinding;
    List<FaqData> myOrder = new ArrayList<>();
    List<FaqData> baakkiWork = new ArrayList<>();
    List<FaqData> joinBaakki = new ArrayList<>();
    FaqAdapter faqAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_help_center);
        setToolbar(activityBinding.tool.ivBack, activityBinding.tool.tvTitle, activityBinding.tool.vwLine,
                activityBinding.tool.tvNeedSomeHelp, getString(R.string.help_center), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new FurtherBottomSheet().show(getSupportFragmentManager(), "FurtherBottomSheet");
                    }
                });
        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    private void init() {
        context = CustomerSupportActivity.this;
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);
        activityBinding.tbCategory.addOnTabSelectedListener(this);
        faqAdapter = new FaqAdapter(context,this);
        activityBinding.setFaqAdapter(faqAdapter);


        faqCategoryService();


    }

    @Override
    public void furtherClick(int pos) {
        new FurtherBottomSheet().show(getSupportFragmentManager(), "FurtherBottomSheet");
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        faqService(tab.getText().toString());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public class MyClickHandlers {
       /* public void onMyOrderClick(View view) {
            faqAdapter.setData(myOrder,false);
            clickText(activityBinding.tv1,activityBinding.tv2,activityBinding.tv3);
        }

        public void onBaakiWorkClick(View view) {
            faqAdapter.setData(baakkiWork,false);
            clickText(activityBinding.tv2,activityBinding.tv1,activityBinding.tv3);

        }

        public void onJoinBaakkiClick(View view) {
            faqAdapter.setData(joinBaakki,false);
            clickText(activityBinding.tv3,activityBinding.tv2,activityBinding.tv1);
        }*/
    }

    private void faqCategoryService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).faqCategoryApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<JsonElement>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull JsonElement jsonElement) {
                        progressLoader.dismiss();
                        handleResponse(jsonElement);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    private void handleResponse(JsonElement jsonElement) {
        JsonObject jsonObject = jsonElement.getAsJsonObject();

        if (jsonObject.get("status").getAsBoolean()) {
         JsonArray jsonArrayElements= jsonObject.get("data").getAsJsonArray();
            for (int i = 0; i < jsonArrayElements.size(); i++) {
                JsonObject term = jsonArrayElements.get(i).getAsJsonObject();
                String s=term.get("name").toString();

                activityBinding.tbCategory.addTab(activityBinding.tbCategory.newTab().setText(s.replace("\"", "")));
            }
            faqService(activityBinding.tbCategory.getTabAt(0).getText().toString());
        } else {
            progressLoader.dismiss();
            Toast.makeText(context, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void faqService(String tabName) {
        Log.e("tabn",tabName);
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).faqApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<FaqRes>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull FaqRes faqRes) {
                        progressLoader.dismiss();
                        handleFavoritesResponse(faqRes,tabName);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void handleFavoritesResponse(FaqRes faqRes,String tabName) {
        myOrder.clear();
        if (faqRes.getStatus()) {
            for (int i = 0; i < faqRes.getData().size(); i++) {
                if (faqRes.getData().get(i).getSection().equals(tabName)) {
                    myOrder.add(faqRes.getData().get(i));
                }
            }
            faqAdapter.setData(myOrder,false);
        }
    }
}
