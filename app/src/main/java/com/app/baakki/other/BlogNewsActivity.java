package com.app.baakki.other;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityBlogBinding;
import com.app.baakki.login.VerifyOtpActivity;
import com.app.baakki.model.blog.BlogData;
import com.app.baakki.model.blog.BlogList;
import com.app.baakki.model.blog.BlogRes;
import com.app.baakki.model.gelallfav.GetAllFavRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class BlogNewsActivity extends BaseActivity implements TabLayout.OnTabSelectedListener {
    Context context;
    CompositeDisposable compositeDisposable;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityBlogBinding activityBinding;
    TopArticleAdapter topArticleAdapter;
    RecentlyAdapter recentlyAdapter;
    BlogRes blogRes;
    List<BlogList> data;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_blog);
        setToolbar(activityBinding.tool.ivBack, activityBinding.tool.tvTitle, activityBinding.tool.vwLine, getString(R.string.blogs_news));

        init();
    }

    private void init() {
        data = new ArrayList<>();
        context = BlogNewsActivity.this;
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        activityBinding.tbBlog.addOnTabSelectedListener(this);

        topArticleAdapter = new TopArticleAdapter(context);
        recentlyAdapter = new RecentlyAdapter(context);

        activityBinding.setRecentlyAdapter(recentlyAdapter);
        activityBinding.setTopArticleAdapter(topArticleAdapter);


        blogService();
    }

    private void blogService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).blogApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<BlogRes>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull BlogRes blogRes) {
                        progressLoader.dismiss();
                        handleFavoritesResponse(blogRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void handleFavoritesResponse(BlogRes blogRes) {
        this.blogRes = blogRes;
        if (blogRes.getStatus()) {
            activityBinding.tbBlog.addTab(activityBinding.tbBlog.newTab().setText("All"));
            for (int i = 0; i < blogRes.getData().size(); i++) {
                activityBinding.tbBlog.addTab(activityBinding.tbBlog.newTab().setText(blogRes.getData().get(i).getName()));
                data.addAll(blogRes.getData().get(i).getBloglist());
            }
            topArticleAdapter.setData(data);
        }
    }

    /*@RequiresApi(api = Build.VERSION_CODES.N)
    private void handleFavoritesResponse(BlogRes blogRes) {
        this.blogRes = blogRes;
        if (blogRes.getStatus()) {
            activityBinding.tbBlog.addTab(activityBinding.tbBlog.newTab().setText("All"));
            for (int i = 0; i < blogRes.getData().size(); i++) {
                activityBinding.tbBlog.addTab(activityBinding.tbBlog.newTab().setText(blogRes.getData().get(i).getName()));
                if (i!=0){
                    data.addAll(blogRes.getData().get(i).getBloglist());
                }
            }

            topArticleAdapter.setData(blogRes.getData().get(0).getBloglist());
            recentlyAdapter.setData(data);


        }
    }*/

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (tab.getPosition()==0){
            activityBinding.rvTopArticle.setVisibility(View.VISIBLE);
            activityBinding.tvRecent.setVisibility(View.GONE);
            activityBinding.tvTop.setVisibility(View.GONE);
            topArticleAdapter.setData(blogRes.getData().get(0).getBloglist());
            recentlyAdapter.setData(data);
        }else {
            activityBinding.rvTopArticle.setVisibility(View.GONE);
            activityBinding.tvRecent.setVisibility(View.GONE);
            activityBinding.tvTop.setVisibility(View.GONE);
            recentlyAdapter.setData(blogRes.getData().get(tab.getPosition()-1).getBloglist());
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
