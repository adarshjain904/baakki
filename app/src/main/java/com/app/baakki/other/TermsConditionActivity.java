package com.app.baakki.other;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityTermsConditionBinding;
import com.app.baakki.model.termpolicy.TermPolicyRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class TermsConditionActivity extends BaseActivity {
    Context context;
    ProgressLoader progressLoader;
    CompositeDisposable compositeDisposable;
    SharedPreference sharedPreference;
    ActivityTermsConditionBinding activityBinding;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_terms_condition);
        setToolbar(activityBinding.tool.ivBack,activityBinding.tool.tvTitle,activityBinding.tool.vwLine,getString(R.string.tems_and_condition));
        init();
    }

    private void init() {
        context = TermsConditionActivity.this;
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);
        activityBinding.tvTermsCondition.setMovementMethod(new ScrollingMovementMethod());

        privacyPolicyAndTermConditionService();
    }

    private void privacyPolicyAndTermConditionService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).privacyPolicyAndTermConditionApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<TermPolicyRes>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull TermPolicyRes termPolicyRes) {
                        progressLoader.dismiss();
                        handleFavoritesResponse(termPolicyRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }


    private void handleFavoritesResponse(TermPolicyRes termPolicyRes) {
        if (termPolicyRes.getStatus()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                activityBinding.tvTermsCondition.setText(Html.fromHtml(termPolicyRes.getTermAndCondition(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                activityBinding.tvTermsCondition.setText(Html.fromHtml(termPolicyRes.getTermAndCondition()));
            }
        }
    }

}
