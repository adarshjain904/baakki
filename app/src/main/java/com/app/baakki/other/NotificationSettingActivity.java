package com.app.baakki.other;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;

import com.app.baakki.databinding.ActivityNotificationSettingBinding;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;

public class NotificationSettingActivity extends BaseActivity {
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityNotificationSettingBinding activityBinding;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_notification_setting);
        setToolbar(activityBinding.tool.ivBack,activityBinding.tool.tvTitle,activityBinding.tool.vwLine,getString(R.string.notification_setting));

        init();
    }

    private void init() {
        context = NotificationSettingActivity.this;
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

    }


}

