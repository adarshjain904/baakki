package com.app.baakki.other;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivitySettingBinding;

import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;

public class SettingActivity extends BaseActivity {
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivitySettingBinding activityBinding;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_setting);
        setToolbar(activityBinding.tool.ivBack, activityBinding.tool.tvTitle, activityBinding.tool.vwLine, getString(R.string.setting));
        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    private void init() {
        context = SettingActivity.this;
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

    }


    public class MyClickHandlers {
        public void onChangePasswordClick(View view) {
            Intent intent = new Intent(context, ChangedPasswordActivity.class);
            startActivity(intent);

        }

        public void onPrivacyPolicyClick(View view) {
                Intent intent = new Intent(context, PrivacyPolicyActivity.class);
                startActivity(intent);

        }

        public void onNotificationSettingClick(View view) {
            Intent intent = new Intent(context,NotificationSettingActivity.class);
            startActivity(intent);

        }
    }
}
