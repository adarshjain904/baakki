package com.app.baakki.other;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.app.baakki.R;
import com.app.baakki.bottomsheet.LogoutBottomSheet;
import com.app.baakki.databinding.FragmentCurrentBinding;
import com.app.baakki.databinding.FragmentOtherBinding;
import com.app.baakki.discover.OrderBookingActivity;
import com.app.baakki.listener.PurchaseDataListnere;
import com.app.baakki.model.pendingcompletecampaign.Pending;
import com.app.baakki.model.pendingcompletecampaign.PendingCompleteCampaignRes;
import com.app.baakki.model.reserveorder.ReserveOrderData;
import com.app.baakki.util.Constant;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;

import java.util.ArrayList;
import java.util.List;

public class CurrentPurchaseFragment extends Fragment {
    FragmentCurrentBinding dataBiding;
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    CurrentPurchaseAdapter currentPurchaseAdapter;
    ArrayList<ReserveOrderData> arrayList;

    public static CurrentPurchaseFragment newInstance(Context context, ArrayList<ReserveOrderData> arrayList) {
        CurrentPurchaseFragment fragment = new CurrentPurchaseFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("current", arrayList);
        fragment.setArguments(bundle);


        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.fragment_current, null, false);

        init();
        return dataBiding.getRoot();
    }

    private void init() {
        context = getActivity();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        currentPurchaseAdapter = new CurrentPurchaseAdapter(context,new MyClickListener());
        dataBiding.setCurrentPurchaseAdapter(currentPurchaseAdapter);


        if (getArguments() != null) {
            arrayList=getArguments().getParcelableArrayList("current");
            if (arrayList != null&& arrayList.size()>0) {
                dataBiding.llNoItem.setVisibility(View.GONE);
                dataBiding.rvCurrent.setVisibility(View.VISIBLE);
                currentPurchaseAdapter.setData(arrayList);
            } else {
                dataBiding.llNoItem.setVisibility(View.VISIBLE);
                dataBiding.rvCurrent.setVisibility(View.GONE);
            }
        } else {
            dataBiding.llNoItem.setVisibility(View.VISIBLE);
            dataBiding.rvCurrent.setVisibility(View.GONE);
        }

    }

    public void setData(PendingCompleteCampaignRes pendingCompleteCampaignRes) {
    }

    public class MyClickListener {
        public void onSelectOrderClick(ReserveOrderData data) {
            Intent intent = new Intent(context, OrderBookingActivity.class);
            intent.putExtra(Constant.RESERVE_DATA, data);
            startActivityForResult(intent, Constant.RESERVE_CODE);
        }
    }
}