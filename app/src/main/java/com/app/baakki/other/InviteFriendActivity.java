package com.app.baakki.other;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityInviteFriendBinding;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;

public class InviteFriendActivity  extends BaseActivity {
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityInviteFriendBinding activityBinding;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_invite_friend);
        setToolbar(activityBinding.tool.ivBack,activityBinding.tool.tvTitle,activityBinding.tool.vwLine,getString(R.string.invite_your_friend));
        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    private void init() {
        context = InviteFriendActivity.this;
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

    }

    public class MyClickHandlers {
        public void onInviteFriendClick(View view) {
           shareLink();
        }
    }

    private void shareLink() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
            String shareMessage= "Let me recommend you this application\n" + "https://play.google.com/store/apps/details?id=" +getPackageName();
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch(Exception e) {
            //e.toString();
        }
    }
}
