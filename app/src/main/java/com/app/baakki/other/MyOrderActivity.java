package com.app.baakki.other;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityMyOrderBinding;
import com.app.baakki.login.VerifyOtpActivity;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;

public class MyOrderActivity extends BaseActivity {
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityMyOrderBinding activityBinding;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_order);
        setToolbar(activityBinding.tool.ivBack,activityBinding.tool.tvTitle,activityBinding.tool.vwLine,getString(R.string.my_order));
        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    private void init() {
        context = MyOrderActivity.this;
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

    }

    public class MyClickHandlers {
        public void onSignUpClick(View view) {

        }
    }
}
