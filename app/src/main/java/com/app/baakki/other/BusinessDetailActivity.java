package com.app.baakki.other;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.bottomsheet.CountryBottomSheet;
import com.app.baakki.databinding.ActivityBusinessDetailBinding;

import com.app.baakki.listener.OnCountryClickListener;
import com.app.baakki.model.country.CountryData;
import com.app.baakki.model.country.CountryRes;
import com.app.baakki.model.joinbaakki.JoinBaakkiRes;
import com.app.baakki.model.rating.RatingRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.DialogUtility;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class BusinessDetailActivity extends BaseActivity {
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    CompositeDisposable compositeDisposable;
    ActivityBusinessDetailBinding activityBinding;
    String name, contact, email, countryId;
    List<CountryData> countryData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_business_detail);
        setToolbar(activityBinding.tool.ivBack, activityBinding.tool.tvTitle, activityBinding.tool.vwLine, "Join Baakki");
        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    private void init() {
        context = BusinessDetailActivity.this;
        progressLoader = new ProgressLoader(context);
        compositeDisposable = new CompositeDisposable();
        sharedPreference = SharedPreference.getInstance(context);

        countryService();
    }

    public class MyClickHandlers implements OnCountryClickListener {
        public void onCountryClick(View view) {
            new CountryBottomSheet(this, countryData).show(getSupportFragmentManager(), "BusinessCategoryBottomSheet");
        }

        @Override
        public void onCategoryItemSelected(CountryData item) {
            countryId = String.valueOf(item.getId());
            activityBinding.tvCountry.setText(item.getName());
        }

        public void onSubmitClick(View view) {
            if (isValid()) {
                joinBaakkiService();
            }
        }
    }

    private boolean isValid() {
        boolean isValid = true;

        name = activityBinding.etBusName.getText().toString();
        email = activityBinding.etEmail.getText().toString();
        contact = activityBinding.etMob.getText().toString();

        if (TextUtils.isEmpty(name)) {
            Toast.makeText(context, "Enter Name of Busines", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (TextUtils.isEmpty(countryId)) {
            Toast.makeText(context, "Please select Country", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (TextUtils.isEmpty(activityBinding.etEmail.getText().toString())) {
            Toast.makeText(context, getString(R.string.enter_email), Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (!ValidationUtils.isValidEmail(email)) {
            Toast.makeText(context, getString(R.string.valid_email), Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (TextUtils.isEmpty(contact)) {
            Toast.makeText(context, "Enter Contact Number", Toast.LENGTH_SHORT).show();
            isValid = false;
        }
        return isValid;
    }

    private void countryService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).countrylistApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CountryRes>() {
                    @Override
                    public void onSuccess(@NonNull CountryRes countryRes) {
                        progressLoader.dismiss();
                        handleResponse(countryRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    private void handleResponse(CountryRes countryRes) {
        if (countryRes.getStatus()) {
            countryData = countryRes.getData();
        } else {
            Toast.makeText(context, countryRes.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void joinBaakkiService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).
                joinBaakkiApi(name, countryId, email, contact)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<JoinBaakkiRes>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull JoinBaakkiRes joinBaakkiRes) {
                        progressLoader.dismiss();
                        handleResponse(joinBaakkiRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    private void handleResponse(JoinBaakkiRes joinBaakkiRes) {

        if (joinBaakkiRes.getStatus()) {
            DialogUtility.showOkDialog(context, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Dialog dialog = (Dialog) view.getTag();
                    dialog.dismiss();
                    finish();
                }
            });
        }else
            Toast.makeText(context, joinBaakkiRes.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
