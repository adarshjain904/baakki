package com.app.baakki.other;

import android.content.Context;

import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.databinding.ItemTopArticleBinding;
import com.app.baakki.discover.ProductAdapter;
import com.app.baakki.model.blog.BlogList;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;


public class TopArticleAdapter extends RecyclerView.Adapter<TopArticleAdapter.ViewHolder> {
    private Context context;
    List<BlogList> dataModelList;

    public TopArticleAdapter(Context ctx) {
        context = ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemTopArticleBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_top_article, parent, false);
        binding.setHandlers(new MyClickHandlers());
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemRowBinding.setData(dataModelList.get(position));
        holder.itemRowBinding.setPos(position);
    }

    public void setData(List<BlogList> dataModelList) {
        this.dataModelList = dataModelList;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return dataModelList == null ? 0 : dataModelList.size();

    }

    public class MyClickHandlers {
        public void onItemClick(int pos) {
            Intent intent = new Intent(context, BlogDetailActivity.class);
            intent.putExtra("blogData", dataModelList.get(pos));
            context.startActivity(intent);
        }


    }

    @BindingAdapter("app:srcBlogImage")
    public static void srcBlogImage(ImageView view, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            RequestOptions options = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.iv_shop_placeholder);
            Glide.with(view.getContext())
                    .load(imageUrl)
                    .apply(options)
                    .into(view);
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemTopArticleBinding itemRowBinding;

        public ViewHolder(ItemTopArticleBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }

}