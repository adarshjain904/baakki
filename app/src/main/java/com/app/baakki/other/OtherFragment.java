package com.app.baakki.other;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.baakki.R;
import com.app.baakki.bottomsheet.LogoutBottomSheet;
import com.app.baakki.databinding.FragmentOtherBinding;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.google.android.material.bottomsheet.BottomSheetDialog;


public class OtherFragment extends Fragment {
    FragmentOtherBinding dataBiding;
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.fragment_other, null, false);
        dataBiding.setHandlers(new MyClickHandlers());

        init();
        return dataBiding.getRoot();
    }

    private void init() {
        context = getActivity();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);
    }


    public class MyClickHandlers {
        public void onBackClick(View view) {
            Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigateUp();
        }

        public void onMyProfileClick(View view) {
            Intent intent = new Intent(context, MyProfileActivity.class);
            startActivity(intent);
        }

        public void onMyPurchaseClick(View view) {
            Intent intent = new Intent(context, MyPurchaseActivity.class);
            startActivity(intent);
        }

        public void onInviteFriendClick(View view) {
            Intent intent = new Intent(context, InviteFriendActivity.class);
            startActivity(intent);
        }

        public void onSettingClick(View view) {
            Intent intent = new Intent(context, SettingActivity.class);
            startActivity(intent);
        }

        public void onBlogNewsClick(View view) {
            Intent intent = new Intent(context, BlogNewsActivity.class);
            startActivity(intent);
        }

        public void onTermsConditionClick(View view) {
            Intent intent = new Intent(context, TermsConditionActivity.class);
            startActivity(intent);

        }

        public void onCustomerSupportClick(View view) {
            Intent intent = new Intent(context, CustomerSupportActivity.class);
            startActivity(intent);
        }

        public void onLogoutClick(View view) {
            new LogoutBottomSheet().show(getActivity().getSupportFragmentManager(), "LogoutBottomSheet");
        }

        public void onJoinBaakkiClick(View view) {
            Intent intent = new Intent(context, BusinessDetailActivity.class);
            startActivity(intent);
        }

        public void onLearnMoreClick(View view) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.baakki.com/"));
            startActivity(browserIntent);
        }
    }
}