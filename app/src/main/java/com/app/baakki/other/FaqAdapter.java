package com.app.baakki.other;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.databinding.ItemHeaderBinding;
import com.app.baakki.listener.FurtherClick;
import com.app.baakki.model.faq.FaqData;

import java.util.List;

public class FaqAdapter extends RecyclerView.Adapter<FaqAdapter.ViewHolder> {
    private Context _context;
    private List<FaqData> header; // header titles
    FurtherClick furtherClick;
   // boolean isRedeenMore = false;


    ItemHeaderBinding binding;

    public FaqAdapter(Context context, FurtherClick furtherClick) {
        this._context = context;
        this.furtherClick = furtherClick;
    }

    public void setData(List<FaqData> listDataHeader, boolean isRedeenMore) {
        for (int i = 0; i <listDataHeader.size() ; i++) {
            listDataHeader.get(i).setIsvisible(false);
        }
        this.header = listDataHeader;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_header, parent, false);
        binding.setHandlers(new MyClickHandlers());
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemRowBinding.setFaqData(header.get(position));
        holder.itemRowBinding.setPos(position);

        holder.itemRowBinding.tvAns.setVisibility(View.GONE);
      //  holder.itemRowBinding.tvFutherHelp.setVisibility(View.GONE);
        holder.itemRowBinding.ivAbout.startAnimation(animate(header.get(position).isIsvisible()));

        holder.itemRowBinding.rvQus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!header.get(position).isIsvisible()) {
                    header.get(position).setIsvisible(true);
                    holder.itemRowBinding.tvAns.setVisibility(View.VISIBLE);
                 //   holder.itemRowBinding.tvFutherHelp.setVisibility(View.VISIBLE);
                    holder.itemRowBinding.ivAbout.startAnimation(animate(header.get(position).isIsvisible()));//
                } else {
                    header.get(position).setIsvisible(false);
                    holder.itemRowBinding.tvAns.setVisibility(View.GONE);
                 //   holder.itemRowBinding.tvFutherHelp.setVisibility(View.GONE);
                    holder.itemRowBinding.ivAbout.startAnimation(animate(header.get(position).isIsvisible()));
                }
            }
        });


    }

    public class MyClickHandlers {
        public void onItemClick(int pos) {
            furtherClick.furtherClick(pos);
          /*  Intent intent = new Intent(_context, PrivacyPolicyActivity.class);
            intent.putExtra("further_help",true);
            _context. startActivity(intent);*/

        }
    }

    private Animation animate(boolean up) {
        Animation anim = AnimationUtils.loadAnimation(_context, up ? R.anim.rotate_up : R.anim.rotate_down);
        anim.setInterpolator(new LinearInterpolator()); // for smooth animation
        return anim;
    }


    @Override
    public int getItemCount() {
        return header == null ? 0 : header.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemHeaderBinding itemRowBinding;

        public ViewHolder(ItemHeaderBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }

}
