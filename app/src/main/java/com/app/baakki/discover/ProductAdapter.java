package com.app.baakki.discover;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Point;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.databinding.ItemLoadingBinding;
import com.app.baakki.databinding.ItemProductBinding;
import com.app.baakki.listener.OnFavProductClick;
import com.app.baakki.model.usergetallcampaign.CommonData;
import com.app.baakki.util.Constant;
import com.app.baakki.util.DialogUtility;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;


public class ProductAdapter extends RecyclerView.Adapter {
    private static final int VIEW_TYPE_LOADING = 0;
    private static final int VIEW_TYPE_NORMAL = 1;
    private boolean isLoaderVisible = false;
    private Context context;
    boolean isAll;
    OnFavProductClick onFavProductClick;
    List<CommonData> dataModelList;

    public ProductAdapter(Context ctx, boolean isAll, OnFavProductClick onFavProductClick) {
        context = ctx;
        this.isAll = isAll;
        this.onFavProductClick = onFavProductClick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ItemProductBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_product, parent, false);
                if (!isAll) {
                    binding.getRoot().getLayoutParams().width = (int) (getScreenWidth() / 1.30);
                }
                binding.setHandlers(new MyClickHandlers());
                return new ViewHolder(binding);
            case VIEW_TYPE_LOADING:
                ItemLoadingBinding loadingBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_loading, parent, false);
                return new ProgressHolder(loadingBinding);
            default:
                return null;

        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ((ViewHolder) holder).itemRowBinding.tvOriginalPrice.setPaintFlags(((ViewHolder) holder).itemRowBinding.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            ((ViewHolder) holder).itemRowBinding.executePendingBindings();
            ((ViewHolder) holder).itemRowBinding.setDiscoverData(dataModelList.get(position));
            ((ViewHolder) holder).itemRowBinding.setPos(position);
            ((ViewHolder) holder).itemRowBinding.tvDiscoverDate.setText("Start from " + DialogUtility.serverDateTime(dataModelList.get(position).getDate()));
            ((ViewHolder) holder).itemRowBinding.tvDiscoverTime.setText(serverDate(dataModelList.get(position)
                    .getFromTime()) + " to " + serverDate(dataModelList.get(position).getToTime()) + "  \u2022 " + dataModelList.get(position).getDistance() + " km");

            /*if (DialogUtility.checkExpairDate(dataModelList.get(position).getCamExpiry(), dataModelList.get(position).getToTime()))
                ((ViewHolder) holder).itemRowBinding.tvRemaining.setText("Expired");
            else*/
            if (dataModelList.get(position).getRemainingQuantity() == null) {
                ((ViewHolder) holder).itemRowBinding.tvRemaining.setText("Sold out ");
            } else if (dataModelList.get(position).getRemainingQuantity() == 0) {
                ((ViewHolder) holder).itemRowBinding.tvRemaining.setText("Sold out ");
            } else
                ((ViewHolder) holder).itemRowBinding.tvRemaining.setText(dataModelList.get(position).getRemainingQuantity() + " left");

        }
    }


    public void setData(List<CommonData> dataModelList) {
        this.dataModelList = dataModelList;
        notifyDataSetChanged();
    }


    public class MyClickHandlers {
        public void onProductClick(int pos) {
            Intent intent = new Intent(context, ProductDetailActivity.class);
            intent.putExtra(Constant.PRODUCT_DETAIL_DATA, dataModelList.get(pos));
            context.startActivity(intent);
        }

        public void onFavClick(CommonData commonData) {
            onFavProductClick.onFavProductClick(commonData);
        }
    }

    @BindingAdapter("app:srcProductImage")
    public static void srcProductImage(ImageView view, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            RequestOptions options = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.iv_shop_placeholder);
            Glide.with(view.getContext())
                    .load(imageUrl)
                    .apply(options)
                    .into(view);
        }
    }

    @BindingAdapter("app:srcCategoryImage")
    public static void srcCategoryImage(ImageView view, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            RequestOptions options = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.ic_mcd_logo);
            Glide.with(view.getContext())
                    .load(imageUrl)
                    .apply(options)
                    .into(view);
        }
    }


    public String serverDate(String date) {
        if (!TextUtils.isEmpty(date)) {
            SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
            SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
            try {
                return parseFormat.format(displayFormat.parse(date));
            } catch (ParseException e) {
                e.printStackTrace();
                return date;
            }
        } else {
            return date;
        }
    }

    @Override
    public int getItemCount() {
        if (isAll)
            return dataModelList == null ? 0 : dataModelList.size();
        else {
            return dataModelList == null ? 0 : dataModelList.size() > 4 ? 4 : dataModelList.size();
        }


    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemProductBinding itemRowBinding;

        public ViewHolder(ItemProductBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }

    public class ProgressHolder extends RecyclerView.ViewHolder {
        public ItemLoadingBinding itemLoadingBinding;

        ProgressHolder(ItemLoadingBinding itemLoadingBinding) {
            super(itemLoadingBinding.getRoot());
            this.itemLoadingBinding = itemLoadingBinding;
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (isLoaderVisible) {
            return position == dataModelList.size() ? VIEW_TYPE_LOADING : VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_NORMAL;
        }
    }


    public void addLoading() {
        isLoaderVisible = true;
        notifyDataSetChanged();

    }

    public void removeLoading() {
        isLoaderVisible = false;
        notifyDataSetChanged();

    }

    public void clear() {
        if (dataModelList != null) {
            dataModelList.clear();
            notifyDataSetChanged();
        }
    }

    public int getScreenWidth() {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        return size.x;
    }


}
