package com.app.baakki.discover;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.bottomsheet.CalenderBottomSheet;
import com.app.baakki.bottomsheet.CancelBottomSheet;
import com.app.baakki.databinding.ActivityReserveOrderBinding;

import com.app.baakki.databinding.ActivityYourOrderNewBinding;
import com.app.baakki.home.HomeActivity;
import com.app.baakki.home.ThankYouActivity;
import com.app.baakki.listener.CancelListener;
import com.app.baakki.model.completecampaign.CompleteCampaignRes;
import com.app.baakki.model.reserveorder.ReserveOrderData;
import com.app.baakki.model.reserveorder.ReserveOrderRes;
import com.app.baakki.model.usergetallcampaign.CommonData;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.DialogUtility;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.app.baakki.util.Constant.serverDate;

public class OrderBookingActivity extends BaseActivity {
    Context context;
    CompositeDisposable compositeDisposable;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityYourOrderNewBinding activityBinding;
    ReserveOrderData reserveOrderData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_your_order_new);
        activityBinding.setMyClickHandler(new MyClickListener());
        setToolbar(activityBinding.tool.ivBack, activityBinding.tool.tvTitle, activityBinding.tool.vwLine, "Reserved Order");
        init();
    }

    private void init() {
        context = this;
        progressLoader = new ProgressLoader(context);
        compositeDisposable = new CompositeDisposable();
        sharedPreference = SharedPreference.getInstance(context);
        reserveOrderData = getIntent().getParcelableExtra(Constant.RESERVE_DATA);
        activityBinding.setData(reserveOrderData);

        activityBinding.tvPrice.setText("£" + Integer.parseInt(reserveOrderData.getQuantity()) * reserveOrderData.getOfferPrice());
        activityBinding.tvCollectDate.setText("Redeem : " + DialogUtility.serverDateTime(reserveOrderData.getDate()));
        activityBinding.tvCollectTime.setText(Constant.serverDate(reserveOrderData.getFromTime()) + " to " + Constant.serverDate(reserveOrderData.getToTime()));

        if (reserveOrderData.getCancelationTime() != null)
            activityBinding.tvCancel.setText("You can cancel your order up to " + reserveOrderData.getCancelationTime() + " hours before the start of time window");
        else
            activityBinding.tvCancel.setText("You can cancel your order up to 0 hours before the start of time window");


        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        String dateString = df.format(Calendar.getInstance().getTime());

        Log.e("currentDate", dateString);
        Log.e("timr", reserveOrderData.getFromTime());

        if (dateString.compareTo(reserveOrderData.getFromTime()) < 0) {
            Log.e("app", "Date1 is before Date2");
            activityBinding.btPay.setVisibility(View.GONE);
        }  else if (dateString.compareTo(reserveOrderData.getToTime()) > 0) {
            Log.e("app", "Date1 is after Date2");
            activityBinding.btPay.setVisibility(View.GONE);
        }   else  {
            activityBinding.btPay.setVisibility(View.VISIBLE);
        }

    }

    public class MyClickListener implements CancelListener {
        public void onTapUseClick(View view) {
            completedCampaignService();
        }

        public void onShopDetailClick(View view) {
            CommonData commonData = new CommonData();
            commonData.setBusinessName(reserveOrderData.getName());
            commonData.setMobile(reserveOrderData.getMobile());
            commonData.setLat(reserveOrderData.getLat());
            commonData.setLon(reserveOrderData.getLon());
            commonData.setBusinessAddress(reserveOrderData.getAddress());
            Intent intent = new Intent(context, AboutProductActivity.class);
            intent.putExtra(Constant.PRODUCT_DETAIL_DATA, commonData);
            startActivity(intent);
        }

        public void onCancelReservationClick(View view) {
            new CancelBottomSheet(reserveOrderData.getOrderId(), reserveOrderData.getQuantity(), this).show(getSupportFragmentManager(), "CancelBottomSheet");
        }


        @Override
        public void onCancelCLick() {
            Intent intent = new Intent(context, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    private void completedCampaignService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).completedCampaignApi(reserveOrderData.getOrderId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CompleteCampaignRes>() {
                    @Override
                    public void onSuccess(@NonNull CompleteCampaignRes completeCampaignRes) {
                        progressLoader.dismiss();
                        handleCategoryResponse(completeCampaignRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                        Log.e("onError: ", e.getMessage());
                    }
                }));
    }

    private void handleCategoryResponse(CompleteCampaignRes completeCampaignRes) {
        Log.e("CompleteCampaign status", completeCampaignRes.getMessage());

        if (completeCampaignRes.getStatus()) {
            Intent intent = new Intent(context, RatingActivity.class);
            intent.putExtra(Constant.PRODUCT_DETAIL_DATA, reserveOrderData);
            startActivity(intent);
        }else
            Toast.makeText(context, completeCampaignRes.getMessage(), Toast.LENGTH_SHORT).show();
    }


}
