package com.app.baakki.discover;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityReserveOrderBinding;
import com.app.baakki.model.reserveorder.ReserveOrderData;
import com.app.baakki.util.Constant;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;

public class ReserveOrderActivity extends BaseActivity {
    Context context;
    CompositeDisposable compositeDisposable;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ArrayList<ReserveOrderData> reserveOrderData;
    ActivityReserveOrderBinding activityBinding;
    ReserveOrderAdapter reserveOrderAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_reserve_order);
        setToolbar(activityBinding.tool.ivBack, activityBinding.tool.tvTitle, activityBinding.tool.vwLine, "Reserved Order");
        init();
    }

    private void init() {
        context = this;
        progressLoader = new ProgressLoader(context);
        compositeDisposable = new CompositeDisposable();
        sharedPreference = SharedPreference.getInstance(context);
        reserveOrderData = getIntent().getParcelableArrayListExtra(Constant.RESERVE_DATA_ARRAY);

        reserveOrderAdapter = new ReserveOrderAdapter(context,new MyClickListener());
        activityBinding.setReserveOrderAdapter(reserveOrderAdapter);
        reserveOrderAdapter.setData(reserveOrderData);
    }

    public class MyClickListener {
        public void onSelectOrderClick(ReserveOrderData data) {
            Intent intent = new Intent(context, OrderBookingActivity.class);
            intent.putExtra(Constant.RESERVE_DATA, data);
            startActivityForResult(intent, Constant.RESERVE_CODE);
        }
    }
}
