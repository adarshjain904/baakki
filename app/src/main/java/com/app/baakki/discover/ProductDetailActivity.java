package com.app.baakki.discover;

import android.animation.LayoutTransition;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityProductDetailBinding;
import com.app.baakki.databinding.ActivityThankYouBinding;
import com.app.baakki.home.SelectDistanceActivity;
import com.app.baakki.home.ThankYouActivity;
import com.app.baakki.model.usergetallcampaign.CommonData;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.DialogUtility;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.app.baakki.util.Constant.serverDate;

public class ProductDetailActivity extends BaseActivity {
    Context context;
    CompositeDisposable compositeDisposable;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityProductDetailBinding activityBinding;
    boolean isShowMore = false;
    boolean isRedeenMore = false;
    CommonData commonData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_product_detail);
        commonData = getIntent().getParcelableExtra(Constant.PRODUCT_DETAIL_DATA);
        setToolbar(activityBinding.tool.ivBack, activityBinding.tool.tvTitle, activityBinding.tool.vwLine, commonData.getCampaignHeading());

        init();
    }

    @SuppressLint("SetTextI18n")
    private void init() {
        context = ProductDetailActivity.this;
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        activityBinding.setProductDetailData(commonData);
        activityBinding.setHandlers(new MyClickHandlers());
        activityBinding.tvOriginalPrice.setPaintFlags(activityBinding.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


        if (getIntent().hasExtra("isFav")) {
            activityBinding.tvTime.setText(serverDate(commonData.getFromTime()) + " to " + serverDate(commonData.getToTime()));

        } else {
            activityBinding.tvTime.setText(serverDate(commonData.getFromTime()) + " to " + serverDate(commonData.getToTime()) + "  \u2022 " + commonData.getDistance() + "Km");
        }

        if (commonData.getAvgRating() != null) {
            activityBinding.tvRating.setText(String.valueOf(DialogUtility.formatBmi(Double.parseDouble(commonData.getAvgRating()))));
            activityBinding.ratingBar.setRating(Float.parseFloat(String.valueOf(commonData.getAvgRating())));
            activityBinding.tvPeopleRating.setText(commonData.getRatingMembers() + " Ratings");
        } else {
            activityBinding.tvRating.setText("0.0");
            activityBinding.ratingBar.setRating(0);
            activityBinding.tvPeopleRating.setText("0 Ratings");
        }

        /*if (DialogUtility.checkExpairDate(commonData.getCamExpiry(), commonData.getToTime())) {
            activityBinding.tvRemaining.setText("Expired");
            activityBinding.tvReseve.setVisibility(View.GONE);
        } else*/
        if (commonData.getRemainingQuantity() == 0 || commonData.getRemainingQuantity() == null) {
            activityBinding.tvRemaining.setText("Sold out");
            activityBinding.tvReseve.setVisibility(View.GONE);
        } else {
            activityBinding.tvRemaining.setText(commonData.getRemainingQuantity() + " left");
            activityBinding.tvReseve.setVisibility(View.VISIBLE);
        }
        applyLayoutTransition(activityBinding.rvDetail);
        applyLayoutTransition(activityBinding.rvRedeem);
    }

    public class MyClickHandlers {
        public void onAboutProductClick(View view) {
            Intent intent = new Intent(context, AboutProductActivity.class);
            intent.putExtra(Constant.PRODUCT_DETAIL_DATA, commonData);
            startActivity(intent);
        }

        public void onReserveClick(View view) {
            Intent intent = new Intent(context, ReviewActivity.class);
            intent.putExtra("isFav", getIntent().hasExtra("isFav"));
            intent.putExtra(Constant.PRODUCT_DETAIL_DATA, commonData);
            startActivity(intent);
        }

        public void onDetailClick(View view) {
            if (!isShowMore) {
                isShowMore = true;
                activityBinding.tvDetail.setMaxLines(Integer.MAX_VALUE);//you
                activityBinding.ivArrowDetail.startAnimation(animate(isShowMore));// r TextView
            } else {
                isShowMore = false;
                activityBinding.tvDetail.setMaxLines(3);//your TextView
                activityBinding.ivArrowDetail.startAnimation(animate(isShowMore));
            }
        }

        public void onRedeemClick(View view) {
            if (!isRedeenMore) {
                isRedeenMore = true;
                activityBinding.tvRedeem.setMaxLines(Integer.MAX_VALUE);//you
                activityBinding.ivArrowRedeem.startAnimation(animate(isRedeenMore));// r TextView
            } else {
                isRedeenMore = false;
                activityBinding.tvRedeem.setMaxLines(3);//your TextView
                activityBinding.ivArrowRedeem.startAnimation(animate(isRedeenMore));
            }
        }

        public void onFavClick(CommonData commonData) {
            if (getIntent().hasExtra("isFav")) {
                addRemoveFavoritesService(Integer.valueOf(commonData.getCampaignId()));
            } else
                addRemoveFavoritesService(commonData.getId());
        }
    }

    private Animation animate(boolean up) {
        Animation anim = AnimationUtils.loadAnimation(this, up ? R.anim.rotate_up : R.anim.rotate_down);
        anim.setInterpolator(new LinearInterpolator()); // for smooth animation
        return anim;
    }

    private void applyLayoutTransition(RelativeLayout rvView) {
        LayoutTransition transition = new LayoutTransition();
        transition.setDuration(300);
        transition.enableTransitionType(LayoutTransition.CHANGING);
        rvView.setLayoutTransition(transition);
    }

    private void addRemoveFavoritesService(Integer campaignId) {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).addRemoveFavoritesApi(campaignId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<JsonElement>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull JsonElement myCampaignListRes) {

                        handleFavoritesResponse(myCampaignListRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void handleFavoritesResponse(JsonElement jsonElement) {
        progressLoader.dismiss();
        JsonObject jsonObject = jsonElement.getAsJsonObject();
       // Toast.makeText(context, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
        if (jsonObject.get("status").getAsBoolean()) {
            if (commonData.getIsLike() == 0) {
                commonData.setIsLike(1);
            } else {
                commonData.setIsLike(0);
            }
            activityBinding.setProductDetailData(commonData);
        }
    }
}
