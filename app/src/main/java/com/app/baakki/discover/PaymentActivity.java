package com.app.baakki.discover;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityPaymentBinding;
import com.app.baakki.databinding.ActivityReviewBinding;
import com.app.baakki.home.HomeActivity;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;

public class PaymentActivity extends BaseActivity {
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityPaymentBinding activityBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_payment);
        setToolbar(activityBinding.tool.ivBack, activityBinding.tool.tvTitle,activityBinding.tool.vwLine, "Payment");
        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    private void init() {
        context = PaymentActivity.this;
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);
    }

    public class MyClickHandlers {
        public void onPayClick(View view) {
            Intent intent = new Intent(context, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }
}

