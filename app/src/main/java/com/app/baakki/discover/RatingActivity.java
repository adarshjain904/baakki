package com.app.baakki.discover;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityRatingBinding;
import com.app.baakki.home.HomeActivity;
import com.app.baakki.model.createorder.CreateOrder;
import com.app.baakki.model.rating.RatingRes;
import com.app.baakki.model.reserveorder.ReserveOrderData;
import com.app.baakki.model.usergetallcampaign.CommonData;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class RatingActivity extends BaseActivity {
    Context context;
    ProgressLoader progressLoader;
    CompositeDisposable compositeDisposable;
    SharedPreference sharedPreference;
    ActivityRatingBinding activityBinding;
    ReserveOrderData commonData;
    String rating;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_rating);
        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    private void init() {
        context = RatingActivity.this;
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        commonData = getIntent().getParcelableExtra(Constant.PRODUCT_DETAIL_DATA);
        activityBinding.setProductDetailData(commonData);

        activityBinding.rbBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                rating = String.valueOf(ratingBar.getRating());
                activityBinding.tvNoRating.setText(String.valueOf(ratingBar.getRating()));
            }
        });
    }

    public class MyClickHandlers {
        public void onRatingClick(View view) {
            if (rating != null && !rating.equals("0.0"))
                orderCreateService();
            else
                Toast.makeText(context, "Please select rating", Toast.LENGTH_SHORT).show();
        }
    }

    private void orderCreateService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).
                submitRatingReviewApi(String.valueOf(commonData.getId()), activityBinding.etTitle.getText().toString(),
                        activityBinding.etComment.getText().toString(), activityBinding.rbBar.getRating())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<RatingRes>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull RatingRes ratingRes) {
                        progressLoader.dismiss();
                        handleResponse(ratingRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    private void handleResponse(RatingRes ratingRes) {
      //  Toast.makeText(context, ratingRes.getMessage(), Toast.LENGTH_SHORT).show();
        if (ratingRes.getStatus()) {
            Intent intent = new Intent(context, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}

