package com.app.baakki.discover;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.baakki.R;
import com.app.baakki.bottomsheet.DistanceBottomSheet;
import com.app.baakki.databinding.FragmentDiscoverBinding;
import com.app.baakki.home.SelectDistanceActivity;
import com.app.baakki.listener.LocationHelper;
import com.app.baakki.listener.OnCategoryClickListener;
import com.app.baakki.listener.OnFavProductClick;
import com.app.baakki.listener.SelectDistanceClick;
import com.app.baakki.model.ChooseLocation;
import com.app.baakki.model.category.CategoryData;
import com.app.baakki.model.category.CategoryRes;
import com.app.baakki.model.reserveorder.ReserveOrderData;
import com.app.baakki.model.reserveorder.ReserveOrderRes;
import com.app.baakki.model.usergetallcampaign.CommonData;
import com.app.baakki.model.usergetallcampaign.UserGelAllCampaignData;
import com.app.baakki.model.usergetallcampaign.UserGetAllCampaignRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.DialogUtility;
import com.app.baakki.util.LocationBuilder;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationResult;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;


public class DiscoverFragment extends Fragment implements OnFavProductClick, OnCategoryClickListener {
    FragmentDiscoverBinding dataBiding;
    Context context;
    ProgressLoader progressLoader;
    CompositeDisposable compositeDisposable;
    SharedPreference sharedPreference;
    DiscoverCategoryAdapter discoverCategoryAdapter;
    LinearLayout addView;
    String lat, lon, currentLat, currentLong, address, categoryId;
    Integer distance;
    List<Place.Field> fields;
    ChooseLocation chooseLocation;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        dataBiding = DataBindingUtil.inflate(inflater, R.layout.fragment_discover, null, false);
        dataBiding.setHandlers(new MyClickHandlers());
        init();
        return dataBiding.getRoot();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void init() {
        context = getActivity();
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        discoverCategoryAdapter = new DiscoverCategoryAdapter(context, this);
        dataBiding.setDiscoverCategoryAdapter(discoverCategoryAdapter);

        Places.initialize(getApplicationContext(), getString(R.string.google_map_key));
        fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);

        chooseLocation = new Gson().fromJson(sharedPreference.getString(Constant.LOCATION_DATA), ChooseLocation.class);
        dataBiding.tvDistanceCity.setText(chooseLocation.getDistance());
        dataBiding.tvSearchCity.setText(chooseLocation.getAddress());

        lat = String.valueOf(chooseLocation.getLatLng().latitude);
        lon = String.valueOf(chooseLocation.getLatLng().longitude);
        if (!TextUtils.isEmpty(chooseLocation.getDistance()))
            distance = Integer.valueOf(chooseLocation.getDistance().substring(0, chooseLocation.getDistance().length() - 3));
        else
            dataBiding.tvDistanceCity.setText(getResources().getString(R.string.select_radius));


        categoryService();
    }

   /* private void ShowLocationDialog() {
        DialogUtility.alertMessageNoGps(context, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, Constant.LOCATION_RESULT);
            }
        });
    }
*/
    @Override
    public void onCategoryItemSelected(CategoryData item) {
        chooseLocation.setCategoryId(String.valueOf(item.getId()));
        chooseLocation.setCategoryName(item.getName());
        sharedPreference.putString(Constant.LOCATION_DATA, new Gson().toJson(chooseLocation));
        myCampaignService();
    }

    public class MyClickHandlers implements SelectDistanceClick {
        public void onSearchClick(View view) {
            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                    .setCountry(sharedPreference.getString(Constant.COUNTRY_CODE))
                    .build(getActivity());
            startActivityForResult(intent, Constant.AUTOCOMPLETE_REQUEST_CODE);
        }

        public void onDistanceClick(View view) {
            new DistanceBottomSheet(this).show(getActivity().getSupportFragmentManager(), "DistanceBottomSheet");
        }

        @Override
        public void selectDistance(String selectedDistance) {
            distance = Integer.valueOf(selectedDistance.substring(0, selectedDistance.length() - 3));

            chooseLocation.setDistance(selectedDistance);
            sharedPreference.putString(Constant.LOCATION_DATA, new Gson().toJson(chooseLocation));
            dataBiding.tvDistanceCity.setText(selectedDistance);
            myCampaignService();
        }

    }

    private ArrayList<String> titleArray() {
        ArrayList<String> titleArray = new ArrayList<>();
        titleArray.add("Recommended for you");
        titleArray.add("Nearby");
        titleArray.add("Expires soon");
        // titleArray.add("Expires today");
        titleArray.add("Trending");
        titleArray.add("Favourites");
        return titleArray;
    }


    private void categoryService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).categoryApi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CategoryRes>() {
                    @Override
                    public void onSuccess(@NonNull CategoryRes categoryRes) {
                        //  progressLoader.dismiss();
                        handleCategoryResponse(categoryRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        Toast.makeText(context, R.string.server_error, Toast.LENGTH_SHORT).show();
                        Log.e("onError: ", e.getMessage());
                    }
                }));
    }

    private void handleCategoryResponse(CategoryRes categoryRes) {
        if (categoryRes.getStatus()) {


            discoverCategoryAdapter.setData(categoryRes.getData(), chooseLocation.getCategoryId());
        } else {
            Toast.makeText(context, categoryRes.getMessage(), Toast.LENGTH_SHORT).show();
        }

        myCampaignService();
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public void addView(String text, UserGelAllCampaignData data) {
        addView = (LinearLayout) this.getLayoutInflater().inflate(R.layout.layout_discover, null);

        TextView tvDiscoverTitle = addView.findViewById(R.id.tv_discover_title);
        TextView tvSeeAll = addView.findViewById(R.id.tv_see_all);
        RecyclerView rvProduct = addView.findViewById(R.id.rv_product);

        ArrayList<CommonData> recommendedArray = new ArrayList<>();
        ArrayList<CommonData> nearbyArray = new ArrayList<>();
        ArrayList<CommonData> expiresoonArray = new ArrayList<>();
        ArrayList<CommonData> trendingArray = new ArrayList<>();
        ArrayList<CommonData> favArray = new ArrayList<>();

        if (text.equals("Recommended for you")) {
            for (int i = 0; i < data.getRecommended().size(); i++) {
                if (data.getRecommended().get(i).getCampaignType().equals("onetime")) {
                    if (!data.getRecommended().get(i).getRemainingQuantity().equals(0)) {
                        recommendedArray.add(data.getRecommended().get(i));
                    }
                }else {
                    recommendedArray.add(data.getRecommended().get(i));
                }
            }
            setListData(recommendedArray, rvProduct, tvSeeAll, tvDiscoverTitle);
        } else if (text.equals("Nearby")) {
            for (int i = 0; i < data.getNearby().size(); i++) {
                if (data.getNearby().get(i).getCampaignType().equals("onetime")) {
                    if (!data.getNearby().get(i).getRemainingQuantity().equals(0)) {
                        nearbyArray.add(data.getNearby().get(i));
                    }
                }else {
                    nearbyArray.add(data.getNearby().get(i));
                }
            }
            setListData(nearbyArray, rvProduct, tvSeeAll, tvDiscoverTitle);
        } else if (text.equals("Expires soon")) {
            for (int i = 0; i < data.getExpirysoon().size(); i++) {
               /* if (data.getExpirysoon().get(i).getCampaignType().equals("onetime")) {*/
                    if (!data.getExpirysoon().get(i).getRemainingQuantity().equals(0)) {
                        expiresoonArray.add(data.getExpirysoon().get(i));
                   /* }else {
                        expiresoonArray.add(data.getExpirysoon().get(i));

                        }*/
            }
            }
            setListData(expiresoonArray, rvProduct, tvSeeAll, tvDiscoverTitle);
        } /*else if (text.equals("Expires today")) {
            setListData(data.getTodayexpiry(), rvProduct, tvSeeAll, tvDiscoverTitle);
        }*/ else if (text.equals("Trending")) {
            for (int i = 0; i < data.getTrending().size(); i++) {
                if (data.getTrending().get(i).getCampaignType().equals("onetime")) {
                    if (!data.getTrending().get(i).getRemainingQuantity().equals(0)) {
                        trendingArray.add(data.getTrending().get(i));
                    }
                }else {
                    trendingArray.add(data.getTrending().get(i));
                }
            }
            setListData(trendingArray, rvProduct, tvSeeAll, tvDiscoverTitle);
        } else if (text.equals("Favourites")) {
            for (int i = 0; i < data.getFavourate().size(); i++) {
                if (data.getFavourate().get(i).getCampaignType().equals("onetime")) {
                    if (!data.getFavourate().get(i).getRemainingQuantity().equals(0)) {
                        favArray.add(data.getFavourate().get(i));
                    }
                }else{
                    favArray.add(data.getFavourate().get(i));
                }
            }
            setListData(favArray, rvProduct, tvSeeAll, tvDiscoverTitle);
        }

        tvDiscoverTitle.setText(text);
        tvSeeAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (data != null) {
                    Intent intent = new Intent(context, SeeAllProductActivity.class);
                    intent.putExtra("title", text);
                    intent.putExtra("lat", lat);
                    intent.putExtra("lon", lon);
                    startActivity(intent);
                } else {
                    Toast.makeText(context, "No Data", Toast.LENGTH_SHORT).show();
                }
            }
        });

        addView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        dataBiding.llAddView.addView(addView);
    }

    private void setListData(List<CommonData> listData, RecyclerView rvProduct, TextView seeAll, TextView heading) {
        ProductAdapter productAdapter = new ProductAdapter(context, false, this);
        rvProduct.setAdapter(productAdapter);
        productAdapter.setData(listData);
        if (listData.size() > 0) {
            seeAll.setVisibility(View.VISIBLE);
            heading.setVisibility(View.VISIBLE);
        } else {
            seeAll.setVisibility(View.GONE);
            heading.setVisibility(View.GONE);
        }
    }

    private void myCampaignService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).
                getCampaignListApi(0, distance, chooseLocation.getCategoryId(), lat, lon)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UserGetAllCampaignRes>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull UserGetAllCampaignRes myCampaignListRes) {
                        progressLoader.dismiss();
                        handleResponse(myCampaignListRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void handleResponse(UserGetAllCampaignRes myCampaignListRes) {

        if (dataBiding.llAddView != null) dataBiding.llAddView.removeAllViews();

        if (myCampaignListRes.getStatus()) {
            dataBiding.includeCampaign.llNoItem.setVisibility(View.GONE);
            for (int i = 0; i < titleArray().size(); i++) {
                addView(titleArray().get(i), myCampaignListRes.getData());
            }
        } else {
            dataBiding.includeCampaign.llNoItem.setVisibility(View.VISIBLE);
            //Toast.makeText(context, "No Item Found", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFavProductClick(CommonData commonData) {
        addRemoveFavoritesService(commonData.getId());

    }

    private void addRemoveFavoritesService(Integer campaignId) {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).addRemoveFavoritesApi(campaignId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<JsonElement>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull JsonElement myCampaignListRes) {

                        handleFavoritesResponse(myCampaignListRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void handleFavoritesResponse(JsonElement jsonElement) {

        JsonObject jsonObject = jsonElement.getAsJsonObject();
        //  Toast.makeText(context, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
        if (jsonObject.get("status").getAsBoolean()) {
            myCampaignService();
        } else {
            progressLoader.dismiss();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                dataBiding.tvSearchCity.setText(place.getAddress());
                lat = String.valueOf(place.getLatLng().latitude);
                lon = String.valueOf(place.getLatLng().longitude);

                chooseLocation.setLatLng(place.getLatLng());
                chooseLocation.setAddress(place.getAddress());
                sharedPreference.putString(Constant.LOCATION_DATA, new Gson().toJson(chooseLocation));
                myCampaignService();
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {

                Status status = Autocomplete.getStatusFromIntent(data);

            } else if (resultCode == RESULT_CANCELED) {

            }
        } /*else if (requestCode == Constant.LOCATION_RESULT) {
            if (DialogUtility.isLocationEnable(context)) {
                ShowLocationDialog();
            }
        } */else if (resultCode == RESULT_OK && requestCode == Constant.RESERVE_CODE) {

        }
    }


}