package com.app.baakki.discover;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivitySeeAllProductBinding;
import com.app.baakki.databinding.FragmentFavoritesBinding;
import com.app.baakki.favorites.FavoritesAdapter;
import com.app.baakki.listener.OnFavProductClick;
import com.app.baakki.model.ChooseLocation;
import com.app.baakki.model.mycampaignlist.MyCampaignListData;
import com.app.baakki.model.usergetallcampaign.CommonData;
import com.app.baakki.model.usergetallcampaign.UserGelAllCampaignData;
import com.app.baakki.model.usergetallcampaign.UserGetAllCampaignRes;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.PaginationListener;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.app.baakki.util.PaginationListener.PAGE_START;


public class SeeAllProductActivity extends BaseActivity implements OnFavProductClick {
    ActivitySeeAllProductBinding dataBiding;

    Context context;
    CompositeDisposable compositeDisposable;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;

    String text;
    String lat, lon, address, categoryId;
            Integer distance;
    ChooseLocation chooseLocation;
    ProductAdapter productAdapter;

    List<CommonData> commonList = new ArrayList<>();
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private boolean isLoading = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataBiding = DataBindingUtil.setContentView(this, R.layout.activity_see_all_product);
        setToolbar(dataBiding.tool.ivBack, dataBiding.tool.tvTitle, dataBiding.tool.vwLine, getIntent().getStringExtra("title"));
        init();
    }

    private void init() {
        text = getIntent().getStringExtra("title");
        context = SeeAllProductActivity.this;
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        dataBiding.rvAllProduct.setLayoutManager(layoutManager);
        productAdapter = new ProductAdapter(context, true, this);
        dataBiding.setProductAdapter(productAdapter);

        chooseLocation = new Gson().fromJson(sharedPreference.getString(Constant.LOCATION_DATA), ChooseLocation.class);
        categoryId = chooseLocation.getCategoryId();

        if (chooseLocation.getDistance() != null)
            distance = Integer.valueOf(chooseLocation.getDistance().substring(0, chooseLocation.getDistance().length() - 3));

        lat = String.valueOf(chooseLocation.getLatLng().latitude);
        lon = String.valueOf(chooseLocation.getLatLng().longitude);

        myCampaignService();

        dataBiding.rvAllProduct.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage++;
                myCampaignService();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }


    private void myCampaignService() {
        if (currentPage == 0) progressLoader.show();
        else productAdapter.addLoading();

        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class)
                .getCampaignListApi(currentPage, distance, categoryId, lat, lon)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UserGetAllCampaignRes>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull UserGetAllCampaignRes myCampaignListRes) {
                        progressLoader.dismiss();
                        handleResponse(myCampaignListRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        isLastPage = true;
                        productAdapter.removeLoading();
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void handleResponse(UserGetAllCampaignRes myCampaignListRes) {
        if (myCampaignListRes.getStatus()) {
            UserGelAllCampaignData data = myCampaignListRes.getData();
            ArrayList<CommonData> recommendedArray = new ArrayList<>();
            ArrayList<CommonData> nearbyArray = new ArrayList<>();
            ArrayList<CommonData> expiresoonArray = new ArrayList<>();
            ArrayList<CommonData> trendingArray = new ArrayList<>();
            ArrayList<CommonData> favArray = new ArrayList<>();
            if (text.equals("Recommended for you")) {
                for (int i = 0; i < data.getRecommended().size(); i++) {
                    if (data.getRecommended().get(i).getCampaignType().equals("onetime")) {
                        if (!data.getRecommended().get(i).getRemainingQuantity().equals(0)) {
                            recommendedArray.add(data.getRecommended().get(i));
                        }
                    }else {
                        recommendedArray.add(data.getRecommended().get(i));
                    }
                }
                commonList.addAll(recommendedArray);
            } else if (text.equals("Nearby")) {
                for (int i = 0; i < data.getNearby().size(); i++) {
                    if (data.getNearby().get(i).getCampaignType().equals("onetime")) {
                        if (!data.getNearby().get(i).getRemainingQuantity().equals(0)) {
                            nearbyArray.add(data.getNearby().get(i));
                        }
                    } else {
                        nearbyArray.add(data.getNearby().get(i));
                    }
                }
                commonList.addAll(nearbyArray);
            } else if (text.equals("Expires soon")) {
                for (int i = 0; i < data.getExpirysoon().size(); i++) {
                  /*  if (data.getExpirysoon().get(i).getCampaignType().equals("onetime")) {*/
                        if (!data.getExpirysoon().get(i).getRemainingQuantity().equals(0)) {
                            expiresoonArray.add(data.getExpirysoon().get(i));
                        }
                   /* }else {
                        expiresoonArray.add(data.getExpirysoon().get(i));
                    }*/
                }
                commonList.addAll(expiresoonArray);
            }/* else if (text.equals("Expires today")) {
                commonList.addAll(data.getTodayexpiry());
            }*/ else if (text.equals("Trending")) {
                for (int i = 0; i < data.getTrending().size(); i++) {
                    if (data.getTrending().get(i).getCampaignType().equals("onetime")) {
                        if (!data.getTrending().get(i).getRemainingQuantity().equals(0)) {
                            trendingArray.add(data.getTrending().get(i));
                        }
                    }else {
                        trendingArray.add(data.getTrending().get(i));
                    }
                }
                commonList.addAll(trendingArray);
            } else if (text.equals("Favorites")) {
                for (int i = 0; i < data.getFavourate().size(); i++) {
                    if (data.getFavourate().get(i).getCampaignType().equals("onetime")) {
                        if (!data.getFavourate().get(i).getRemainingQuantity().equals(0)) {
                            favArray.add(data.getFavourate().get(i));
                        }
                    }else{
                        favArray.add(data.getFavourate().get(i));
                    }
                }
                commonList.addAll(favArray);
            }

            productAdapter.setData(commonList);
            isLoading = false;
        } else {
            isLastPage = true;
            productAdapter.removeLoading();
           // if (currentPage == 0)
               // Toast.makeText(context, myCampaignListRes.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onFavProductClick(CommonData commonData) {
        addRemoveFavoritesService(commonData.getId());
    }

    private void addRemoveFavoritesService(Integer campaignId) {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).addRemoveFavoritesApi(campaignId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<JsonElement>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull JsonElement myCampaignListRes) {

                        handleFavoritesResponse(myCampaignListRes);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void handleFavoritesResponse(JsonElement jsonElement) {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
       // Toast.makeText(context, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
        if (jsonObject.get("status").getAsBoolean()) {
            currentPage = PAGE_START;
            isLastPage = false;
            commonList.clear();
            productAdapter.clear();
            myCampaignService();
        } else {
            progressLoader.dismiss();
        }
    }


}