package com.app.baakki.discover;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityReviewBinding;
import com.app.baakki.home.HomeActivity;
import com.app.baakki.listener.LocationHelper;
import com.app.baakki.login.SignUpActivity;
import com.app.baakki.model.createorder.CreateOrder;
import com.app.baakki.model.usergetallcampaign.CommonData;
import com.app.baakki.model.usergetallcampaign.UserGetAllCampaignRes;
import com.app.baakki.other.PrivacyPolicyActivity;
import com.app.baakki.other.TermsConditionActivity;
import com.app.baakki.services.ApiClient;
import com.app.baakki.services.ApiInterface;
import com.app.baakki.util.Constant;
import com.app.baakki.util.DialogUtility;
import com.app.baakki.util.LocationBuilder;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.app.baakki.util.ValidationUtils;
import com.google.android.gms.location.LocationResult;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.app.baakki.util.Constant.serverDate;

public class ReviewActivity extends BaseActivity /*implements LocationHelper*/ {
    Context context;
    CompositeDisposable compositeDisposable;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityReviewBinding activityBinding;
    CommonData commonData;
    LocationBuilder locationBuilder;
    int quantity = 1, price;
    double percentage;
   /* String lat, lon, address;*/

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_review);
        setToolbar(activityBinding.tool.ivBack, activityBinding.tool.tvTitle, activityBinding.tool.vwLine, "Review");
        activityBinding.setHandlers(new MyClickHandlers());
        init();
    }

    @SuppressLint("SetTextI18n")
    private void init() {
        context = ReviewActivity.this;
        compositeDisposable = new CompositeDisposable();
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);

        activityBinding.tvOriginalPrice.setPaintFlags(activityBinding.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        commonData = getIntent().getParcelableExtra(Constant.PRODUCT_DETAIL_DATA);
        activityBinding.setProductDetailData(commonData);

        int diff = (commonData.getOriginalPrice() - commonData.getOfferPrice()) * 100;
        percentage = (diff) / commonData.getOriginalPrice();
        activityBinding.tvPercentage.setText((int) percentage + "% off");
        price = commonData.getOfferPrice();
        if (getIntent().getBooleanExtra("isFav",false)) {
            activityBinding.tvTime.setText(serverDate(commonData.getFromTime()) + " to " + serverDate(commonData.getToTime()));

        } else {
            activityBinding.tvTime.setText(serverDate(commonData.getFromTime()) + " to " + serverDate(commonData.getToTime()) + "  \u2022 " + commonData.getDistance() + "Km");
        }




       /*
        locationBuilder = new LocationBuilder(ReviewActivity.this);
        locationBuilder.getLocation(ReviewActivity.this);

       if (DialogUtility.isLocationEnable(context)) {
            ShowLocationDialog();
        }*/
        setTextClick(activityBinding.tvTermsCondition);
    }

    public class MyClickHandlers {
        public void onReserveClick(View view) {
            if (isValid())
                orderCreateService();
        }

        public void onMinusClick(View view) {
            if (quantity > 1) {
                quantity--;
                activityBinding.tvQuantity.setText(String.valueOf(quantity));
                activityBinding.tvTotalPrice.setText("£" + price * quantity);
            }
        }

        public void onAddClick(View view) {
            quantity++;
            activityBinding.tvQuantity.setText(String.valueOf(quantity));
            activityBinding.tvTotalPrice.setText("£" + price * quantity);
        }
    }

    private boolean isValid() {

        boolean isValid = true;
        if (commonData.getRemainingQuantity() < quantity) {
            Toast.makeText(context, "Purchase quantity should be less than stock quantity", Toast.LENGTH_SHORT).show();
            isValid = false;
        }/*else if (!getIntent().getStringExtra(Constant.OTP).equals(code)) {
            Toast.makeText(context, context.getResources().getString(R.string.incorrect_code), Toast.LENGTH_SHORT).show();
            isValid = false;
        }*/
        return isValid;
    }

    private void orderCreateService() {
        progressLoader.show();
        compositeDisposable.add(ApiClient.getClient().create(ApiInterface.class).
              /*  orderCreateApi(commonData.getCampaintimesId(), lat, lon, quantity)*/
                orderCreateApi(commonData.getCampaintimesId(), quantity)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CreateOrder>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(@NonNull CreateOrder createOrder) {
                        progressLoader.dismiss();
                        handleResponse(createOrder);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        progressLoader.dismiss();
                        ValidationUtils.errorCode(context, e.getMessage());
                    }
                }));
    }

    private void handleResponse(CreateOrder createOrder) {
       // Toast.makeText(context, createOrder.getMessage(), Toast.LENGTH_SHORT).show();
        if (createOrder.getStatus()) {
            Intent intent = new Intent(context, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            /*  Intent intent = new Intent(context, PaymentActivity.class);
            intent.putExtra(Constant.PRODUCT_DETAIL_DATA, commonData);
            startActivity(intent);*/
        }
    }

  /*  private void ShowLocationDialog() {
        DialogUtility.alertMessageNoGps(context, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, Constant.LOCATION_RESULT);
            }
        });
    }

    @Override
    public void getLocation(LocationResult locationResult) {
        for (Location location : locationResult.getLocations()) {
            if (location.getLatitude() == 0 && location.getLongitude() == 0) {
                Log.e("null lat log", "=--=-=");
                return;
            }

            lat = String.valueOf(location.getLatitude());
            lon = String.valueOf(location.getLongitude());
            address = locationBuilder.getAddress(context, location.getLatitude(), location.getLongitude());

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.LOCATION_RESULT) {
            if (DialogUtility.isLocationEnable(context)) {
                ShowLocationDialog();
            }
        }
    }*/

    private void setTextClick(TextView textView) {
        SpannableString ss = new SpannableString(textView.getText().toString());

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                startActivity(new Intent(context, TermsConditionActivity.class));
            }

        };

        ClickableSpan clickableSpan2 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                startActivity(new Intent(context, PrivacyPolicyActivity.class));
            }
        };

        ss.setSpan(clickableSpan1, 36, 56, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan2, 75, 89, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }
}

