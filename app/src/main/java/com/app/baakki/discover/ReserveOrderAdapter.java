package com.app.baakki.discover;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.databinding.ItemPreviousPurchaseBinding;
import com.app.baakki.databinding.ItemReserveOrderBinding;
import com.app.baakki.model.reserveorder.ReserveOrderData;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import static com.app.baakki.util.Constant.serverDate;

public class ReserveOrderAdapter extends RecyclerView.Adapter<ReserveOrderAdapter.ViewHolder>  {
    private Context context;
    ArrayList<ReserveOrderData> dataModelList;
    ReserveOrderActivity.MyClickListener myClickListener;

    public ReserveOrderAdapter(Context ctx, ReserveOrderActivity.MyClickListener myClickListener) {
        context = ctx;
        this.myClickListener = myClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemReserveOrderBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_reserve_order, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemRowBinding.setData(dataModelList.get(position));
        holder.itemRowBinding.setHandler(myClickListener);
        holder.itemRowBinding.tvOriginalPrice.setPaintFlags(holder.itemRowBinding.tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.itemRowBinding.tvDiscoverTime.setText(serverDate(dataModelList.get(position)
                .getFromTime()) + " to " + serverDate(dataModelList.get(position).getToTime())+"  \u2022 "+dataModelList.get(position).getDistance()+" km");

        int diff=(dataModelList.get(position).getOriginalPrice()-dataModelList.get(position).getOfferPrice())*100;
       double percentage =  (diff)/dataModelList.get(position).getOriginalPrice();
        holder.itemRowBinding.tvPercentage.setText(String.valueOf((int)percentage)+"% off");
    }

    public void setData(ArrayList<ReserveOrderData> dataModelList){
        this.dataModelList = dataModelList;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return dataModelList==null?0:dataModelList.size();
    }

    @BindingAdapter("app:srcReserveImage")
    public static void srcReserveImage(ImageView view, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            RequestOptions options = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.iv_shop_placeholder);
            Glide.with(view.getContext())
                    .load(imageUrl)
                    .apply(options)
                    .into(view);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemReserveOrderBinding itemRowBinding;

        public ViewHolder(ItemReserveOrderBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }

}