package com.app.baakki.discover;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.baakki.R;
import com.app.baakki.databinding.ItemDiscoverCategoryBinding;
import com.app.baakki.listener.OnCategoryClickListener;
import com.app.baakki.model.category.CategoryData;
import com.app.baakki.model.usergetallcampaign.CommonData;
import com.app.baakki.util.Constant;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

public class DiscoverCategoryAdapter extends RecyclerView.Adapter<DiscoverCategoryAdapter.ViewHolder> {
    private Context context;
    String categoryId;
    List<CategoryData> dataModelList;
    OnCategoryClickListener onCategoryClickListener;
    private int selectedPos = -1;

    public DiscoverCategoryAdapter(Context ctx, OnCategoryClickListener onCategoryClickListener) {
        context = ctx;
        this.onCategoryClickListener = onCategoryClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemDiscoverCategoryBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_discover_category, parent, false);
        binding.getRoot().getLayoutParams().width = (int) (getScreenWidth() / 3.75);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CategoryData dataList = dataModelList.get(position);
        holder.itemRowBinding.setCategoryData(dataList);
        holder.itemRowBinding.setPos(position);
        holder.itemRowBinding.setHandlers(new MyClickHandlers());
        if (categoryId != null)
            if (dataList.getId() == Integer.parseInt(categoryId)) {
                selectedPos = position;
            }

        holder.itemRowBinding.setSelected(selectedPos == position);
        holder.itemRowBinding.executePendingBindings();
    }

    public void setData(List<CategoryData> dataModelList, String categoryId) {
        this.dataModelList = dataModelList;
        this.categoryId = categoryId;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return dataModelList == null ? 0 : dataModelList.size();
    }

    @BindingAdapter("app:srcUserCategoryPhoto")
    public static void srcUserCategoryPhoto(ImageView view, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl)) {
            RequestOptions options = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.iv_shop_placeholder);
            Glide.with(view.getContext())
                    .load(imageUrl)
                    .apply(options)
                    .into(view);
        }
    }

    public class MyClickHandlers {
        public void onCategoryClick(int pos) {
            if (onCategoryClickListener != null) {
                if (pos != RecyclerView.NO_POSITION) {
                    onCategoryClickListener.onCategoryItemSelected(dataModelList.get(pos));
                    notifyItemChanged(selectedPos);
                    categoryId = String.valueOf(dataModelList.get(pos).getId());
                    selectedPos = pos;
                    notifyItemChanged(selectedPos);
                }
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemDiscoverCategoryBinding itemRowBinding;

        public ViewHolder(ItemDiscoverCategoryBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }
    }

    public int getScreenWidth() {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

}