package com.app.baakki.discover;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.app.baakki.R;
import com.app.baakki.base.BaseActivity;
import com.app.baakki.databinding.ActivityAboutProductBinding;

import com.app.baakki.model.usergetallcampaign.CommonData;
import com.app.baakki.util.Constant;
import com.app.baakki.util.DialogUtility;
import com.app.baakki.util.ProgressLoader;
import com.app.baakki.util.SharedPreference;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class AboutProductActivity extends BaseActivity implements OnMapReadyCallback {
    Context context;
    ProgressLoader progressLoader;
    SharedPreference sharedPreference;
    ActivityAboutProductBinding activityBinding;
    CommonData commonData;
    GoogleMap googleMap;
    Marker marker;
    LatLng latLng;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_about_product);

        init();
    }

    private void init() {
        context = AboutProductActivity.this;
        progressLoader = new ProgressLoader(context);
        sharedPreference = SharedPreference.getInstance(context);
        activityBinding.setHandlers(new MyClickHandlers());

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        commonData = getIntent().getParcelableExtra(Constant.PRODUCT_DETAIL_DATA);
        activityBinding.setProductDetailData(commonData);

        setToolbar(activityBinding.tool.ivBack, activityBinding.tool.tvTitle, activityBinding.tool.vwLine,
                "About " + commonData.getBusinessName());

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        latLng = new LatLng(Double.parseDouble(commonData.getLat()), Double.parseDouble(commonData.getLon()));
        addMarker(latLng, 12);
    }

    public void addMarker(LatLng latLng, int i) {
        googleMap.clear();
        marker = googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(bitmapDescriptorFromVector(context, R.drawable.ic_baakki_marker)));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, i), 5000, null);
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public class MyClickHandlers {
        public void onNavigationClick(View view) {
            String url = "http://maps.google.com/maps?daddr=" + latLng.latitude + "," + latLng.longitude + " (" + commonData.getBusinessAddress() + ")";
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);


        }
    }
}
