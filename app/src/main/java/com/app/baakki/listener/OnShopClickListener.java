package com.app.baakki.listener;

import com.app.baakki.model.shoplist.ShopListData;

public interface OnShopClickListener {
    void onItemSelected(ShopListData item);
}
