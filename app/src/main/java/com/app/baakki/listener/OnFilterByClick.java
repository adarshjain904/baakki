package com.app.baakki.listener;

public interface OnFilterByClick {
    void onFilterByClick(String serviceName,String StoreName,String collectionStartTime,String collectionEndTime,String soldOut);
}
