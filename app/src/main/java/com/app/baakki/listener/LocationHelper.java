package com.app.baakki.listener;

import com.google.android.gms.location.LocationResult;

public interface LocationHelper {
    void getLocation(LocationResult locationResult);
}
