package com.app.baakki.listener;

import com.app.baakki.model.category.CategoryData;
import com.app.baakki.model.subcategory.SubcategoryData;

public interface OnSubcategoryClickListener {
    void onSubcategoryItemSelected(SubcategoryData item);
}
