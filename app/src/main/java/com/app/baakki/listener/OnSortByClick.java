package com.app.baakki.listener;

public interface OnSortByClick {
    void onSortByClick(String rating, String price,String ending,String newly,String nearest);
}
