package com.app.baakki.listener;

import com.app.baakki.model.category.CategoryData;
import com.app.baakki.model.shoplist.ShopListData;

public interface OnCategoryClickListener {
    void onCategoryItemSelected(CategoryData item);
}
