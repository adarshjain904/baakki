package com.app.baakki.listener;

import com.app.baakki.model.category.CategoryData;

public interface SelectCategoryClick {
    void selectCategory(CategoryData category);
}
