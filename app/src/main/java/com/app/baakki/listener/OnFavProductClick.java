package com.app.baakki.listener;

import com.app.baakki.model.usergetallcampaign.CommonData;

public interface OnFavProductClick {
    void onFavProductClick(CommonData  commonData);
}
