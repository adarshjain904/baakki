package com.app.baakki.listener;

import com.app.baakki.model.category.CategoryData;
import com.app.baakki.model.country.CountryData;

public interface OnCountryClickListener {
    void onCategoryItemSelected(CountryData item);
}
