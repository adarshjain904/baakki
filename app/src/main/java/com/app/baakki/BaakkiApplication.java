package com.app.baakki;
import android.app.Application;
import android.content.Context;
import androidx.annotation.Nullable;
import org.jetbrains.annotations.Contract;

import io.reactivex.plugins.RxJavaPlugins;


public class BaakkiApplication extends Application {
    private static final String TAG = "UIKitApplication";
    private static BaakkiApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

    }

    @Contract(pure = true)
    @Nullable
    public static Context getAppContext() {
        return sInstance;
    }

}
