
package com.app.baakki.model.usergetallcampaign;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommonData implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("cid")
    @Expose
    private String cid;
    @SerializedName("campaign_id")
    @Expose
    private String campaignId;
    @SerializedName("campaintimes_id")
    @Expose
    private String campaintimesId;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("subcategory_id")
    @Expose
    private Integer subcategoryId;
    @SerializedName("campaign_type")
    @Expose
    private String campaignType;
    @SerializedName("shop_id")
    @Expose
    private Integer shopId;
    @SerializedName("original_price")
    @Expose
    private Integer originalPrice;
    @SerializedName("offer_price")
    @Expose
    private Integer offerPrice;
    @SerializedName("campaign_heading")
    @Expose
    private String campaignHeading;
    @SerializedName("stock_quantity")
    @Expose
    private Integer stockQuantity;
    @SerializedName("remaining_quantity")
    @Expose
    private Integer remainingQuantity;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("how_to_redeem")
    @Expose
    private String howToRedeem;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("is_deleted")
    @Expose
    private String isDeleted;
    @SerializedName("is_listing")
    @Expose
    private String isListing;
    @SerializedName("cam_expiry")
    @Expose
    private String camExpiry;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("from_time")
    @Expose
    private String fromTime;
    @SerializedName("to_time")
    @Expose
    private String toTime;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("category_image")
    @Expose
    private String categoryImage;
    @SerializedName("category_name")
    @Expose
    private String categoryName;

    @SerializedName("is_liked")
    @Expose
    private Integer isLike;
    @SerializedName("rating_members")
    @Expose
    private Integer ratingMembers;
    @SerializedName("avg_rating")
    @Expose
    private String avgRating;
    @SerializedName("business_name")
    @Expose
    private String businessName;
    @SerializedName("business_lat")
    @Expose
    private String businessLat;
    @SerializedName("business_lon")
    @Expose
    private String businessLon;
    @SerializedName("business_country")
    @Expose
    private String businessCountry;
    @SerializedName("business_address")
    @Expose
    private String businessAddress;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("about_shop")
    @Expose
    private String aboutShop;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCampaintimesId() {
        return campaintimesId;
    }

    public void setCampaintimesId(String campaintimesId) {
        this.campaintimesId = campaintimesId;
    }

    public Integer getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(Integer subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getRemainingQuantity() {
        return remainingQuantity;
    }

    public void setRemainingQuantity(Integer remainingQuantity) {
        this.remainingQuantity = remainingQuantity;
    }

    public Integer getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(Integer originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Integer getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(Integer offerPrice) {
        this.offerPrice = offerPrice;
    }

    public String getCampaignHeading() {
        return campaignHeading;
    }

    public void setCampaignHeading(String campaignHeading) {
        this.campaignHeading = campaignHeading;
    }

    public Integer getIsLike() {
        return isLike;
    }

    public void setIsLike(Integer isLike) {
        this.isLike = isLike;
    }

    public Integer getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(Integer stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHowToRedeem() {
        return howToRedeem;
    }

    public void setHowToRedeem(String howToRedeem) {
        this.howToRedeem = howToRedeem;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getIsListing() {
        return isListing;
    }

    public void setIsListing(String isListing) {
        this.isListing = isListing;
    }

    public String getCamExpiry() {
        return camExpiry;
    }

    public void setCamExpiry(String camExpiry) {
        this.camExpiry = camExpiry;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Integer getRatingMembers() {
        return ratingMembers;
    }

    public void setRatingMembers(Integer ratingMembers) {
        this.ratingMembers = ratingMembers;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessLat() {
        return businessLat;
    }

    public void setBusinessLat(String businessLat) {
        this.businessLat = businessLat;
    }

    public String getBusinessLon() {
        return businessLon;
    }

    public void setBusinessLon(String businessLon) {
        this.businessLon = businessLon;
    }

    public String getBusinessCountry() {
        return businessCountry;
    }

    public void setBusinessCountry(String businessCountry) {
        this.businessCountry = businessCountry;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAboutShop() {
        return aboutShop;
    }

    public void setAboutShop(String aboutShop) {
        this.aboutShop = aboutShop;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(String avgRating) {
        this.avgRating = avgRating;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.cid);
        dest.writeString(this.campaignId);
        dest.writeString(this.campaintimesId);
        dest.writeValue(this.categoryId);
        dest.writeValue(this.subcategoryId);
        dest.writeString(this.campaignType);
        dest.writeValue(this.shopId);
        dest.writeValue(this.originalPrice);
        dest.writeValue(this.offerPrice);
        dest.writeString(this.campaignHeading);
        dest.writeValue(this.stockQuantity);
        dest.writeValue(this.remainingQuantity);
        dest.writeString(this.description);
        dest.writeString(this.howToRedeem);
        dest.writeString(this.image);
        dest.writeString(this.address);
        dest.writeString(this.lat);
        dest.writeString(this.lon);
        dest.writeString(this.isDeleted);
        dest.writeString(this.isListing);
        dest.writeString(this.camExpiry);
        dest.writeString(this.date);
        dest.writeString(this.fromTime);
        dest.writeString(this.toTime);
        dest.writeValue(this.distance);
        dest.writeString(this.categoryImage);
        dest.writeString(this.categoryName);
        dest.writeValue(this.isLike);
        dest.writeValue(this.ratingMembers);
        dest.writeString(this.avgRating);
        dest.writeString(this.businessName);
        dest.writeString(this.businessLat);
        dest.writeString(this.businessLon);
        dest.writeString(this.businessCountry);
        dest.writeString(this.businessAddress);
        dest.writeString(this.mobile);
        dest.writeString(this.aboutShop);
    }

    public void readFromParcel(Parcel source) {
        this.id = (Integer) source.readValue(Integer.class.getClassLoader());
        this.cid = source.readString();
        this.campaignId = source.readString();
        this.campaintimesId = source.readString();
        this.categoryId = (Integer) source.readValue(Integer.class.getClassLoader());
        this.subcategoryId = (Integer) source.readValue(Integer.class.getClassLoader());
        this.campaignType = source.readString();
        this.shopId = (Integer) source.readValue(Integer.class.getClassLoader());
        this.originalPrice = (Integer) source.readValue(Integer.class.getClassLoader());
        this.offerPrice = (Integer) source.readValue(Integer.class.getClassLoader());
        this.campaignHeading = source.readString();
        this.stockQuantity = (Integer) source.readValue(Integer.class.getClassLoader());
        this.remainingQuantity = (Integer) source.readValue(Integer.class.getClassLoader());
        this.description = source.readString();
        this.howToRedeem = source.readString();
        this.image = source.readString();
        this.address = source.readString();
        this.lat = source.readString();
        this.lon = source.readString();
        this.isDeleted = source.readString();
        this.isListing = source.readString();
        this.camExpiry = source.readString();
        this.date = source.readString();
        this.fromTime = source.readString();
        this.toTime = source.readString();
        this.distance = (Double) source.readValue(Double.class.getClassLoader());
        this.categoryImage = source.readString();
        this.categoryName = source.readString();
        this.isLike = (Integer) source.readValue(Integer.class.getClassLoader());
        this.ratingMembers = (Integer) source.readValue(Integer.class.getClassLoader());
        this.avgRating = source.readString();
        this.businessName = source.readString();
        this.businessLat = source.readString();
        this.businessLon = source.readString();
        this.businessCountry = source.readString();
        this.businessAddress = source.readString();
        this.mobile = source.readString();
        this.aboutShop = source.readString();
    }

    public CommonData() {
    }

    protected CommonData(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.cid = in.readString();
        this.campaignId = in.readString();
        this.campaintimesId = in.readString();
        this.categoryId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.subcategoryId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.campaignType = in.readString();
        this.shopId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.originalPrice = (Integer) in.readValue(Integer.class.getClassLoader());
        this.offerPrice = (Integer) in.readValue(Integer.class.getClassLoader());
        this.campaignHeading = in.readString();
        this.stockQuantity = (Integer) in.readValue(Integer.class.getClassLoader());
        this.remainingQuantity = (Integer) in.readValue(Integer.class.getClassLoader());
        this.description = in.readString();
        this.howToRedeem = in.readString();
        this.image = in.readString();
        this.address = in.readString();
        this.lat = in.readString();
        this.lon = in.readString();
        this.isDeleted = in.readString();
        this.isListing = in.readString();
        this.camExpiry = in.readString();
        this.date = in.readString();
        this.fromTime = in.readString();
        this.toTime = in.readString();
        this.distance = (Double) in.readValue(Double.class.getClassLoader());
        this.categoryImage = in.readString();
        this.categoryName = in.readString();
        this.isLike = (Integer) in.readValue(Integer.class.getClassLoader());
        this.ratingMembers = (Integer) in.readValue(Integer.class.getClassLoader());
        this.avgRating = in.readString();
        this.businessName = in.readString();
        this.businessLat = in.readString();
        this.businessLon = in.readString();
        this.businessCountry = in.readString();
        this.businessAddress = in.readString();
        this.mobile = in.readString();
        this.aboutShop = in.readString();
    }

    public static final Parcelable.Creator<CommonData> CREATOR = new Parcelable.Creator<CommonData>() {
        @Override
        public CommonData createFromParcel(Parcel source) {
            return new CommonData(source);
        }

        @Override
        public CommonData[] newArray(int size) {
            return new CommonData[size];
        }
    };
}
