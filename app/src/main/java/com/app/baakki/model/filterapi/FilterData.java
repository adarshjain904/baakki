
package com.app.baakki.model.filterapi;

import java.util.List;

import com.app.baakki.model.usergetallcampaign.CommonData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class FilterData {

    @SerializedName("filterdata")
    @Expose
    private List<CommonData> filterdata = null;
    @SerializedName("apply_fiterkey")
    @Expose
    private ApplyFiterkey applyFiterkey;

    public List<CommonData> getFilterdata() {
        return filterdata;
    }

    public void setFilterdata(List<CommonData> filterdata) {
        this.filterdata = filterdata;
    }

    public ApplyFiterkey getApplyFiterkey() {
        return applyFiterkey;
    }

    public void setApplyFiterkey(ApplyFiterkey applyFiterkey) {
        this.applyFiterkey = applyFiterkey;
    }

}
