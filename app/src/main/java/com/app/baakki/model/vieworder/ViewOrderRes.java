
package com.app.baakki.model.vieworder;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ViewOrderRes {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("user_data")
    @Expose
    private List<UserDatum> userData = null;
    @SerializedName("total_order")
    @Expose
    private List<Integer> totalOrder = null;
    @SerializedName("total_order_quantity")
    @Expose
    private List<String> totalOrderQuantity = null;
    @SerializedName("alluserdata")
    @Expose
    private List<List<Alluserdatum>> alluserdata = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<UserDatum> getUserData() {
        return userData;
    }

    public void setUserData(List<UserDatum> userData) {
        this.userData = userData;
    }

    public List<Integer> getTotalOrder() {
        return totalOrder;
    }

    public void setTotalOrder(List<Integer> totalOrder) {
        this.totalOrder = totalOrder;
    }

    public List<String> getTotalOrderQuantity() {
        return totalOrderQuantity;
    }

    public void setTotalOrderQuantity(List<String> totalOrderQuantity) {
        this.totalOrderQuantity = totalOrderQuantity;
    }

    public List<List<Alluserdatum>> getAlluserdata() {
        return alluserdata;
    }

    public void setAlluserdata(List<List<Alluserdatum>> alluserdata) {
        this.alluserdata = alluserdata;
    }

}
