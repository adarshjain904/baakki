
package com.app.baakki.model.createorder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CreateOrder {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private CreateOrderData data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CreateOrderData getData() {
        return data;
    }

    public void setData(CreateOrderData data) {
        this.data = data;
    }

}
