
package com.app.baakki.model.mycampaignlist;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shop implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("cid")
    @Expose
    private String cid;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("business_type")
    @Expose
    private String businessType;
    @SerializedName("business_name")
    @Expose
    private String businessName;
    @SerializedName("business_hours")
    @Expose
    private String businessHours;
    @SerializedName("business_lat")
    @Expose
    private String businessLat;
    @SerializedName("business_lon")
    @Expose
    private String businessLon;
    @SerializedName("business_address")
    @Expose
    private String businessAddress;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessHours() {
        return businessHours;
    }

    public void setBusinessHours(String businessHours) {
        this.businessHours = businessHours;
    }

    public String getBusinessLat() {
        return businessLat;
    }

    public void setBusinessLat(String businessLat) {
        this.businessLat = businessLat;
    }

    public String getBusinessLon() {
        return businessLon;
    }

    public void setBusinessLon(String businessLon) {
        this.businessLon = businessLon;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.cid);
        dest.writeValue(this.userId);
        dest.writeString(this.businessType);
        dest.writeString(this.businessName);
        dest.writeString(this.businessHours);
        dest.writeString(this.businessLat);
        dest.writeString(this.businessLon);
        dest.writeString(this.businessAddress);
        dest.writeString(this.image);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
    }

    public void readFromParcel(Parcel source) {
        this.id = (Integer) source.readValue(Integer.class.getClassLoader());
        this.cid = source.readString();
        this.userId = (Integer) source.readValue(Integer.class.getClassLoader());
        this.businessType = source.readString();
        this.businessName = source.readString();
        this.businessHours = source.readString();
        this.businessLat = source.readString();
        this.businessLon = source.readString();
        this.businessAddress = source.readString();
        this.image = source.readString();
        this.createdAt = source.readString();
        this.updatedAt = source.readString();
    }

    public Shop() {
    }

    protected Shop(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.cid = in.readString();
        this.userId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.businessType = in.readString();
        this.businessName = in.readString();
        this.businessHours = in.readString();
        this.businessLat = in.readString();
        this.businessLon = in.readString();
        this.businessAddress = in.readString();
        this.image = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
    }

    public static final Parcelable.Creator<Shop> CREATOR = new Parcelable.Creator<Shop>() {
        @Override
        public Shop createFromParcel(Parcel source) {
            return new Shop(source);
        }

        @Override
        public Shop[] newArray(int size) {
            return new Shop[size];
        }
    };
}
