
package com.app.baakki.model.pendingcompletecampaign;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Pending {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("cid")
    @Expose
    private String cid;
    @SerializedName("campaintimes_id")
    @Expose
    private Integer campaintimesId;
    @SerializedName("campaintimes_cid")
    @Expose
    private String campaintimesCid;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("subcategory_id")
    @Expose
    private Integer subcategoryId;
    @SerializedName("campaign_type")
    @Expose
    private String campaignType;
    @SerializedName("shop_id")
    @Expose
    private Integer shopId;
    @SerializedName("original_price")
    @Expose
    private Integer originalPrice;
    @SerializedName("offer_price")
    @Expose
    private Integer offerPrice;
    @SerializedName("campaign_heading")
    @Expose
    private String campaignHeading;
    @SerializedName("stock_quantity")
    @Expose
    private Integer stockQuantity;
    @SerializedName("remaining_quantity")
    @Expose
    private Integer remainingQuantity;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("how_to_redeem")
    @Expose
    private String howToRedeem;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("is_deleted")
    @Expose
    private String isDeleted;
    @SerializedName("is_listing")
    @Expose
    private String isListing;
    @SerializedName("cam_expiry")
    @Expose
    private String camExpiry;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("from_time")
    @Expose
    private String fromTime;
    @SerializedName("to_time")
    @Expose
    private String toTime;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("is_liked")
    @Expose
    private Integer isLiked;
    @SerializedName("category_image")
    @Expose
    private String categoryImage;
    @SerializedName("category_name")
    @Expose
    private String categoryName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public Integer getCampaintimesId() {
        return campaintimesId;
    }

    public void setCampaintimesId(Integer campaintimesId) {
        this.campaintimesId = campaintimesId;
    }

    public String getCampaintimesCid() {
        return campaintimesCid;
    }

    public void setCampaintimesCid(String campaintimesCid) {
        this.campaintimesCid = campaintimesCid;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(Integer subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(Integer originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Integer getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(Integer offerPrice) {
        this.offerPrice = offerPrice;
    }

    public String getCampaignHeading() {
        return campaignHeading;
    }

    public void setCampaignHeading(String campaignHeading) {
        this.campaignHeading = campaignHeading;
    }

    public Integer getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(Integer stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public Integer getRemainingQuantity() {
        return remainingQuantity;
    }

    public void setRemainingQuantity(Integer remainingQuantity) {
        this.remainingQuantity = remainingQuantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHowToRedeem() {
        return howToRedeem;
    }

    public void setHowToRedeem(String howToRedeem) {
        this.howToRedeem = howToRedeem;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getIsListing() {
        return isListing;
    }

    public void setIsListing(String isListing) {
        this.isListing = isListing;
    }

    public String getCamExpiry() {
        return camExpiry;
    }

    public void setCamExpiry(String camExpiry) {
        this.camExpiry = camExpiry;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(Integer isLiked) {
        this.isLiked = isLiked;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

}
