
package com.app.baakki.model.termpolicy;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class TermPolicyRes {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("term_and_condition")
    @Expose
    private String termAndCondition;
    @SerializedName("privacy_policy")
    @Expose
    private String privacyPolicy;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getTermAndCondition() {
        return termAndCondition;
    }

    public void setTermAndCondition(String termAndCondition) {
        this.termAndCondition = termAndCondition;
    }

    public String getPrivacyPolicy() {
        return privacyPolicy;
    }

    public void setPrivacyPolicy(String privacyPolicy) {
        this.privacyPolicy = privacyPolicy;
    }

}
