
package com.app.baakki.model.vieworder;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Alluserdatum implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("role_id")
    @Expose
    private String roleId;
    @SerializedName("cid")
    @Expose
    private String cid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("profile_photo")
    @Expose
    private String profilePhoto;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("postal_code")
    @Expose
    private String postalCode;
    @SerializedName("lcoality")
    @Expose
    private String lcoality;
    @SerializedName("business_type")
    @Expose
    private String businessType;
    @SerializedName("social_id")
    @Expose
    private String socialId;
    @SerializedName("register_via")
    @Expose
    private String registerVia;
    @SerializedName("device_token")
    @Expose
    private String deviceToken;
    @SerializedName("device_type")
    @Expose
    private String deviceType;
    @SerializedName("remember_token")
    @Expose
    private String rememberToken;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("campaintimes_cid")
    @Expose
    private String campaintimesCid;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("order_status")
    @Expose
    private String orderStatus;
    @SerializedName("order_date")
    @Expose
    private String orderDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getLcoality() {
        return lcoality;
    }

    public void setLcoality(String lcoality) {
        this.lcoality = lcoality;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getRegisterVia() {
        return registerVia;
    }

    public void setRegisterVia(String registerVia) {
        this.registerVia = registerVia;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(String rememberToken) {
        this.rememberToken = rememberToken;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCampaintimesCid() {
        return campaintimesCid;
    }

    public void setCampaintimesCid(String campaintimesCid) {
        this.campaintimesCid = campaintimesCid;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.roleId);
        dest.writeString(this.cid);
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.mobile);
        dest.writeString(this.password);
        dest.writeString(this.profilePhoto);
        dest.writeString(this.countryId);
        dest.writeString(this.address);
        dest.writeString(this.lat);
        dest.writeString(this.lon);
        dest.writeString(this.country);
        dest.writeString(this.state);
        dest.writeString(this.postalCode);
        dest.writeString(this.lcoality);
        dest.writeString(this.businessType);
        dest.writeString(this.socialId);
        dest.writeString(this.registerVia);
        dest.writeString(this.deviceToken);
        dest.writeString(this.deviceType);
        dest.writeString(this.rememberToken);
        dest.writeString(this.isActive);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeString(this.campaintimesCid);
        dest.writeString(this.quantity);
        dest.writeString(this.orderStatus);
        dest.writeString(this.orderDate);
    }

    public void readFromParcel(Parcel source) {
        this.id = (Integer) source.readValue(Integer.class.getClassLoader());
        this.roleId = source.readString();
        this.cid = source.readString();
        this.name = source.readString();
        this.email = source.readString();
        this.mobile = source.readParcelable(String.class.getClassLoader());
        this.password = source.readString();
        this.profilePhoto = source.readString();
        this.countryId = source.readString();
        this.address = source.readParcelable(String.class.getClassLoader());
        this.lat = source.readString();
        this.lon = source.readString();
        this.country = source.readParcelable(String.class.getClassLoader());
        this.state = source.readParcelable(String.class.getClassLoader());
        this.postalCode = source.readParcelable(String.class.getClassLoader());
        this.lcoality = source.readParcelable(String.class.getClassLoader());
        this.businessType = source.readParcelable(String.class.getClassLoader());
        this.socialId = source.readParcelable(String.class.getClassLoader());
        this.registerVia = source.readString();
        this.deviceToken = source.readString();
        this.deviceType = source.readString();
        this.rememberToken = source.readParcelable(String.class.getClassLoader());
        this.isActive = source.readString();
        this.createdAt = source.readString();
        this.updatedAt = source.readString();
        this.campaintimesCid = source.readString();
        this.quantity = source.readString();
        this.orderStatus = source.readString();
        this.orderDate = source.readString();
    }

    public Alluserdatum() {
    }

    protected Alluserdatum(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.roleId = in.readString();
        this.cid = in.readString();
        this.name = in.readString();
        this.email = in.readString();
        this.mobile = in.readParcelable(String.class.getClassLoader());
        this.password = in.readString();
        this.profilePhoto = in.readString();
        this.countryId = in.readString();
        this.address = in.readParcelable(String.class.getClassLoader());
        this.lat = in.readString();
        this.lon = in.readString();
        this.country = in.readParcelable(String.class.getClassLoader());
        this.state = in.readParcelable(String.class.getClassLoader());
        this.postalCode = in.readParcelable(String.class.getClassLoader());
        this.lcoality = in.readParcelable(String.class.getClassLoader());
        this.businessType = in.readParcelable(String.class.getClassLoader());
        this.socialId = in.readParcelable(String.class.getClassLoader());
        this.registerVia = in.readString();
        this.deviceToken = in.readString();
        this.deviceType = in.readString();
        this.rememberToken = in.readParcelable(String.class.getClassLoader());
        this.isActive = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.campaintimesCid = in.readString();
        this.quantity = in.readString();
        this.orderStatus = in.readString();
        this.orderDate = in.readString();
    }

    public static final Parcelable.Creator<Alluserdatum> CREATOR = new Parcelable.Creator<Alluserdatum>() {
        @Override
        public Alluserdatum createFromParcel(Parcel source) {
            return new Alluserdatum(source);
        }

        @Override
        public Alluserdatum[] newArray(int size) {
            return new Alluserdatum[size];
        }
    };
}
