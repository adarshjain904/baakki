
package com.app.baakki.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginSignupRes {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private LoginSignupData loginSignupData;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginSignupData getLoginSignupData() {
        return loginSignupData;
    }

    public void setLoginSignupData(LoginSignupData loginSignupData) {
        this.loginSignupData = loginSignupData;
    }
}
