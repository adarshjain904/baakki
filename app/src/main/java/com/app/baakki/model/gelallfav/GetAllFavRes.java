
package com.app.baakki.model.gelallfav;

import java.util.List;

import com.app.baakki.model.usergetallcampaign.CommonData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAllFavRes {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<CommonData> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CommonData> getData() {
        return data;
    }

    public void setData(List<CommonData> data) {
        this.data = data;
    }

}
