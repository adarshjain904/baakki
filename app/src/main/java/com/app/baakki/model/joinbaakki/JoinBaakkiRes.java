
package com.app.baakki.model.joinbaakki;


import com.app.baakki.model.cancelorder.CancelOrderData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class JoinBaakkiRes {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private JoinBaakkiData data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JoinBaakkiData getData() {
        return data;
    }

    public void setData(JoinBaakkiData data) {
        this.data = data;
    }

}
