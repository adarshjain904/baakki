
package com.app.baakki.model.mycampaignlist;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyCampaignListData implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("cid")
    @Expose
    private String cid;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("subcategory_id")
    @Expose
    private Integer subcategoryId;
    @SerializedName("campaign_type")
    @Expose
    private String campaignType;
    @SerializedName("shop_id")
    @Expose
    private Integer shopId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("original_price")
    @Expose
    private Integer originalPrice;
    @SerializedName("offer_price")
    @Expose
    private Integer offerPrice;
    @SerializedName("campaign_heading")
    @Expose
    private String campaignHeading;
    @SerializedName("remaining_quantity")
    @Expose
    private String remainingQuantity;
    @SerializedName("stock_quantity")
    @Expose
    private Integer stockQuantity;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("how_to_redeem")
    @Expose
    private String howToRedeem;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("is_listing")
    @Expose
    private String isListing;
    @SerializedName("is_deleted")
    @Expose
    private String isDeleted;
    @SerializedName("deleted_by")
    @Expose
    private String deletedBy;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("campaign_start_date")
    @Expose
    private String campaignStartDate;
    @SerializedName("campaign_end_date")
    @Expose
    private String campaignEndDate;
    @SerializedName("campaign_satrt_time")
    @Expose
    private String campaignSatrtTime;
    @SerializedName("campaign_end_time")
    @Expose
    private String campaignEndTime;
    @SerializedName("campaign_expire_message")
    @Expose
    private String campaignExpireMessage;
    @SerializedName("campaigntimes")
    @Expose
    private List<Campaigntime> campaigntimes = null;
    @SerializedName("category")
    @Expose
    private Category category;
    @SerializedName("subcategory")
    @Expose
    private Subcategory subcategory;
    @SerializedName("shop")
    @Expose
    private Shop shop;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(Integer subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(Integer originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Integer getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(Integer offerPrice) {
        this.offerPrice = offerPrice;
    }

    public String getCampaignHeading() {
        return campaignHeading;
    }

    public void setCampaignHeading(String campaignHeading) {
        this.campaignHeading = campaignHeading;
    }

    public String getRemainingQuantity() {
        return remainingQuantity;
    }

    public void setRemainingQuantity(String remainingQuantity) {
        this.remainingQuantity = remainingQuantity;
    }

    public Integer getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(Integer stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHowToRedeem() {
        return howToRedeem;
    }

    public void setHowToRedeem(String howToRedeem) {
        this.howToRedeem = howToRedeem;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIsListing() {
        return isListing;
    }

    public void setIsListing(String isListing) {
        this.isListing = isListing;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCampaignStartDate() {
        return campaignStartDate;
    }

    public void setCampaignStartDate(String campaignStartDate) {
        this.campaignStartDate = campaignStartDate;
    }

    public String getCampaignEndDate() {
        return campaignEndDate;
    }

    public void setCampaignEndDate(String campaignEndDate) {
        this.campaignEndDate = campaignEndDate;
    }

    public String getCampaignSatrtTime() {
        return campaignSatrtTime;
    }

    public void setCampaignSatrtTime(String campaignSatrtTime) {
        this.campaignSatrtTime = campaignSatrtTime;
    }

    public String getCampaignEndTime() {
        return campaignEndTime;
    }

    public void setCampaignEndTime(String campaignEndTime) {
        this.campaignEndTime = campaignEndTime;
    }

    public String getCampaignExpireMessage() {
        return campaignExpireMessage;
    }

    public void setCampaignExpireMessage(String campaignExpireMessage) {
        this.campaignExpireMessage = campaignExpireMessage;
    }

    public List<Campaigntime> getCampaigntimes() {
        return campaigntimes;
    }

    public void setCampaigntimes(List<Campaigntime> campaigntimes) {
        this.campaigntimes = campaigntimes;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Subcategory getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(Subcategory subcategory) {
        this.subcategory = subcategory;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.cid);
        dest.writeValue(this.categoryId);
        dest.writeValue(this.subcategoryId);
        dest.writeString(this.campaignType);
        dest.writeValue(this.shopId);
        dest.writeValue(this.userId);
        dest.writeValue(this.originalPrice);
        dest.writeValue(this.offerPrice);
        dest.writeString(this.campaignHeading);
        dest.writeString(this.remainingQuantity);
        dest.writeValue(this.stockQuantity);
        dest.writeString(this.address);
        dest.writeString(this.lat);
        dest.writeString(this.lon);
        dest.writeString(this.description);
        dest.writeString(this.howToRedeem);
        dest.writeString(this.image);
        dest.writeString(this.isListing);
        dest.writeString(this.isDeleted);
        dest.writeString(this.deletedBy);
        dest.writeString(this.status);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeString(this.campaignStartDate);
        dest.writeString(this.campaignEndDate);
        dest.writeString(this.campaignSatrtTime);
        dest.writeString(this.campaignEndTime);
        dest.writeString(this.campaignExpireMessage);
        dest.writeList(this.campaigntimes);
        dest.writeParcelable(this.category, flags);
        dest.writeParcelable(this.subcategory, flags);
        dest.writeParcelable(this.shop, flags);
    }

    public void readFromParcel(Parcel source) {
        this.id = (Integer) source.readValue(Integer.class.getClassLoader());
        this.cid = source.readString();
        this.categoryId = (Integer) source.readValue(Integer.class.getClassLoader());
        this.subcategoryId = (Integer) source.readValue(Integer.class.getClassLoader());
        this.campaignType = source.readString();
        this.shopId = (Integer) source.readValue(Integer.class.getClassLoader());
        this.userId = (Integer) source.readValue(Integer.class.getClassLoader());
        this.originalPrice = (Integer) source.readValue(Integer.class.getClassLoader());
        this.offerPrice = (Integer) source.readValue(Integer.class.getClassLoader());
        this.campaignHeading = source.readString();
        this.remainingQuantity = source.readString();
        this.stockQuantity = (Integer) source.readValue(Integer.class.getClassLoader());
        this.address = source.readString();
        this.lat = source.readString();
        this.lon = source.readString();
        this.description = source.readString();
        this.howToRedeem = source.readString();
        this.image = source.readString();
        this.isListing = source.readString();
        this.isDeleted = source.readString();
        this.deletedBy = source.readParcelable(Object.class.getClassLoader());
        this.status = source.readString();
        this.createdAt = source.readString();
        this.updatedAt = source.readString();
        this.campaignStartDate = source.readString();
        this.campaignEndDate = source.readString();
        this.campaignSatrtTime = source.readString();
        this.campaignEndTime = source.readString();
        this.campaignExpireMessage = source.readString();
        this.campaigntimes = new ArrayList<Campaigntime>();
        source.readList(this.campaigntimes, Campaigntime.class.getClassLoader());
        this.category = source.readParcelable(Category.class.getClassLoader());
        this.subcategory = source.readParcelable(Subcategory.class.getClassLoader());
        this.shop = source.readParcelable(Shop.class.getClassLoader());
    }

    public MyCampaignListData() {
    }

    protected MyCampaignListData(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.cid = in.readString();
        this.categoryId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.subcategoryId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.campaignType = in.readString();
        this.shopId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.userId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.originalPrice = (Integer) in.readValue(Integer.class.getClassLoader());
        this.offerPrice = (Integer) in.readValue(Integer.class.getClassLoader());
        this.campaignHeading = in.readString();
        this.remainingQuantity = in.readString();
        this.stockQuantity = (Integer) in.readValue(Integer.class.getClassLoader());
        this.address = in.readString();
        this.lat = in.readString();
        this.lon = in.readString();
        this.description = in.readString();
        this.howToRedeem = in.readString();
        this.image = in.readString();
        this.isListing = in.readString();
        this.isDeleted = in.readString();
        this.deletedBy = in.readParcelable(Object.class.getClassLoader());
        this.status = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.campaignStartDate = in.readString();
        this.campaignEndDate = in.readString();
        this.campaignSatrtTime = in.readString();
        this.campaignEndTime = in.readString();
        this.campaignExpireMessage = in.readString();
        this.campaigntimes = new ArrayList<Campaigntime>();
        in.readList(this.campaigntimes, Campaigntime.class.getClassLoader());
        this.category = in.readParcelable(Category.class.getClassLoader());
        this.subcategory = in.readParcelable(Subcategory.class.getClassLoader());
        this.shop = in.readParcelable(Shop.class.getClassLoader());
    }

    public static final Parcelable.Creator<MyCampaignListData> CREATOR = new Parcelable.Creator<MyCampaignListData>() {
        @Override
        public MyCampaignListData createFromParcel(Parcel source) {
            return new MyCampaignListData(source);
        }

        @Override
        public MyCampaignListData[] newArray(int size) {
            return new MyCampaignListData[size];
        }
    };
}
