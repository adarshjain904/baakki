
package com.app.baakki.model.vieworder;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UserDatum implements Parcelable {

    @SerializedName("campaigns_id")
    @Expose
    private String campaignsId;
    @SerializedName("cid")
    @Expose
    private String cid;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("subcategory_id")
    @Expose
    private String subcategoryId;
    @SerializedName("campaign_type")
    @Expose
    private String campaignType;
    @SerializedName("original_price")
    @Expose
    private String originalPrice;
    @SerializedName("offer_price")
    @Expose
    private String offerPrice;
    @SerializedName("campaign_heading")
    @Expose
    private String campaignHeading;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("from_time")
    @Expose
    private String fromTime;
    @SerializedName("to_time")
    @Expose
    private String toTime;

    public String getCampaignsId() {
        return campaignsId;
    }

    public void setCampaignsId(String campaignsId) {
        this.campaignsId = campaignsId;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(String offerPrice) {
        this.offerPrice = offerPrice;
    }

    public String getCampaignHeading() {
        return campaignHeading;
    }

    public void setCampaignHeading(String campaignHeading) {
        this.campaignHeading = campaignHeading;
    }

    public String getImage() {
        return image;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.campaignsId);
        dest.writeString(this.cid);
        dest.writeString(this.categoryId);
        dest.writeString(this.subcategoryId);
        dest.writeString(this.campaignType);
        dest.writeString(this.originalPrice);
        dest.writeString(this.offerPrice);
        dest.writeString(this.campaignHeading);
        dest.writeString(this.image);
        dest.writeString(this.fromTime);
        dest.writeString(this.toTime);
    }

    public void readFromParcel(Parcel source) {
        this.campaignsId = source.readString();
        this.cid = source.readString();
        this.categoryId = source.readString();
        this.subcategoryId = source.readString();
        this.campaignType = source.readString();
        this.originalPrice = source.readString();
        this.offerPrice = source.readString();
        this.campaignHeading = source.readString();
        this.image = source.readString();
        this.fromTime = source.readString();
        this.toTime = source.readString();
    }

    public UserDatum() {
    }

    protected UserDatum(Parcel in) {
        this.campaignsId = in.readString();
        this.cid = in.readString();
        this.categoryId = in.readString();
        this.subcategoryId = in.readString();
        this.campaignType = in.readString();
        this.originalPrice = in.readString();
        this.offerPrice = in.readString();
        this.campaignHeading = in.readString();
        this.image = in.readString();
        this.fromTime = in.readString();
        this.toTime = in.readString();
    }

    public static final Parcelable.Creator<UserDatum> CREATOR = new Parcelable.Creator<UserDatum>() {
        @Override
        public UserDatum createFromParcel(Parcel source) {
            return new UserDatum(source);
        }

        @Override
        public UserDatum[] newArray(int size) {
            return new UserDatum[size];
        }
    };
}
