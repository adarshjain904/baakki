
package com.app.baakki.model.usergetallcampaign;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserGelAllCampaignData {

    @SerializedName("nearby")
    @Expose
    private List<CommonData> nearby = null;
    @SerializedName("recommended")
    @Expose
    private List<CommonData> recommended = null;
    @SerializedName("expirysoon")
    @Expose
    private List<CommonData> expirysoon = null;
    @SerializedName("todayexpiry")
    @Expose
    private List<CommonData> todayexpiry = null;
    @SerializedName("favourate")
    @Expose
    private List<CommonData> favourate = null;

    @SerializedName("trending")
    @Expose
    private List<CommonData> trending = null;

    public List<CommonData> getNearby() {
        return nearby;
    }

    public void setNearby(List<CommonData> nearby) {
        this.nearby = nearby;
    }

    public List<CommonData> getRecommended() {
        return recommended;
    }

    public void setRecommended(List<CommonData> recommended) {
        this.recommended = recommended;
    }

    public List<CommonData> getExpirysoon() {
        return expirysoon;
    }

    public void setExpirysoon(List<CommonData> expirysoon) {
        this.expirysoon = expirysoon;
    }

    public List<CommonData> getTodayexpiry() {
        return todayexpiry;
    }

    public void setTodayexpiry(List<CommonData> todayexpiry) {
        this.todayexpiry = todayexpiry;
    }

    public List<CommonData> getFavourate() {
        return favourate;
    }

    public void setFavourate(List<CommonData> favourate) {
        this.favourate = favourate;
    }

    public List<CommonData> getTrending() {
        return trending;
    }

    public void setTrending(List<CommonData> trending) {
        this.trending = trending;
    }
}
