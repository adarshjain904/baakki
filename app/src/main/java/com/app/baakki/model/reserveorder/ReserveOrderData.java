
package com.app.baakki.model.reserveorder;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ReserveOrderData implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("cid")
    @Expose
    private String cid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("campaintimes_id")
    @Expose
    private Integer campaintimesId;
    @SerializedName("campaintimes_cid")
    @Expose
    private String campaintimesCid;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("subcategory_id")
    @Expose
    private Integer subcategoryId;
    @SerializedName("campaign_type")
    @Expose
    private String campaignType;
    @SerializedName("shop_id")
    @Expose
    private Integer shopId;
    @SerializedName("original_price")
    @Expose
    private Integer originalPrice;
    @SerializedName("offer_price")
    @Expose
    private Integer offerPrice;
    @SerializedName("campaign_heading")
    @Expose
    private String campaignHeading;
    @SerializedName("stock_quantity")
    @Expose
    private Integer stockQuantity;
    @SerializedName("remaining_quantity")
    @Expose
    private Integer remainingQuantity;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("how_to_redeem")
    @Expose
    private String howToRedeem;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("is_deleted")
    @Expose
    private String isDeleted;
    @SerializedName("is_listing")
    @Expose
    private String isListing;
    @SerializedName("cam_expiry")
    @Expose
    private String camExpiry;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("from_time")
    @Expose
    private String fromTime;
    @SerializedName("to_time")
    @Expose
    private String toTime;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("is_liked")
    @Expose
    private Integer isLiked;
    @SerializedName("category_image")
    @Expose
    private String categoryImage;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("cancelation_time")
    @Expose
    private String cancelationTime;
    @SerializedName("order_status")
    @Expose
    private String orderStatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public Integer getCampaintimesId() {
        return campaintimesId;
    }

    public void setCampaintimesId(Integer campaintimesId) {
        this.campaintimesId = campaintimesId;
    }

    public String getCampaintimesCid() {
        return campaintimesCid;
    }

    public void setCampaintimesCid(String campaintimesCid) {
        this.campaintimesCid = campaintimesCid;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(Integer subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(Integer originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Integer getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(Integer offerPrice) {
        this.offerPrice = offerPrice;
    }

    public String getCampaignHeading() {
        return campaignHeading;
    }

    public void setCampaignHeading(String campaignHeading) {
        this.campaignHeading = campaignHeading;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(Integer stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public Integer getRemainingQuantity() {
        return remainingQuantity;
    }

    public void setRemainingQuantity(Integer remainingQuantity) {
        this.remainingQuantity = remainingQuantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHowToRedeem() {
        return howToRedeem;
    }

    public void setHowToRedeem(String howToRedeem) {
        this.howToRedeem = howToRedeem;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getIsListing() {
        return isListing;
    }

    public void setIsListing(String isListing) {
        this.isListing = isListing;
    }

    public String getCamExpiry() {
        return camExpiry;
    }

    public void setCamExpiry(String camExpiry) {
        this.camExpiry = camExpiry;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Integer getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(Integer isLiked) {
        this.isLiked = isLiked;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCancelationTime() {
        return cancelationTime;
    }

    public void setCancelationTime(String cancelationTime) {
        this.cancelationTime = cancelationTime;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.cid);
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.mobile);
        dest.writeValue(this.campaintimesId);
        dest.writeString(this.campaintimesCid);
        dest.writeValue(this.categoryId);
        dest.writeValue(this.subcategoryId);
        dest.writeString(this.campaignType);
        dest.writeValue(this.shopId);
        dest.writeValue(this.originalPrice);
        dest.writeValue(this.offerPrice);
        dest.writeString(this.campaignHeading);
        dest.writeValue(this.stockQuantity);
        dest.writeValue(this.remainingQuantity);
        dest.writeString(this.description);
        dest.writeString(this.howToRedeem);
        dest.writeString(this.image);
        dest.writeString(this.address);
        dest.writeString(this.lat);
        dest.writeString(this.lon);
        dest.writeString(this.isDeleted);
        dest.writeString(this.isListing);
        dest.writeString(this.camExpiry);
        dest.writeString(this.date);
        dest.writeString(this.fromTime);
        dest.writeString(this.toTime);
        dest.writeValue(this.distance);
        dest.writeValue(this.isLiked);
        dest.writeString(this.categoryImage);
        dest.writeString(this.categoryName);
        dest.writeString(this.quantity);
        dest.writeString(this.orderId);
        dest.writeString(this.cancelationTime);
        dest.writeString(this.orderStatus);
    }

    public void readFromParcel(Parcel source) {
        this.id = (Integer) source.readValue(Integer.class.getClassLoader());
        this.cid = source.readString();
        this.name = source.readString();
        this.email = source.readString();
        this.mobile = source.readString();
        this.campaintimesId = (Integer) source.readValue(Integer.class.getClassLoader());
        this.campaintimesCid = source.readString();
        this.categoryId = (Integer) source.readValue(Integer.class.getClassLoader());
        this.subcategoryId = (Integer) source.readValue(Integer.class.getClassLoader());
        this.campaignType = source.readString();
        this.shopId = (Integer) source.readValue(Integer.class.getClassLoader());
        this.originalPrice = (Integer) source.readValue(Integer.class.getClassLoader());
        this.offerPrice = (Integer) source.readValue(Integer.class.getClassLoader());
        this.campaignHeading = source.readString();
        this.stockQuantity = (Integer) source.readValue(Integer.class.getClassLoader());
        this.remainingQuantity = (Integer) source.readValue(Integer.class.getClassLoader());
        this.description = source.readString();
        this.howToRedeem = source.readString();
        this.image = source.readString();
        this.address = source.readString();
        this.lat = source.readString();
        this.lon = source.readString();
        this.isDeleted = source.readString();
        this.isListing = source.readString();
        this.camExpiry = source.readString();
        this.date = source.readString();
        this.fromTime = source.readString();
        this.toTime = source.readString();
        this.distance = (Double) source.readValue(Double.class.getClassLoader());
        this.isLiked = (Integer) source.readValue(Integer.class.getClassLoader());
        this.categoryImage = source.readString();
        this.categoryName = source.readString();
        this.quantity = source.readString();
        this.orderId = source.readString();
        this.cancelationTime = source.readString();
        this.orderStatus = source.readString();
    }

    public ReserveOrderData() {
    }

    protected ReserveOrderData(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.cid = in.readString();
        this.name = in.readString();
        this.email = in.readString();
        this.mobile = in.readString();
        this.campaintimesId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.campaintimesCid = in.readString();
        this.categoryId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.subcategoryId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.campaignType = in.readString();
        this.shopId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.originalPrice = (Integer) in.readValue(Integer.class.getClassLoader());
        this.offerPrice = (Integer) in.readValue(Integer.class.getClassLoader());
        this.campaignHeading = in.readString();
        this.stockQuantity = (Integer) in.readValue(Integer.class.getClassLoader());
        this.remainingQuantity = (Integer) in.readValue(Integer.class.getClassLoader());
        this.description = in.readString();
        this.howToRedeem = in.readString();
        this.image = in.readString();
        this.address = in.readString();
        this.lat = in.readString();
        this.lon = in.readString();
        this.isDeleted = in.readString();
        this.isListing = in.readString();
        this.camExpiry = in.readString();
        this.date = in.readString();
        this.fromTime = in.readString();
        this.toTime = in.readString();
        this.distance = (Double) in.readValue(Double.class.getClassLoader());
        this.isLiked = (Integer) in.readValue(Integer.class.getClassLoader());
        this.categoryImage = in.readString();
        this.categoryName = in.readString();
        this.quantity = in.readString();
        this.orderId = in.readString();
        this.cancelationTime = in.readString();
        this.orderStatus = in.readString();
    }

    public static final Parcelable.Creator<ReserveOrderData> CREATOR = new Parcelable.Creator<ReserveOrderData>() {
        @Override
        public ReserveOrderData createFromParcel(Parcel source) {
            return new ReserveOrderData(source);
        }

        @Override
        public ReserveOrderData[] newArray(int size) {
            return new ReserveOrderData[size];
        }
    };
}
