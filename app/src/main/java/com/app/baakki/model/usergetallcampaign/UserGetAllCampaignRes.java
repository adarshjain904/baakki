
package com.app.baakki.model.usergetallcampaign;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserGetAllCampaignRes {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private UserGelAllCampaignData data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserGelAllCampaignData getData() {
        return data;
    }

    public void setData(UserGelAllCampaignData data) {
        this.data = data;
    }

}
