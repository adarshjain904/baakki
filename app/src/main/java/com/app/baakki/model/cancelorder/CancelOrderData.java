
package com.app.baakki.model.cancelorder;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CancelOrderData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("campaigns_id")
    @Expose
    private String campaignsId;
    @SerializedName("campaintimes_id")
    @Expose
    private String campaintimesId;
    @SerializedName("shop_id")
    @Expose
    private String shopId;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("total_amount")
    @Expose
    private String totalAmount;
    @SerializedName("expiry_date")
    @Expose
    private String expiryDate;
    @SerializedName("campaign_date")
    @Expose
    private String campaignDate;
    @SerializedName("campaign_time")
    @Expose
    private String campaignTime;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_lat")
    @Expose
    private String userLat;
    @SerializedName("user_lng")
    @Expose
    private String userLng;
    @SerializedName("order_status")
    @Expose
    private String orderStatus;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCampaignsId() {
        return campaignsId;
    }

    public void setCampaignsId(String campaignsId) {
        this.campaignsId = campaignsId;
    }

    public String getCampaintimesId() {
        return campaintimesId;
    }

    public void setCampaintimesId(String campaintimesId) {
        this.campaintimesId = campaintimesId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getCampaignDate() {
        return campaignDate;
    }

    public void setCampaignDate(String campaignDate) {
        this.campaignDate = campaignDate;
    }

    public String getCampaignTime() {
        return campaignTime;
    }

    public void setCampaignTime(String campaignTime) {
        this.campaignTime = campaignTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserLat() {
        return userLat;
    }

    public void setUserLat(String userLat) {
        this.userLat = userLat;
    }

    public String getUserLng() {
        return userLng;
    }

    public void setUserLng(String userLng) {
        this.userLng = userLng;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

}
