
package com.app.baakki.model.blog;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BlogList implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("heading")
    @Expose
    private String heading;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.categoryId);
        dest.writeString(this.image);
        dest.writeString(this.content);
        dest.writeString(this.heading);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);

    }

    public void readFromParcel(Parcel source) {
        this.id = (Integer) source.readValue(Integer.class.getClassLoader());
        this.categoryId = (Integer) source.readValue(Integer.class.getClassLoader());
        this.image = source.readString();
        this.content = source.readString();
        this.heading = source.readString();
        this.createdAt = source.readString();
        this.updatedAt = source.readString();

    }

    public BlogList() {
    }

    protected BlogList(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.categoryId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.image = in.readString();
        this.content = in.readString();
        this.heading = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
    }

    public static final Parcelable.Creator<BlogList> CREATOR = new Parcelable.Creator<BlogList>() {
        @Override
        public BlogList createFromParcel(Parcel source) {
            return new BlogList(source);
        }

        @Override
        public BlogList[] newArray(int size) {
            return new BlogList[size];
        }
    };
}
