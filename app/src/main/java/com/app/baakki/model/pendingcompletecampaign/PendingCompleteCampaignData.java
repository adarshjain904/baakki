
package com.app.baakki.model.pendingcompletecampaign;

import java.util.ArrayList;
import java.util.List;

import com.app.baakki.model.reserveorder.ReserveOrderData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PendingCompleteCampaignData {

    @SerializedName("completed")
    @Expose
    private ArrayList<ReserveOrderData> completed = null;
    @SerializedName("pending")
    @Expose
    private ArrayList<ReserveOrderData> pending = null;

    public ArrayList<ReserveOrderData> getCompleted() {
        return completed;
    }

    public void setCompleted(ArrayList<ReserveOrderData> completed) {
        this.completed = completed;
    }

    public ArrayList<ReserveOrderData> getPending() {
        return pending;
    }

    public void setPending(ArrayList<ReserveOrderData> pending) {
        this.pending = pending;
    }

}
