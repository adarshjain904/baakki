
package com.app.baakki.model.pendingcompletecampaign;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PendingCompleteCampaignRes {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private PendingCompleteCampaignData data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PendingCompleteCampaignData getData() {
        return data;
    }

    public void setData(PendingCompleteCampaignData data) {
        this.data = data;
    }

}
