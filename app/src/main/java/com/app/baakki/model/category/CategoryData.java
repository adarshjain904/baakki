
package com.app.baakki.model.category;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryData implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("is_featured")
    @Expose
    private Integer isFeatured;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Integer getIsFeatured() {
        return isFeatured;
    }

    public void setIsFeatured(Integer isFeatured) {
        this.isFeatured = isFeatured;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeValue(this.status);
        dest.writeString(this.photo);
        dest.writeValue(this.isFeatured);
        dest.writeString(this.updatedAt);
        dest.writeString(this.createdAt);
    }

    public void readFromParcel(Parcel source) {
        this.id = (Integer) source.readValue(Integer.class.getClassLoader());
        this.name = source.readString();
        this.status = (Integer) source.readValue(Integer.class.getClassLoader());
        this.photo = source.readString();
        this.isFeatured = (Integer) source.readValue(Integer.class.getClassLoader());
        this.updatedAt = source.readString();
        this.createdAt = source.readString();
    }

    public CategoryData() {
    }

    protected CategoryData(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.name = in.readString();
        this.status = (Integer) in.readValue(Integer.class.getClassLoader());
        this.photo = in.readString();
        this.isFeatured = (Integer) in.readValue(Integer.class.getClassLoader());
        this.updatedAt = in.readString();
        this.createdAt = in.readString();
    }

    public static final Parcelable.Creator<CategoryData> CREATOR = new Parcelable.Creator<CategoryData>() {
        @Override
        public CategoryData createFromParcel(Parcel source) {
            return new CategoryData(source);
        }

        @Override
        public CategoryData[] newArray(int size) {
            return new CategoryData[size];
        }
    };
}
