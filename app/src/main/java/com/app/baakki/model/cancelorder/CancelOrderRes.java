
package com.app.baakki.model.cancelorder;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CancelOrderRes {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private CancelOrderData data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CancelOrderData getData() {
        return data;
    }

    public void setData(CancelOrderData data) {
        this.data = data;
    }

}
