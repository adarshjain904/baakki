
package com.app.baakki.model.mycampaignlist;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Campaigntime implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("cid")
    @Expose
    private String cid;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("shop_id")
    @Expose
    private Integer shopId;
    @SerializedName("campaign_id")
    @Expose
    private Integer campaignId;
    @SerializedName("campaign_type")
    @Expose
    private String campaignType;
    @SerializedName("stock_quantity")
    @Expose
    private Integer stockQuantity;
    @SerializedName("rest_quantity")
    @Expose
    private String restQuantity;
    @SerializedName("sold_quantity")
    @Expose
    private String soldQuantity;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("from_time")
    @Expose
    private String fromTime;
    @SerializedName("to_time")
    @Expose
    private String toTime;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    public String getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public Integer getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(Integer stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public String getRestQuantity() {
        return restQuantity;
    }

    public void setRestQuantity(String restQuantity) {
        this.restQuantity = restQuantity;
    }

    public String getSoldQuantity() {
        return soldQuantity;
    }

    public void setSoldQuantity(String soldQuantity) {
        this.soldQuantity = soldQuantity;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.cid);
        dest.writeValue(this.userId);
        dest.writeValue(this.shopId);
        dest.writeValue(this.campaignId);
        dest.writeString(this.campaignType);
        dest.writeValue(this.stockQuantity);
        dest.writeString(this.restQuantity);
        dest.writeString(this.soldQuantity);
        dest.writeString(this.date);
        dest.writeString(this.fromTime);
        dest.writeString(this.toTime);
        dest.writeString(this.isActive);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
    }

    public void readFromParcel(Parcel source) {
        this.id = (Integer) source.readValue(Integer.class.getClassLoader());
        this.cid = source.readString();
        this.userId = (Integer) source.readValue(Integer.class.getClassLoader());
        this.shopId = (Integer) source.readValue(Integer.class.getClassLoader());
        this.campaignId = (Integer) source.readValue(Integer.class.getClassLoader());
        this.campaignType = source.readString();
        this.stockQuantity = (Integer) source.readValue(Integer.class.getClassLoader());
        this.restQuantity = source.readParcelable(Object.class.getClassLoader());
        this.soldQuantity = source.readParcelable(Object.class.getClassLoader());
        this.date = source.readString();
        this.fromTime = source.readString();
        this.toTime = source.readString();
        this.isActive = source.readString();
        this.createdAt = source.readString();
        this.updatedAt = source.readString();
    }

    public Campaigntime() {
    }

    protected Campaigntime(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.cid = in.readString();
        this.userId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.shopId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.campaignId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.campaignType = in.readString();
        this.stockQuantity = (Integer) in.readValue(Integer.class.getClassLoader());
        this.restQuantity = in.readParcelable(Object.class.getClassLoader());
        this.soldQuantity = in.readParcelable(Object.class.getClassLoader());
        this.date = in.readString();
        this.fromTime = in.readString();
        this.toTime = in.readString();
        this.isActive = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
    }

    public static final Parcelable.Creator<Campaigntime> CREATOR = new Parcelable.Creator<Campaigntime>() {
        @Override
        public Campaigntime createFromParcel(Parcel source) {
            return new Campaigntime(source);
        }

        @Override
        public Campaigntime[] newArray(int size) {
            return new Campaigntime[size];
        }
    };
}
