
package com.app.baakki.model.addcampaignres;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddCampaignRes {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private AddCampaignData data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AddCampaignData getData() {
        return data;
    }

    public void setData(AddCampaignData data) {
        this.data = data;
    }

}
