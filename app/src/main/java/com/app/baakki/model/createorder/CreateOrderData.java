
package com.app.baakki.model.createorder;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CreateOrderData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("campaigns_id")
    @Expose
    private Integer campaignsId;
    @SerializedName("campaintimes_id")
    @Expose
    private Integer campaintimesId;
    @SerializedName("shop_id")
    @Expose
    private Integer shopId;
    @SerializedName("total_amount")
    @Expose
    private Integer totalAmount;
    @SerializedName("campaign_date")
    @Expose
    private String campaignDate;
    @SerializedName("campaign_time")
    @Expose
    private String campaignTime;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("user_lat")
    @Expose
    private Double userLat;
    @SerializedName("user_lng")
    @Expose
    private Double userLng;
    @SerializedName("order_status")
    @Expose
    private String orderStatus;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCampaignsId() {
        return campaignsId;
    }

    public void setCampaignsId(Integer campaignsId) {
        this.campaignsId = campaignsId;
    }

    public Integer getCampaintimesId() {
        return campaintimesId;
    }

    public void setCampaintimesId(Integer campaintimesId) {
        this.campaintimesId = campaintimesId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCampaignDate() {
        return campaignDate;
    }

    public void setCampaignDate(String campaignDate) {
        this.campaignDate = campaignDate;
    }

    public String getCampaignTime() {
        return campaignTime;
    }

    public void setCampaignTime(String campaignTime) {
        this.campaignTime = campaignTime;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Double getUserLat() {
        return userLat;
    }

    public void setUserLat(Double userLat) {
        this.userLat = userLat;
    }

    public Double getUserLng() {
        return userLng;
    }

    public void setUserLng(Double userLng) {
        this.userLng = userLng;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

}
