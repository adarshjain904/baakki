package com.app.baakki.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChooseLocation implements Parcelable {
    @SerializedName("latLng")
    @Expose
    LatLng latLng;
    @SerializedName("address")
    @Expose
    String address;
    @SerializedName("distance")
    @Expose
    String distance;
    @SerializedName("categoryId")
    @Expose
    String categoryId;
    @SerializedName("categoryName")
    @Expose
    String categoryName;

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.latLng, flags);
        dest.writeString(this.address);
        dest.writeString(this.distance);
        dest.writeString(this.categoryId);
        dest.writeString(this.categoryName);
    }

    public void readFromParcel(Parcel source) {
        this.latLng = source.readParcelable(LatLng.class.getClassLoader());
        this.address = source.readString();
        this.distance = source.readString();
        this.categoryId = source.readString();
        this.categoryName = source.readString();
    }

    public ChooseLocation() {
    }

    protected ChooseLocation(Parcel in) {
        this.latLng = in.readParcelable(LatLng.class.getClassLoader());
        this.address = in.readString();
        this.distance = in.readString();
        this.categoryId = in.readString();
        this.categoryName = in.readString();
    }

    public static final Parcelable.Creator<ChooseLocation> CREATOR = new Parcelable.Creator<ChooseLocation>() {
        @Override
        public ChooseLocation createFromParcel(Parcel source) {
            return new ChooseLocation(source);
        }

        @Override
        public ChooseLocation[] newArray(int size) {
            return new ChooseLocation[size];
        }
    };
}
